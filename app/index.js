const { app, BrowserWindow, ipcMain, Menu } = require('electron');

const state = {
   f4 () {
      state.window.setFullScreen((state.fullscreen = !state.fullscreen));
      if (state.pad !== void 0) {
         state.restore(state.pad);
         state.pad = void 0;
      }
   },
   fullscreen: false,
   init () {
      state.window = new BrowserWindow({
         height: 480,
         icon: './icon.png',
         title: 'DUSTTALE: THE FRACTURED QUEEN',
         useContentSize: process.platform === 'win32',
         webPreferences: {
            disableDialogs: true,
            disableHtmlFullscreenWindowResize: true,
            enableWebSQL: false,
            spellcheck: false
         },
         width: 640,
         x: state.position?.[0],
         y: state.position?.[1]
      });
      state.window.on('closed', () => {
         state.window = null;
      });
      state.window?.webContents.on('before-input-event', (_, { key, type }) => {
         state.window && type === 'keyDown' && (key === 'F4' || key === 'F11') && state.f4();
      });
      const port = process.argv.find(arg => arg.length !== 0 && !Number.isNaN(+arg));
      if (port === void 0) {
         state.window.loadFile(`./assets/index.html`);
      } else {
         state.window.loadURL(`http://localhost:${port}/index.html`);
      }
      state.size = state.window.getSize();
      process.argv.includes('devtools') && state.window.webContents.openDevTools();
   },
   /** @type {number | void} */
   pad: 0,
   /** @type {number[] | void} */
   position: void 0,
   reload: false,
   respawn: false,
   /** @param {number} pad */
   restore (pad) {
      if (state.fullscreen) {
         state.pad = pad;
      } else {
         state.window?.setSize(state.size[0] + pad, state.size[1], true);
      }
   },
   /** @type {number[]} */
   size: [],
   /** @type {BrowserWindow | void} */
   window: void 0
};

app.on('ready', () => {
   state.init();
});

app.on('activate', () => {
   state.window ? state.window.focus() : state.init();
});

app.on('window-all-closed', () => {
   state.reload || app.exit();
});

ipcMain.handle('f4', () => {
   state.f4();
});

ipcMain.handle('reload', () => {
   state.window?.reload();
});

Menu.setApplicationMenu(null);
