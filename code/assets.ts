import { renderer } from './core';
import { X, XAudio, XColor, XDaemon, XInstance, XRouter, XSprite } from './storyteller';

import castleRoadBackdrop1 from '../assets/image/backgrounds/castle-road-backdrop-1.png?url';
import castleRoadBackdrop2 from '../assets/image/backgrounds/castle-road-backdrop-2.png?url';
import castleRoadBackdrop3 from '../assets/image/backgrounds/castle-road-backdrop-3.png?url';
import castleRoadBackdrop4 from '../assets/image/backgrounds/castle-road-backdrop-4.png?url';
import castleRoadBackdrop5 from '../assets/image/backgrounds/castle-road-backdrop-5.png?url';
import castleRoadBackdrop6 from '../assets/image/backgrounds/castle-road-backdrop-6.png?url';
import castleRoadBackdrop7 from '../assets/image/backgrounds/castle-road-backdrop-7.png?url';
import throneRoomOverlay from '../assets/image/backgrounds/throne-room-overlay.png?url';
import soulNormal from '../assets/image/players/soul/normal.png?url';
import soulInvulnerable from '../assets/image/players/soul/invulnerable.png?url';
import friskDown0 from '../assets/image/players/frisk/spr_maincharad_0.png?url';
import friskDown1 from '../assets/image/players/frisk/spr_maincharad_1.png?url';
import friskDown2 from '../assets/image/players/frisk/spr_maincharad_2.png?url';
import friskDown3 from '../assets/image/players/frisk/spr_maincharad_3.png?url';
import friskLeft0 from '../assets/image/players/frisk/spr_maincharal_0.png?url';
import friskLeft1 from '../assets/image/players/frisk/spr_maincharal_1.png?url';
import friskRight0 from '../assets/image/players/frisk/spr_maincharar_0.png?url';
import friskRight1 from '../assets/image/players/frisk/spr_maincharar_1.png?url';
import friskUp0 from '../assets/image/players/frisk/spr_maincharau_0.png?url';
import friskUp1 from '../assets/image/players/frisk/spr_maincharau_1.png?url';
import friskUp2 from '../assets/image/players/frisk/spr_maincharau_2.png?url';
import friskUp3 from '../assets/image/players/frisk/spr_maincharau_3.png?url';
import friskSilhouetteDown0 from '../assets/image/players/frisk/spr_maincharad_0.png?url';
import friskSilhouetteDown1 from '../assets/image/players/frisk/spr_maincharad_1.png?url';
import friskSilhouetteDown2 from '../assets/image/players/frisk/spr_maincharad_2.png?url';
import friskSilhouetteDown3 from '../assets/image/players/frisk/spr_maincharad_3.png?url';
import friskSilhouetteLeft0 from '../assets/image/players/frisk/spr_maincharal_0.png?url';
import friskSilhouetteLeft1 from '../assets/image/players/frisk/spr_maincharal_1.png?url';
import friskSilhouetteRight0 from '../assets/image/players/frisk/spr_maincharar_0.png?url';
import friskSilhouetteRight1 from '../assets/image/players/frisk/spr_maincharar_1.png?url';
import friskSilhouetteUp0 from '../assets/image/players/frisk/spr_maincharau_0.png?url';
import friskSilhouetteUp1 from '../assets/image/players/frisk/spr_maincharau_1.png?url';
import friskSilhouetteUp2 from '../assets/image/players/frisk/spr_maincharau_2.png?url';
import friskSilhouetteUp3 from '../assets/image/players/frisk/spr_maincharau_3.png?url';
import friskStarkDown0 from '../assets/image/players/frisk-stark/spr_maincharad_stark_0.png?url';
import friskStarkDown1 from '../assets/image/players/frisk-stark/spr_maincharad_stark_1.png?url';
import friskStarkDown2 from '../assets/image/players/frisk-stark/spr_maincharad_stark_2.png?url';
import friskStarkDown3 from '../assets/image/players/frisk-stark/spr_maincharad_stark_3.png?url';
import friskStarkLeft0 from '../assets/image/players/frisk-stark/spr_maincharal_stark_0.png?url';
import friskStarkLeft1 from '../assets/image/players/frisk-stark/spr_maincharal_stark_1.png?url';
import friskStarkRight0 from '../assets/image/players/frisk-stark/spr_maincharar_stark_0.png?url';
import friskStarkRight1 from '../assets/image/players/frisk-stark/spr_maincharar_stark_1.png?url';
import friskStarkUp0 from '../assets/image/players/frisk-stark/spr_maincharau_stark_0.png?url';
import friskStarkUp1 from '../assets/image/players/frisk-stark/spr_maincharau_stark_1.png?url';
import friskStarkUp2 from '../assets/image/players/frisk-stark/spr_maincharau_stark_2.png?url';
import friskStarkUp3 from '../assets/image/players/frisk-stark/spr_maincharau_stark_3.png?url';
import castleRoadBackground from '../assets/image/backgrounds/castle-road.png?url';
import lastCorridorBackground from '../assets/image/backgrounds/last-corridor.png?url';
import castleHookBackground from '../assets/image/backgrounds/castle-hook.png?url';
import castleHookScribbleBackground from '../assets/image/backgrounds/castle-hook-scribble.png?url';
import coffinStairsBackground from '../assets/image/backgrounds/coffin-stairs.png?url';
import throneRoomBackground from '../assets/image/backgrounds/throne-room.png?url';
import coffinRoomBackground from '../assets/image/backgrounds/coffin-room.png?url';
import coffinRoomMonsterBackground from '../assets/image/backgrounds/coffin-room-monster.png?url';
import buttonFight0 from '../assets/image/buttons/fight0.png?url';
import buttonFight1 from '../assets/image/buttons/fight1.png?url';
import buttonAct0 from '../assets/image/buttons/act0.png?url';
import buttonAct1 from '../assets/image/buttons/act1.png?url';
import buttonItem0 from '../assets/image/buttons/item0.png?url';
import buttonItem1 from '../assets/image/buttons/item1.png?url';
import buttonMercy0 from '../assets/image/buttons/mercy0.png?url';
import buttonMercy1 from '../assets/image/buttons/mercy1.png?url';
import deadeye from '../assets/image/sprites/misc/deadeye.png?url';
import hp from '../assets/image/sprites/misc/hp.png?url';
import menu from '../assets/image/sprites/misc/menu.png?url';
import pillar from '../assets/image/sprites/misc/pillar.png?url';
import sans3 from '../assets/image/sprites/sans/spr_sans_l_3.png?url';
import sans0 from '../assets/image/sprites/sans/spr_sans_l_0.png?url';
import sans1 from '../assets/image/sprites/sans/spr_sans_l_1.png?url';
import sans2 from '../assets/image/sprites/sans/spr_sans_l_2.png?url';
import throneRoomDecoration from '../assets/image/backgrounds/throne-room-decoration.png?url';
import goatmom0 from '../assets/image/sprites/toriel/toriel-back.png?url';
import goatmom1 from '../assets/image/sprites/toriel/toriel-front.png?url';
import sfxAppear from '../assets/audio/sfx/snd_spearappear.mp3?url';
import sfxBattleFall from '../assets/audio/sfx/battle-fall.mp3?url';
import sfxBell from '../assets/audio/sfx/bell.mp3?url';
import sfxMenu from '../assets/audio/sfx/menu.mp3?url';
import sfxHeal from '../assets/audio/sfx/heal.mp3?url';
import sfxSave from '../assets/audio/sfx/save.mp3?url';
import sfxDeath from '../assets/audio/sfx/break.mp3?url';
import sfxShatter from '../assets/audio/sfx/shatter.mp3?url';
import sfxHurt from '../assets/audio/sfx/snd_hurt1.mp3?url';
import sfxNoise from '../assets/audio/sfx/noise.mp3?url';
import sfxCharge from '../assets/audio/sfx/charge.mp3?url';
import sfxFire from '../assets/audio/sfx/fire.mp3?url';
import sfxSwing from '../assets/audio/sfx/swing.mp3?url';
import sfxHit from '../assets/audio/sfx/hit.mp3?url';
import sfxInfo from '../assets/audio/sfx/snd_item.mp3?url';
import sfxSelect from '../assets/audio/sfx/select.mp3?url';
import sfxBox from '../assets/audio/sfx/box.mp3?url';
import sansVoice from '../assets/audio/voices/sans.mp3?url';
import asgoreVoice from '../assets/audio/voices/asgore.mp3?url';
import floweyVoice from '../assets/audio/voices/flowey2.mp3?url';
import torielVoice from '../assets/audio/voices/toriel1.mp3?url';
import narratorVoice from '../assets/audio/voices/narrator.mp3?url';
import storyVoice from '../assets/audio/voices/storyteller.mp3?url';
import introMusic from '../assets/audio/music/intro.mp3?url';
import birdsAmbience from '../assets/audio/music/birds.mp3?url';
import speechRight from '../assets/image/speech/right.png?url';
import torielBody from '../assets/image/sprites/toriel-battle/toriel.base.png?url';
import torielBrowFair from '../assets/image/sprites/toriel-battle/toriel.brow.fair.png?url';
import torielBrowFurrow from '../assets/image/sprites/toriel-battle/toriel.brow.furrow.png?url';
import torielBrowNeutral from '../assets/image/sprites/toriel-battle/toriel.brow.neutral.png?url';
import torielBrowOpen from '../assets/image/sprites/toriel-battle/toriel.brow.open.png?url';
import torielBrowPerked from '../assets/image/sprites/toriel-battle/toriel.brow.perked.png?url';
import torielBrowPushed from '../assets/image/sprites/toriel-battle/toriel.brow.pushed.png?url';
import torielBrowRaised from '../assets/image/sprites/toriel-battle/toriel.brow.raised.png?url';
import torielBrowWild from '../assets/image/sprites/toriel-battle/toriel.brow.wild.png?url';
import torielBrowWhatL from '../assets/image/sprites/toriel-battle/toriel.brow.what-l.png?url';
import deathMusic from '../assets/audio/music/death.mp3?url';
import phase1Music from '../assets/audio/music/phase1.mp3?url';
import torielBrowWhatR from '../assets/image/sprites/toriel-battle/toriel.brow.what-r.png?url';
import torielBrowFlat from '../assets/image/sprites/toriel-battle/toriel.brow.flat.png?url';
import torielBrowSlant from '../assets/image/sprites/toriel-battle/toriel.brow.slant.png?url';
import torielEyesClosed from '../assets/image/sprites/toriel-battle/toriel.eyes.closed.png?url';
import torielEyesOpenA from '../assets/image/sprites/toriel-battle/toriel.eyes.open-a.png?url';
import torielEyesOpenB from '../assets/image/sprites/toriel-battle/toriel.eyes.open-b.png?url';
import torielEyesOpenC from '../assets/image/sprites/toriel-battle/toriel.eyes.open-c.png?url';
import torielEyesWtfL from '../assets/image/sprites/toriel-battle/toriel.eyes.wtf-l.png?url';
import torielEyesWtfR from '../assets/image/sprites/toriel-battle/toriel.eyes.wtf-r.png?url';
import torielEyesUp from '../assets/image/sprites/toriel-battle/toriel.eyes.up.png?url';
import torielMouthNeutral from '../assets/image/sprites/toriel-battle/toriel.mouth.neutral.png?url';
import torielMouthSmile from '../assets/image/sprites/toriel-battle/toriel.mouth.smile.png?url';
import torielMouthSmol from '../assets/image/sprites/toriel-battle/toriel.mouth.smol.png?url';
import torielMouthSmileOpen from '../assets/image/sprites/toriel-battle/toriel.mouth.smile-open.png?url';
import torielMouthSmileWide from '../assets/image/sprites/toriel-battle/toriel.mouth.smile-wide.png?url';
import torielMouthSmileSus from '../assets/image/sprites/toriel-battle/toriel.mouth.smile-sus.png?url';
import torielMouthNeutralOpen from '../assets/image/sprites/toriel-battle/toriel.mouth.open-neutral.png?url';
import torielArmExtendLeft from '../assets/image/sprites/toriel-battle/toriel.arm-extend-left.png?url';
import torielArmExtendRight from '../assets/image/sprites/toriel-battle/toriel.arm-extend-right.png?url';
import torielArmPocketLeft from '../assets/image/sprites/toriel-battle/toriel.arm-pocket-left.png?url';
import torielArmPocketRight from '../assets/image/sprites/toriel-battle/toriel.arm-pocket-right.png?url';
import azzyBlaster0 from '../assets/image/sprites/toriel-battle/blaster/blaster_layer_1.png?url';
import azzyBlaster1 from '../assets/image/sprites/toriel-battle/blaster/blaster_layer_2.png?url';
import azzyBlaster2 from '../assets/image/sprites/toriel-battle/blaster/blaster_layer_3.png?url';
import azzyBlaster3 from '../assets/image/sprites/toriel-battle/blaster/blaster_layer_4.png?url';
import azzyBlaster4 from '../assets/image/sprites/toriel-battle/blaster/blaster_layer_5.png?url';
import sstriker1 from '../assets/image/sprites/toriel-battle/sstriker/sstriker_layer_1.png?url';
import sstriker2 from '../assets/image/sprites/toriel-battle/sstriker/sstriker_layer_2.png?url';
import sstriker3 from '../assets/image/sprites/toriel-battle/sstriker/sstriker_layer_3.png?url';
import sstriker4 from '../assets/image/sprites/toriel-battle/sstriker/sstriker_layer_4.png?url';
import sstriker5 from '../assets/image/sprites/toriel-battle/sstriker/sstriker_layer_5.png?url';
import sstriker6 from '../assets/image/sprites/toriel-battle/sstriker/sstriker_layer_6.png?url';

function bobber (self: XSprite) {
   return () => (self.position.y = -(self.state.index % 2));
}

export const blackshader = (color: XColor): XColor => [ 0, 0, 0, color[3] ];

export const content = {
   castleRoadBackdrop1: X.imageAsset(castleRoadBackdrop1),
   castleRoadBackdrop2: X.imageAsset(castleRoadBackdrop2),
   castleRoadBackdrop3: X.imageAsset(castleRoadBackdrop3),
   castleRoadBackdrop4: X.imageAsset(castleRoadBackdrop4),
   castleRoadBackdrop5: X.imageAsset(castleRoadBackdrop5),
   castleRoadBackdrop6: X.imageAsset(castleRoadBackdrop6),
   castleRoadBackdrop7: X.imageAsset(castleRoadBackdrop7),
   throneRoomOverlay: X.imageAsset(throneRoomOverlay),
   soulNormal: X.imageAsset(soulNormal),
   soulInvulnerable: X.imageAsset(soulInvulnerable),
   friskDown0: X.imageAsset(friskDown0),
   friskDown1: X.imageAsset(friskDown1),
   friskDown2: X.imageAsset(friskDown2),
   friskDown3: X.imageAsset(friskDown3),
   friskLeft0: X.imageAsset(friskLeft0),
   friskLeft1: X.imageAsset(friskLeft1),
   friskRight0: X.imageAsset(friskRight0),
   friskRight1: X.imageAsset(friskRight1),
   friskUp0: X.imageAsset(friskUp0),
   friskUp1: X.imageAsset(friskUp1),
   friskUp2: X.imageAsset(friskUp2),
   friskUp3: X.imageAsset(friskUp3),
   friskSilhouetteDown0: X.imageAsset(friskSilhouetteDown0, { shader: blackshader }),
   friskSilhouetteDown1: X.imageAsset(friskSilhouetteDown1, { shader: blackshader }),
   friskSilhouetteDown2: X.imageAsset(friskSilhouetteDown2, { shader: blackshader }),
   friskSilhouetteDown3: X.imageAsset(friskSilhouetteDown3, { shader: blackshader }),
   friskSilhouetteLeft0: X.imageAsset(friskSilhouetteLeft0, { shader: blackshader }),
   friskSilhouetteLeft1: X.imageAsset(friskSilhouetteLeft1, { shader: blackshader }),
   friskSilhouetteRight0: X.imageAsset(friskSilhouetteRight0, { shader: blackshader }),
   friskSilhouetteRight1: X.imageAsset(friskSilhouetteRight1, { shader: blackshader }),
   friskSilhouetteUp0: X.imageAsset(friskSilhouetteUp0, { shader: blackshader }),
   friskSilhouetteUp1: X.imageAsset(friskSilhouetteUp1, { shader: blackshader }),
   friskSilhouetteUp2: X.imageAsset(friskSilhouetteUp2, { shader: blackshader }),
   friskSilhouetteUp3: X.imageAsset(friskSilhouetteUp3, { shader: blackshader }),
   friskStarkDown0: X.imageAsset(friskStarkDown0),
   friskStarkDown1: X.imageAsset(friskStarkDown1),
   friskStarkDown2: X.imageAsset(friskStarkDown2),
   friskStarkDown3: X.imageAsset(friskStarkDown3),
   friskStarkLeft0: X.imageAsset(friskStarkLeft0),
   friskStarkLeft1: X.imageAsset(friskStarkLeft1),
   friskStarkRight0: X.imageAsset(friskStarkRight0),
   friskStarkRight1: X.imageAsset(friskStarkRight1),
   friskStarkUp0: X.imageAsset(friskStarkUp0),
   friskStarkUp1: X.imageAsset(friskStarkUp1),
   friskStarkUp2: X.imageAsset(friskStarkUp2),
   friskStarkUp3: X.imageAsset(friskStarkUp3),
   castleRoadBackground: X.imageAsset(castleRoadBackground),
   lastCorridorBackground: X.imageAsset(lastCorridorBackground),
   castleHookBackground: X.imageAsset(castleHookBackground),
   castleHookScribbleBackground: X.imageAsset(castleHookScribbleBackground),
   coffinStairsBackground: X.imageAsset(coffinStairsBackground),
   throneRoomBackground: X.imageAsset(throneRoomBackground),
   coffinRoomBackground: X.imageAsset(coffinRoomBackground),
   coffinRoomMonsterBackground: X.imageAsset(coffinRoomMonsterBackground),
   buttonFight0: X.imageAsset(buttonFight0),
   buttonFight1: X.imageAsset(buttonFight1),
   buttonAct0: X.imageAsset(buttonAct0),
   buttonAct1: X.imageAsset(buttonAct1),
   buttonItem0: X.imageAsset(buttonItem0),
   buttonItem1: X.imageAsset(buttonItem1),
   buttonMercy0: X.imageAsset(buttonMercy0),
   buttonMercy1: X.imageAsset(buttonMercy1),
   deadeye: X.imageAsset(deadeye),
   hp: X.imageAsset(hp),
   menu: X.imageAsset(menu),
   pillar: X.imageAsset(pillar),
   sans3: X.imageAsset(sans3, { shader: blackshader }),
   sans0: X.imageAsset(sans0, { shader: blackshader }),
   sans1: X.imageAsset(sans1, { shader: blackshader }),
   sans2: X.imageAsset(sans2, { shader: blackshader }),
   throneRoomDecoration: X.imageAsset(throneRoomDecoration),
   goatmom0: X.imageAsset(goatmom0),
   goatmom1: X.imageAsset(goatmom1),
   sfxAppear: X.audioAsset(sfxAppear),
   sfxBattleFall: X.audioAsset(sfxBattleFall),
   sfxBell: X.audioAsset(sfxBell),
   sfxMenu: X.audioAsset(sfxMenu),
   sfxHeal: X.audioAsset(sfxHeal),
   sfxSave: X.audioAsset(sfxSave),
   sfxDeath: X.audioAsset(sfxDeath),
   sfxShatter: X.audioAsset(sfxShatter),
   sfxHurt: X.audioAsset(sfxHurt),
   sfxNoise: X.audioAsset(sfxNoise),
   sfxCharge: X.audioAsset(sfxCharge),
   sfxFire: X.audioAsset(sfxFire),
   sfxSwing: X.audioAsset(sfxSwing),
   sfxHit: X.audioAsset(sfxHit),
   sfxInfo: X.audioAsset(sfxInfo),
   sfxSelect: X.audioAsset(sfxSelect),
   sfxBox: X.audioAsset(sfxBox),
   sansVoice: X.audioAsset(sansVoice),
   asgoreVoice: X.audioAsset(asgoreVoice),
   floweyVoice: X.audioAsset(floweyVoice),
   torielVoice: X.audioAsset(torielVoice),
   narratorVoice: X.audioAsset(narratorVoice),
   storyVoice: X.audioAsset(storyVoice),
   introMusic: X.audioAsset(introMusic),
   birdsAmbience: X.audioAsset(birdsAmbience),
   speechRight: X.imageAsset(speechRight),
   torielBody: X.imageAsset(torielBody),
   torielBrowFair: X.imageAsset(torielBrowFair),
   torielBrowFurrow: X.imageAsset(torielBrowFurrow),
   torielBrowNeutral: X.imageAsset(torielBrowNeutral),
   torielBrowOpen: X.imageAsset(torielBrowOpen),
   torielBrowPerked: X.imageAsset(torielBrowPerked),
   torielBrowPushed: X.imageAsset(torielBrowPushed),
   torielBrowRaised: X.imageAsset(torielBrowRaised),
   torielBrowWild: X.imageAsset(torielBrowWild),
   torielBrowWhatL: X.imageAsset(torielBrowWhatL),
   deathMusic: X.audioAsset(deathMusic),
   phase1Music: X.audioAsset(phase1Music),
   torielBrowWhatR: X.imageAsset(torielBrowWhatR),
   torielBrowFlat: X.imageAsset(torielBrowFlat),
   torielBrowSlant: X.imageAsset(torielBrowSlant),
   torielEyesClosed: X.imageAsset(torielEyesClosed),
   torielEyesOpenA: X.imageAsset(torielEyesOpenA),
   torielEyesOpenB: X.imageAsset(torielEyesOpenB),
   torielEyesOpenC: X.imageAsset(torielEyesOpenC),
   torielEyesWtfL: X.imageAsset(torielEyesWtfL),
   torielEyesWtfR: X.imageAsset(torielEyesWtfR),
   torielEyesUp: X.imageAsset(torielEyesUp),
   torielMouthNeutral: X.imageAsset(torielMouthNeutral),
   torielMouthSmile: X.imageAsset(torielMouthSmile),
   torielMouthSmol: X.imageAsset(torielMouthSmol),
   torielMouthSmileOpen: X.imageAsset(torielMouthSmileOpen),
   torielMouthSmileWide: X.imageAsset(torielMouthSmileWide),
   torielMouthSmileSus: X.imageAsset(torielMouthSmileSus),
   torielMouthNeutralOpen: X.imageAsset(torielMouthNeutralOpen),
   torielArmExtendLeft: X.imageAsset(torielArmExtendLeft),
   torielArmExtendRight: X.imageAsset(torielArmExtendRight),
   torielArmPocketLeft: X.imageAsset(torielArmPocketLeft),
   torielArmPocketRight: X.imageAsset(torielArmPocketRight),
   azzyBlaster0: X.imageAsset(azzyBlaster0),
   azzyBlaster1: X.imageAsset(azzyBlaster1),
   azzyBlaster2: X.imageAsset(azzyBlaster2),
   azzyBlaster3: X.imageAsset(azzyBlaster3),
   azzyBlaster4: X.imageAsset(azzyBlaster4),
   sstriker1: X.imageAsset(sstriker1),
   sstriker2: X.imageAsset(sstriker2),
   sstriker3: X.imageAsset(sstriker3),
   sstriker4: X.imageAsset(sstriker4),
   sstriker5: X.imageAsset(sstriker5),
   sstriker6: X.imageAsset(sstriker6),
   save0: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAATCAYAAACQjC21AAAABHNCSVQICAgIfAhkiAAAAJVJREFUOE/Fk1sOwCAIBLEH6P3P2QvYgI8QBdm0JvbTLpMBlAj7MhYjutAgmkOAOT+CgywRIConuRSkm10J33HNdsOzQLUcXpC5JGuGPVgB7phTmWn7LywNFFAE8egNzsDPIMiwGaxMFWga2XSgWjLNK8ytW10bLkpDW94I+/kKGBb/CfATdO+eBh8zZAnzZYxtbzd8AcogIfTBlJSgAAAAAElFTkSuQmCC'
   ),
   save1: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAKCAYAAABi8KSDAAAABHNCSVQICAgIfAhkiAAAAGlJREFUGFeNUIENwCAIgx2w/+/cA8xWIcUty5oYkRaomJnFOH8QLmLEO1qjA2xc1IBQMpBfHHjPbiSYOee9vanT0VUw5VVYGtpY8OwquRaq+KGRv5B7tfHlG0Xwm9toGwGXR/e8T+JoQdyraSjCsdP8kAAAAABJRU5ErkJggg=='
   ),
   normalFireball0: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAARFJREFUWEftloEKwyAMROv+/587Ip3N5E4v0TEYKxTa1ORezioex//KO3DmU+/MkizixbM1qvQjAVDFz7MxLDmRAUgw85QlgB0uRAGW7EY+RAD6ua/1Vl2IAKAGUCzk0hYA4IIMoQJA+2H7weWpAiCtLbHfAXD/QWhlpBzwYqvzkAJgohmwrQAMbBRXAOQlOBJi3xQAljuMg80Jjp8BfLR7I5oBQGoLllLazQYpLowAaPcm7q/+nQGh+AgAja9dX5c9vG4fh3ksGAZghbLxrwPMjtTwP0BzjnbBbrqgSSkHerH+HSqR4MwBS4MukHotrHRvgxWAMIQqHgFoEPXhPna1jmux9/1Bak4a5FTUw6Zc9wmmiF0F4pOO5gAAAABJRU5ErkJggg=='
   ),
   normalFireball1: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAOpJREFUWEftlUsOwyAMRCH3vzMNlSBkAv6MlU1EpC7c2p7nsdumtJ/twHZgO/ABB0pkhhwpPmtHcarXEQD4i5fSGSgnIgAB9quUBcDpaRgWgBbEQi+Atmftc9RPHoDWfGo/e4xWgKnoYxziDSvArfUwLSF5L6EAUDUC5AaQxJg7sAC8tv/qpAbwqrgFANfd45xzaq8xybsG6R9sOX0VxgdvY8h5Jg/F2gpQB2OxOSbP4iiA+6cXISgAtBtjFJFizcLlHYhNrxvR+ic14RRyQViPrw1gAai5fdcru+GbYe1rcqDBWg/OLF4b/wD71U34b5J0KwAAAABJRU5ErkJggg=='
   ),
   normalFireball2: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAQ1JREFUWEftl+EOwiAMhMH4/q+MlmTQMQ567RL3QxMTKbT39ahmpvR/xR0okRI5kvzN1eKuWq8AQBUvpTG4nIgABNh7qhdg7N4N4wVAgvQ1eACm3XtngQGgu0M26bgV4BCfdm8RQmcsAGZRzzVYABD8LfHHA1zsVzY/x4EIFHUFESFkFwWAikTitwOwX8UVwGUAI52i3BUAytnGGRfeoNq2+5z7A1BkOF0OaHFpYFxLzOoCDaDExILjPYUA7p7CNIClKHPm5wCrR2k4iJs7rwYMVwVNcTkwTv24hmqTjZUDchy6MKnVQtbuJWEHQEMw4laABlE/9H9CreNaSP0wyfK0uViYD4r2oo7eYmqmD6zsX/7gnXOHAAAAAElFTkSuQmCC'
   ),
   normalFireball3: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAOtJREFUWEftltEOhiAIhfNf7//K9uOWMoR5AG/a7CqROJ8Hy67rXMeB48Bx4MMO1B3sJViEi0drNOlfAKCJ19oZUk5EAALM9iMpgB0upADsdeEzXoBVv1fzE5kHQG6+VkxpgwvCAzDRy4ACI1Om8RYAJjwJrAIhgIygBEIB1P7zYlEoFECCm2PvPtgOQGQeiNtcyphQ7S9lnEFR+0lidZItxV9ODYJBmjrZFpiFAWdbShbA9dXToEIA0m451oSsGGKhug+sgm8c6T/lIgCU54JAxT0AHaLdjN8xGvaLv5r/ILQ4KIlpoJsOrvsAjPxQ8Z/pZ88AAAAASUVORK5CYII='
   ),
   blueFireball0: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAARFJREFUWEftloEKwyAMROv+/587Ip3N5E4v0TEYKxTa1ORezioex//KO3DmU+/MkizixbM1qvQjAVDFz7MxLDmRAUgw85QlgB0uRAGW7EY+RAD6ua/1Vl2IAKAGUCzk0hYA4IIMoQJA+2H7weWpAiCtLbHfAXD/QWhlpBzwYqvzkAJgohmwrQAMbBRXAOQlOBJi3xQAljuMg80Jjp8BfLR7I5oBQGoLllLazQYpLowAaPcm7q/+nQGh+AgAja9dX5c9vG4fh3ksGAZghbLxrwPMjtTwP0BzjnbBbrqgSSkHerH+HSqR4MwBS4MukHotrHRvgxWAMIQqHgFoEPXhPna1jmux9/1Bak4a5FTUw6Zc9wmmiF0F4pOO5gAAAABJRU5ErkJggg==',
      { shader: ([ r, g, b, a ]) => [ 0, g, b, a ] }
   ),
   blueFireball1: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAOpJREFUWEftlUsOwyAMRCH3vzMNlSBkAv6MlU1EpC7c2p7nsdumtJ/twHZgO/ABB0pkhhwpPmtHcarXEQD4i5fSGSgnIgAB9quUBcDpaRgWgBbEQi+Atmftc9RPHoDWfGo/e4xWgKnoYxziDSvArfUwLSF5L6EAUDUC5AaQxJg7sAC8tv/qpAbwqrgFANfd45xzaq8xybsG6R9sOX0VxgdvY8h5Jg/F2gpQB2OxOSbP4iiA+6cXISgAtBtjFJFizcLlHYhNrxvR+ic14RRyQViPrw1gAai5fdcru+GbYe1rcqDBWg/OLF4b/wD71U34b5J0KwAAAABJRU5ErkJggg==',
      { shader: ([ r, g, b, a ]) => [ 0, g, b, a ] }
   ),
   blueFireball2: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAQ1JREFUWEftl+EOwiAMhMH4/q+MlmTQMQ567RL3QxMTKbT39ahmpvR/xR0okRI5kvzN1eKuWq8AQBUvpTG4nIgABNh7qhdg7N4N4wVAgvQ1eACm3XtngQGgu0M26bgV4BCfdm8RQmcsAGZRzzVYABD8LfHHA1zsVzY/x4EIFHUFESFkFwWAikTitwOwX8UVwGUAI52i3BUAytnGGRfeoNq2+5z7A1BkOF0OaHFpYFxLzOoCDaDExILjPYUA7p7CNIClKHPm5wCrR2k4iJs7rwYMVwVNcTkwTv24hmqTjZUDchy6MKnVQtbuJWEHQEMw4laABlE/9H9CreNaSP0wyfK0uViYD4r2oo7eYmqmD6zsX/7gnXOHAAAAAElFTkSuQmCC',
      { shader: ([ r, g, b, a ]) => [ 0, g, b, a ] }
   ),
   blueFireball3: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAOtJREFUWEftltEOhiAIhfNf7//K9uOWMoR5AG/a7CqROJ8Hy67rXMeB48Bx4MMO1B3sJViEi0drNOlfAKCJ19oZUk5EAALM9iMpgB0upADsdeEzXoBVv1fzE5kHQG6+VkxpgwvCAzDRy4ACI1Om8RYAJjwJrAIhgIygBEIB1P7zYlEoFECCm2PvPtgOQGQeiNtcyphQ7S9lnEFR+0lidZItxV9ODYJBmjrZFpiFAWdbShbA9dXToEIA0m451oSsGGKhug+sgm8c6T/lIgCU54JAxT0AHaLdjN8xGvaLv5r/ILQ4KIlpoJsOrvsAjPxQ8Z/pZ88AAAAASUVORK5CYII=',
      { shader: ([ r, g, b, a ]) => [ 0, g, b, a ] }
   ),
   orangeFireball0: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAARFJREFUWEftloEKwyAMROv+/587Ip3N5E4v0TEYKxTa1ORezioex//KO3DmU+/MkizixbM1qvQjAVDFz7MxLDmRAUgw85QlgB0uRAGW7EY+RAD6ua/1Vl2IAKAGUCzk0hYA4IIMoQJA+2H7weWpAiCtLbHfAXD/QWhlpBzwYqvzkAJgohmwrQAMbBRXAOQlOBJi3xQAljuMg80Jjp8BfLR7I5oBQGoLllLazQYpLowAaPcm7q/+nQGh+AgAja9dX5c9vG4fh3ksGAZghbLxrwPMjtTwP0BzjnbBbrqgSSkHerH+HSqR4MwBS4MukHotrHRvgxWAMIQqHgFoEPXhPna1jmux9/1Bak4a5FTUw6Zc9wmmiF0F4pOO5gAAAABJRU5ErkJggg==',
      { shader: ([ r, g, b, a ]) => [ r, g / 2, 0, a ] }
   ),
   orangeFireball1: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAOpJREFUWEftlUsOwyAMRCH3vzMNlSBkAv6MlU1EpC7c2p7nsdumtJ/twHZgO/ABB0pkhhwpPmtHcarXEQD4i5fSGSgnIgAB9quUBcDpaRgWgBbEQi+Atmftc9RPHoDWfGo/e4xWgKnoYxziDSvArfUwLSF5L6EAUDUC5AaQxJg7sAC8tv/qpAbwqrgFANfd45xzaq8xybsG6R9sOX0VxgdvY8h5Jg/F2gpQB2OxOSbP4iiA+6cXISgAtBtjFJFizcLlHYhNrxvR+ic14RRyQViPrw1gAai5fdcru+GbYe1rcqDBWg/OLF4b/wD71U34b5J0KwAAAABJRU5ErkJggg==',
      { shader: ([ r, g, b, a ]) => [ r, g / 2, 0, a ] }
   ),
   orangeFireball2: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAQ1JREFUWEftl+EOwiAMhMH4/q+MlmTQMQ567RL3QxMTKbT39ahmpvR/xR0okRI5kvzN1eKuWq8AQBUvpTG4nIgABNh7qhdg7N4N4wVAgvQ1eACm3XtngQGgu0M26bgV4BCfdm8RQmcsAGZRzzVYABD8LfHHA1zsVzY/x4EIFHUFESFkFwWAikTitwOwX8UVwGUAI52i3BUAytnGGRfeoNq2+5z7A1BkOF0OaHFpYFxLzOoCDaDExILjPYUA7p7CNIClKHPm5wCrR2k4iJs7rwYMVwVNcTkwTv24hmqTjZUDchy6MKnVQtbuJWEHQEMw4laABlE/9H9CreNaSP0wyfK0uViYD4r2oo7eYmqmD6zsX/7gnXOHAAAAAElFTkSuQmCC',
      { shader: ([ r, g, b, a ]) => [ r, g / 2, 0, a ] }
   ),
   orangeFireball3: X.imageAsset(
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAOtJREFUWEftltEOhiAIhfNf7//K9uOWMoR5AG/a7CqROJ8Hy67rXMeB48Bx4MMO1B3sJViEi0drNOlfAKCJ19oZUk5EAALM9iMpgB0upADsdeEzXoBVv1fzE5kHQG6+VkxpgwvCAzDRy4ACI1Om8RYAJjwJrAIhgIygBEIB1P7zYlEoFECCm2PvPtgOQGQeiNtcyphQ7S9lnEFR+0lidZItxV9ODYJBmjrZFpiFAWdbShbA9dXToEIA0m451oSsGGKhug+sgm8c6T/lIgCU54JAxT0AHaLdjN8xGvaLv5r/ILQ4KIlpoJsOrvsAjPxQ8Z/pZ88AAAAASUVORK5CYII=',
      { shader: ([ r, g, b, a ]) => [ r, g / 2, 0, a ] }
   )
};

export const whatthewhat = {
   battler: {
      down: new XSprite({
         scale: { x: 0.5, y: 0.5 },
         anchor: { x: 0, y: 0 },
         frames: [ content.soulNormal ]
      }),
      left: new XSprite({
         scale: { x: 0.5, y: 0.5 },
         anchor: { x: 0, y: 0 },
         frames: [ content.soulNormal ]
      }),
      right: new XSprite({
         scale: { x: 0.5, y: 0.5 },
         anchor: { x: 0, y: 0 },
         frames: [ content.soulNormal ]
      }),
      up: new XSprite({
         scale: { x: 0.5, y: 0.5 },
         anchor: { x: 0, y: 0 },
         frames: [ content.soulNormal ]
      })
   },
   battlerHurt: {
      down: new XSprite({
         scale: { x: 0.5, y: 0.5 },
         anchor: { x: 0, y: 0 },
         frames: [ content.soulInvulnerable ]
      }),
      left: new XSprite({
         anchor: { x: 0, y: 0 },
         scale: { x: 0.5, y: 0.5 },
         frames: [ content.soulInvulnerable ]
      }),
      right: new XSprite({
         anchor: { x: 0, y: 0 },
         scale: { x: 0.5, y: 0.5 },
         frames: [ content.soulInvulnerable ]
      }),
      up: new XSprite({
         anchor: { x: 0, y: 0 },
         scale: { x: 0.5, y: 0.5 },
         frames: [ content.soulInvulnerable ]
      })
   },
   frisk: {
      down: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskDown3, content.friskDown0, content.friskDown1, content.friskDown2 ]
      }).wrapOn('tick', bobber),
      left: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskLeft1, content.friskLeft0 ]
      }),
      right: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskRight1, content.friskRight0 ]
      }),
      up: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskUp3, content.friskUp0, content.friskUp1, content.friskUp2 ]
      }).wrapOn('tick', bobber)
   },
   friskSilhouette: {
      down: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [
            content.friskSilhouetteDown3,
            content.friskSilhouetteDown0,
            content.friskSilhouetteDown1,
            content.friskSilhouetteDown2
         ]
      }).wrapOn('tick', bobber),
      left: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskSilhouetteLeft1, content.friskSilhouetteLeft0 ]
      }),
      right: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskSilhouetteRight1, content.friskSilhouetteRight0 ]
      }),
      up: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [
            content.friskSilhouetteUp3,
            content.friskSilhouetteUp0,
            content.friskSilhouetteUp1,
            content.friskSilhouetteUp2
         ]
      }).wrapOn('tick', bobber)
   },
   friskStark: {
      down: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskStarkDown3, content.friskStarkDown0, content.friskStarkDown1, content.friskStarkDown2 ]
      }).wrapOn('tick', bobber),
      left: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskStarkLeft1, content.friskStarkLeft0 ]
      }),
      right: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskStarkRight1, content.friskStarkRight0 ]
      }),
      up: new XSprite({
         anchor: { x: 0, y: 1 },
         step: 3,
         steps: 6,
         frames: [ content.friskStarkUp3, content.friskStarkUp0, content.friskStarkUp1, content.friskStarkUp2 ]
      }).wrapOn('tick', bobber)
   }
};

export const buttons = {
   fight: new XSprite({
      anchor: { y: 1 },
      position: { x: 16, y: 237 },
      scale: { x: 0.5, y: 0.5 },
      frames: [ content.buttonFight0, content.buttonFight1 ]
   }),
   act: new XSprite({
      anchor: { y: 1 },
      position: { x: 92.5, y: 237 },
      scale: { x: 0.5, y: 0.5 },
      frames: [ content.buttonAct0, content.buttonAct1 ]
   }),
   item: new XSprite({
      anchor: { y: 1 },
      position: { x: 172.5, y: 237 },
      scale: { x: 0.5, y: 0.5 },
      frames: [ content.buttonItem0, content.buttonItem1 ]
   }),
   mercy: new XSprite({
      anchor: { y: 1 },
      position: { x: 250, y: 237 },
      scale: { x: 0.5, y: 0.5 },
      frames: [ content.buttonMercy0, content.buttonMercy1 ]
   })
};

export const sprites = {
   hp: new XSprite({
      scale: { x: 0.5, y: 0.5 },
      position: { x: 122, y: 202.5 },
      frames: [ content.hp ]
   }),
   snas: new XSprite({
      anchor: { x: 0, y: 1 },
      step: 3,
      steps: 12,
      frames: [ content.sans3, content.sans0, content.sans1, content.sans2 ]
   }),
   sstriker: new XSprite({
      anchor: { x: 0, y: 0 },
      position: { x: 160, y: 60 },
      scale: { x: 1, y: 1 },
      steps: 3,
      priority: 1000000,
      frames: [
         content.sstriker1,
         content.sstriker2,
         content.sstriker3,
         content.sstriker4,
         content.sstriker5,
         content.sstriker6,
         content.sstriker6
      ]
   }).wrapOn('tick', self => {
      return () => {
         if (self.state.index === 6) {
            renderer.detach('menu', self.disable());
         }
      };
   })
};

function reverb (
   context: AudioContext,
   input: AudioNode,
   output: AudioNode,
   mix: number,
   seconds: number,
   ...points: number[]
) {
   // impulse setup
   let index = -1;
   const size = context.sampleRate * seconds;
   const impulse = context.createBuffer(2, size, context.sampleRate);
   const channels = new Array(impulse.numberOfChannels).fill(0).map((x, index) => impulse.getChannelData(index));
   while (++index < size) {
      for (const channel of channels) {
         channel[index] = (Math.random() * 2 - 1) * X.math.bezier(index / size, ...points);
         channel[index] = (Math.random() * 2 - 1) * X.math.bezier(index / size, ...points);
      }
   }

   // nodes
   const wetNode = context.createGain();
   const dryNode = context.createGain();
   const convolver = context.createConvolver();

   // routing
   input.connect(convolver).connect(wetNode).connect(output);
   input.connect(dryNode).connect(output);

   // mix
   wetNode.gain.value = mix;
   dryNode.gain.value = 1 - mix;
   convolver.buffer = impulse;
}

/** xplayer polyfill */
class OldPlayer {
   daemon: XDaemon;
   instances = [] as XInstance[];
   constructor ({
      buffer,
      volume: gain,
      router,
      rate
   }: {
      buffer: XAudio;
      volume?: number;
      router?: XRouter;
      rate?: number;
   }) {
      this.daemon = X.daemon(buffer, { gain, router, rate });
   }
   get source () {
      return this.instances[this.instances.length - 1];
   }
   start (stop?: boolean) {
      stop && this.stop();
      const instance = this.daemon.instance();
      this.instances.push(instance);
      return instance;
   }
   stop () {
      for (const instance of this.instances.splice(0, this.instances.length)) {
         instance.stop();
      }
   }
}

export const sounds = {
   appear: new OldPlayer({
      buffer: content.sfxAppear,
      volume: 0.5
   }),
   info: new OldPlayer({
      buffer: content.sfxInfo,
      volume: 0.5
   }),
   death: new OldPlayer({
      buffer: content.sfxDeath,
      volume: 0.5
   }),
   shatter: new OldPlayer({
      buffer: content.sfxShatter,
      volume: 0.5
   }),
   swing: new OldPlayer({
      buffer: content.sfxSwing,
      volume: 0.5
   }),
   hit: new OldPlayer({
      buffer: content.sfxHit,
      volume: 0.25
   }),
   battleFall: new OldPlayer({
      buffer: content.sfxBattleFall,
      volume: 0.25
   }),
   bell: new OldPlayer({
      buffer: content.sfxBell,
      volume: 0.3
   }),
   menu: new OldPlayer({
      buffer: content.sfxMenu,
      volume: 0.5,
      router: (context, source) => reverb(context, source, context.destination, 0.1, 0.25, 1, 0, 0)
   }),
   heal: new OldPlayer({
      buffer: content.sfxHeal,
      volume: 0.25,
      router: (context, source) => reverb(context, source, context.destination, 0.1, 0.5, 1, 1, 0)
   }),
   save: new OldPlayer({
      buffer: content.sfxSave,
      volume: 0.25,
      router: (context, source) => reverb(context, source, context.destination, 0.1, 0.5, 1, 1, 0)
   }),
   hurt: new OldPlayer({
      buffer: content.sfxHurt,
      volume: 0.25
   }),
   noise: new OldPlayer({
      buffer: content.sfxNoise,
      volume: 0.25
   }),
   select: new OldPlayer({
      buffer: content.sfxSelect,
      volume: 0.5,
      router: (context, source) => reverb(context, source, context.destination, 0.1, 0.25, 1, 0, 0)
   }),
   box: new OldPlayer({
      buffer: content.sfxBox,
      volume: 0.25,
      router: (context, source) => reverb(context, source, context.destination, 0.1, 0.25, 1, 0, 0)
   }),
   blaster: {
      charge: new OldPlayer({ buffer: content.sfxCharge, volume: 0.25 }),
      fire: new OldPlayer({ buffer: content.sfxFire, volume: 0.25 })
   },
   dialoguer: {
      sans: new OldPlayer({
         buffer: content.sansVoice,
         router: (context, source) => reverb(context, source, context.destination, 0.7, 2, 1, 0, 0)
      }),
      gorey: new OldPlayer({ buffer: content.asgoreVoice }),
      darkflower: new OldPlayer({ buffer: content.floweyVoice }),
      narrator: new OldPlayer({ buffer: content.narratorVoice }),
      tori: new OldPlayer({ buffer: content.torielVoice, rate: 0.8 }),
      storyteller: new OldPlayer({ buffer: content.storyVoice, volume: 0.6 })
   }
};

export const music = {
   intro: new OldPlayer({ buffer: content.introMusic, volume: 0 }),
   birds: new OldPlayer({
      buffer: content.birdsAmbience,
      volume: 0,
      router: (context, source) => reverb(context, source, context.destination, 0.1, 0.25, 1, 0, 0)
   }),
   death: new OldPlayer({ buffer: content.deathMusic }),
   phase1: new OldPlayer({ buffer: content.phase1Music, volume: 0.5 })
};

export const bullets = {
   normalFireball: [ content.normalFireball0, content.normalFireball1, content.normalFireball2, content.normalFireball3 ],
   blueFireball: [ content.blueFireball0, content.blueFireball1, content.blueFireball2, content.blueFireball3 ],
   orangeFireball: [ content.orangeFireball0, content.orangeFireball1, content.orangeFireball2, content.orangeFireball3 ]
};
