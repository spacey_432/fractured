import { global, swapSprites } from './core';
import { AzzyBlaster, Firebol } from './bullets';
import * as dialogue from './dialogue';
import { random } from './random';
import { game, renderer } from './core';
import { atlas } from './menu';
import { dialoguer } from './dialoguer';
import { manager } from './data';
import { content, music, sounds, sprites, whatthewhat } from './assets';
import { X, X2, XInput, XRectangle, XRenderer, XSprite, XVector } from './storyteller';
import { XBattler, XWalker } from './undertale';
import { player } from './player';

const deathLoader = Promise.all([ content.deathMusic.load(), content.asgoreVoice.load() ]);
const phase1Loader = content.phase1Music.load();

let dead = false;
let toriCheckCount = 0;

type TorielOptions = 'FIGHT' | 'FIGHT2' | 'ACT' | 'ACT2' | 'check' | 'ITEM' | 'ITEM2' | 'MERCY' | 'spare' | 'flee';
type TorielPhases = 'mercy' | 'battle1';

const extraDialogues = {
   pacifistBetrayEarly: [
      '{#s:isVengeful}Hee hee hee... that is what I thought.',
      '{#s:isVengefulClosedEyes}As if your little spare-then-fight trick was going to work on me again...',
      '{#s:isVengeful}How foolish of you.'
   ],
   pacifistBetrayExtra: [
      '{#s:HardTruthOpenEyes}Hmm, where were we again?',
      '{#s:HardTruth}...',
      '{#s:HardTruth}...',
      '{#s:HardTruth}......',
      '{#s:respectFace}Oh, of course.',
      '{#s:respectYou}Forgive me, human...',
      '{#s:isNeutralClosedEyes}It is difficult to remember a conversation...',
      '{#s:isVengefulClosedEyes}When you are busy dealing with such a terrible creature.'
   ],
   fightResumeText: [
      '{#s:HardTruth}As I was saying, you killed Asgore and Undyne.',
      "{#s:isMad}But, that's not all, is it?",
      '{#s:HardTruthClosed}...',
      '{#s:isMad}No. There was... a skeleton. A skeleton named Sans.'
   ],
   pacifistBetrayPreProposal: [
      '{#s:isMadClosedEyes}I was foolish to let my guard down...',
      '{#s:isVengefulClosedEyes}Heheheh....',
      '{#s:isVengeful}It will not happen again.'
   ],
   pacifistBetrayPostProposal: [
      '{#s:hitShock}You... you...',
      '{#s:hitShock}You were planning this all along, were you not?',
      '{#s:hitShockClosedEyes}...',
      '{#s:hitSmile1}Ha...',
      '{#s:hitSmile2}Hahaha...'
   ],
   pacifistBetrayLastLaugh: [
      '{#s:hitSmile3}HAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHA'
   ],
   prePhase2: [
      'To think I would almost die after one strike.',
      'Well, it does not matter. It will not change the outcome of this fight.',
      '...',
      'I have to do what I planned to do from the start.',
      'This ends here and now.'
   ],
   pacifistEnding: [
      '{#s:respectYou}Thank you, my child.',
      '{#s:respectFace}...',
      '{#s:sadgoatO}Please...',
      '{#s:sadgoat}Just get it over with.'
   ]
};

export const mamaLoadouts = {
   isMad: {
      arm0: 0,
      arm1: 0,
      brow: 2,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   isVengeful: {
      arm0: 0,
      arm1: 0,
      brow: 2,
      eyes: 1,
      mouth: 1,
      xtra: 0
   },
   isVengefulClosedEyes: {
      arm0: 0,
      arm1: 0,
      brow: 2,
      eyes: 0,
      mouth: 1,
      xtra: 0
   },
   hitShock: {
      arm0: 0,
      arm1: 0,
      brow: 5,
      eyes: 3,
      mouth: 5,
      xtra: 0
   },
   hitShockClosedEyes: {
      arm0: 0,
      arm1: 0,
      brow: 5,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   hitSmile1: {
      arm0: 0,
      arm1: 0,
      brow: 5,
      eyes: 0,
      mouth: 2,
      xtra: 0
   },
   hitSmile2: {
      arm0: 0,
      arm1: 0,
      brow: 5,
      eyes: 3,
      mouth: 4,
      xtra: 0
   },
   hitSmile3: {
      arm0: 0,
      arm1: 0,
      brow: 5,
      eyes: 3,
      mouth: 3,
      xtra: 0
   },
   wat: {
      arm0: 0,
      arm1: 0,
      brow: 5,
      eyes: 3,
      mouth: 6,
      xtra: 0
   },
   isMadClosedEyes: {
      arm0: 0,
      arm1: 0,
      brow: 2,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   isNeutral: {
      arm0: 0,
      arm1: 0,
      brow: 9,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   isNeutralClosedEyes: {
      arm0: 0,
      arm1: 0,
      brow: 9,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   isPissed: {
      arm0: 0,
      arm1: 0,
      brow: 1,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   isPissedClosedEyes: {
      arm0: 0,
      arm1: 0,
      brow: 1,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   isHurt: {
      arm0: 0,
      arm1: 0,
      brow: 1,
      eyes: 0,
      mouth: 5,
      xtra: 0
   },
   isMadAtYou: {
      arm0: 0,
      arm1: 0,
      brow: 1,
      eyes: 1,
      mouth: 5,
      xtra: 0
   },
   isNotAsMad: {
      arm0: 0,
      arm1: 0,
      brow: 9,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   isREALLYFUCKINGMADOHGOD: {
      arm0: 0,
      arm1: 0,
      brow: 1,
      eyes: 1,
      mouth: 5,
      xtra: 0
   },
   huh: {
      arm0: 0,
      arm1: 0,
      brow: 10,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   huhWTF: {
      arm0: 0,
      arm1: 0,
      brow: 9,
      eyes: 5,
      mouth: 6,
      xtra: 0
   },
   hmmWTF: {
      arm0: 0,
      arm1: 0,
      brow: 10,
      eyes: 5,
      mouth: 6,
      xtra: 0
   },
   hmmWTFno: {
      arm0: 0,
      arm1: 0,
      brow: 2,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   isTired: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   respectFace: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 0,
      mouth: 1,
      xtra: 0
   },
   respectSad: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   respectSadClosed: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   respectYou: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 1,
      mouth: 1,
      xtra: 0
   },
   hopeRestored: {
      arm0: 0,
      arm1: 0,
      brow: 8,
      eyes: 2,
      mouth: 5,
      xtra: 0
   },
   HopeAccepted: {
      arm0: 0,
      arm1: 0,
      brow: 8,
      eyes: 2,
      mouth: 2,
      xtra: 0
   },
   happygoat: {
      arm0: 0,
      arm1: 0,
      brow: 8,
      eyes: 2,
      mouth: 1,
      xtra: 0
   },
   happygoatC: {
      arm0: 0,
      arm1: 0,
      brow: 8,
      eyes: 0,
      mouth: 1,
      xtra: 0
   },
   happygoatD: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 0,
      mouth: 1,
      xtra: 0
   },
   sadgoat: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   sadgoatO: {
      arm0: 0,
      arm1: 0,
      brow: 3,
      eyes: 1,
      mouth: 0,
      xtra: 0
   },
   HardTruth: {
      arm0: 0,
      arm1: 0,
      brow: 10,
      eyes: 0,
      mouth: 0,
      xtra: 0
   },
   HardTruthOpenEyes: {
      arm0: 0,
      arm1: 0,
      brow: 10,
      eyes: 1,
      mouth: 0,
      xtra: 0
   }
};

export function loadout (name: keyof typeof mamaLoadouts) {
   if (name in mamaLoadouts) {
      const mamaLoadout = mamaLoadouts[name];
      for (const key in mamaLoadout) {
         TORIEL.body[key as keyof typeof mamaLoadout] = mamaLoadout[key as keyof typeof mamaLoadout];
      }
   }
}

export const battler = new XBattler<TorielOptions>({
   menu: [ 'FIGHT', 'ACT', 'ITEM', 'MERCY' ],
   choices: {
      FIGHT: [ 'FIGHT2' ],
      async FIGHT2 () {
         TORIEL.destroy();
         atlas.switch(null);
         let hit = 0;
         const input = new XInput({ codes: [ 'z', 'Z', 'Enter' ] });
         const deadeyeTarget = new XSprite({
            anchor: { x: 0, y: 0 },
            position: { x: 160, y: 160 },
            scale: { x: 0.5, y: 0.5 },
            frames: [ content.deadeye ]
         });
         const hitRect = new XRectangle({
            fill: 'white',
            stroke: 'black',
            priority: 5,
            anchor: { y: 1 },
            position: { x: 35.5, y: 191.75 },
            size: { x: 5.5, y: 63.5 },
            line: {
               width: 1.5
            }
         });
         hitRect.alpha.value = 1;
         deadeyeTarget.alpha.value = 1;
         renderer.attach('foreground', deadeyeTarget, hitRect);
         const destie = { x: 295.5, y: 191.75 };
         await Promise.race([
            hitRect.position.modulate(1250, hitRect.position.value(), destie, destie).then(() => {
               hit === 0 && (hit = 1);
            }),
            input.on('down').then(async () => {
               if (hit === 0) {
                  hit = 2;
                  X.pause(250).then(async () => {
                     sounds.swing.start();
                     sprites.sstriker.reset();
                     renderer.attach('menu', sprites.sstriker.enable());
                     await X.pause(775);
                     sounds.hit.start();
                     renderer.shake.value = 5;
                     renderer.shake.modulate(200, 5, 0, 0);
                     loadout('isHurt');
                     X.pause(650).then(() => {
                        loadout('isMadClosedEyes');
                     });
                     let alt = -1;
                     const frame = 20;
                     const duration = 500;
                     const increment = duration / frame;
                     const maxOffset = 10;
                     let amount = increment;
                     while (amount > 0) {
                        TORIEL.body.base.position.x =
                           160 + X.math.bezier(--amount / increment, 0, 0, 1) * maxOffset * (alt *= -1);
                        await X.pause(frame);
                     }
                  });
               }
            })
         ]);
         if (hit === 2) {
            TORIEL.state.attack = true;
            await hitRect.position.modulate(0);
            let runs = 0;
            while (runs < 4) {
               hitRect.fill = 'black';
               hitRect.stroke = 'white';
               await X.pause(70);
               hitRect.fill = 'white';
               hitRect.stroke = 'black';
               await X.pause(70);
               runs++;
            }
            await X.pause(750);
         }
         await Promise.all([
            hitRect.alpha.modulate(300, 0),
            deadeyeTarget.scale.modulate(700, { x: 0, y: 0.5 }),
            X.pause(100).then(async () => await deadeyeTarget.alpha.modulate(400, 0))
         ]);
         renderer.detach('foreground', deadeyeTarget, hitRect);
      },
      ACT: [ 'ACT2' ],
      ACT2: [ 'check' ],
      async check () {
         TORIEL.destroy();
         atlas.switch(null);
         let textContent: string[];
         switch (toriCheckCount++) {
            case 0:
               textContent = [
                  '{#c:storyteller}{#i:40}* TORIEL - ATK 99 DEF 99\n* The fractured queen of a broken\n  kingdom.',
                  '* Once knew what was best for\n  you...{^10} still does.'
               ];
               break;
            case 1:
               textContent = [
                  '{#c:storyteller}{#i:40}* TORIEL - ATK 99 DEF 99\n* Could she really get any more\n  desperate than this?'
               ];
               break;
            default:
               textContent = [
                  "{#c:storyteller}{#i:40}* TORIEL - ATK 99 DEF 99\n* There's nothing else of\n  interest here."
               ];
               break;
         }
         const text = dialoguer.text(...textContent);
         atlas.switch('opponentDialoguer');
         await text;
         atlas.switch(null);
      },
      ITEM: Array(8)
         .fill(0)
         .map(() => 'ITEM2'),
      async ITEM2 () {
         TORIEL.destroy();
         atlas.switch(null);
         sounds.heal.start();
         const text = manager.activate(global.item, 'use');
         const textPromise = dialoguer.text(...(typeof text === 'function' ? text() : text));
         atlas.switch('opponentDialoguer');
         await textPromise;
         atlas.switch(null);
         // eat shit (literally) (sorta) (not really)
      },
      MERCY: [ 'spare', 'flee' ],
      async spare () {
         TORIEL.destroy();
         atlas.switch(null);
      },
      async flee () {
         TORIEL.destroy();
         atlas.switch(null);
         // run away like a sussy baka...
      }
   },
   async attack (choice) {
      if (choice === 'flee') {
         location.href = 'https://spacey-432.itch.io/outertale';
         await new Promise(resolve => {});
      }
      const state = TORIEL.state;
      const action = state.attack ? 'attack' : [ 'FIGHT2', 'spare' ].includes(choice) ? 'spare' : 'other';
      state.attack = false;
      if (action === 'spare') {
         // spare 5x for mercy route
         state.mercy && state.phase === 'battle1' && ++state.progress.mercy > 4 && (state.phase = 'mercy');
      } else if (action === 'attack') {
         // any attack aborts the mercy ending
         state.mercy = false;
         if (state.phase === 'mercy') {
            // mercy phase betrayals
            if (state.progress.mercy < 18) {
               // early betrayal, return to phase 1
               state.phase = 'battle1';
               await TORIEL.speak(...extraDialogues.pacifistBetrayEarly);
               // toriel can be FORGETFUL sometimes?? (hub cinema title xd)
               if (state.progress.mercy > 10) {
                  await X.pause(2000);
                  await TORIEL.speak(...extraDialogues.pacifistBetrayExtra);
                  await X.pause(1000);
               }
               await TORIEL.speak(...extraDialogues.fightResumeText);
               // remove first line of phase 1 dialogue #5 (then there was a skeleton named sans)
               dialogue.battle1[5].splice(0, 1);
            } else if (state.progress.mercy < 22) {
               // partial betrayal, skip to phase 2
               await TORIEL.speak(...extraDialogues.pacifistBetrayPreProposal);
               return false;
            } else {
               // total betrayal, end battle
               await TORIEL.speak(...extraDialogues.pacifistBetrayPostProposal);
               await X.pause(1000);
               await TORIEL.speak('{#s:wat}...');
               TORIEL.speak(...extraDialogues.pacifistBetrayLastLaugh);
               await X.pause(5000);
               return false;
            }
         }
      }
      if (action === 'other' || (await TORIEL.advance())) {
         state.progress.mercy > 18 || (TORIEL.body.eyes = 1);
         state.progress.mercy > 18 || (TORIEL.body.mouth = 0);
         TORIEL.spawn();
         Object.assign(game.player.position, { x: 156, y: 82 });
         const stateKey = `${state.phase}:${state.progress[state.phase]}`;
         const r = () => random(stateKey.replace('mercy', 'battle1'));
         switch (stateKey) {
            case 'battle1:0': {
               await TORIEL.resize({ x: 100, y: 80 }, 300);
               await Firebol.sequence(35, async (promises, index) => {
                  promises.push(
                     Firebol.simple({
                        scale: 0.8,
                        position: { x: 160 + (r() * 2 * 60 - 60), y: 30 },
                        type: 'white',
                        damage: 5,
                        async handler (flame) {
                           await Promise.all([ flame.alpha.modulate(100, 1), flame.scale.modulate(150, { x: 1, y: 1 }) ]);
                           await flame.position.modulate(
                              500,
                              flame.position.value(),
                              flame.position.endpoint(
                                 game.player.position.direction(flame.position) + (r() * 2 * 5 - 5),
                                 300
                              )
                           );
                        }
                     })
                  );
                  await X.pause(250);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:12':
            case 'battle1:1': {
               const blaster = new AzzyBlaster({ metadata: { kr: true } });
               await TORIEL.resize({ x: 100, y: 80 }, 300);
               await Firebol.sequence(35, async (promises, index) => {
                  if (index % 10 === 0) {
                     const angle = Math.floor(r() * 4) * 90;
                     blaster.position = game.player.position.endpoint(angle, 100);
                     blaster.rotation.value = angle + 90;
                     promises.push(
                        blaster.activate(
                           renderer,
                           'foreground',
                           { x: blaster.position.x < 160 ? -200 : 200, y: blaster.position.y < 120 ? -200 : 200 },
                           0,
                           500,
                           100,
                           500,
                           1000,
                           1000,
                           100
                        )
                     );
                  } else {
                     promises.push(
                        Firebol.simple({
                           scale: 0.8,
                           position: { x: 160 + (r() * 2 * 60 - 60), y: 30 },
                           type: 'blue',
                           damage: 5,
                           async handler (flame) {
                              await Promise.all([
                                 flame.alpha.modulate(100, 1),
                                 flame.scale.modulate(150, { x: 1, y: 1 })
                              ]);
                              await flame.position.modulate(
                                 500,
                                 flame.position.value(),
                                 flame.position.endpoint(
                                    game.player.position.direction(flame.position) + (r() * 2 * 5 - 5),
                                    300
                                 )
                              );
                           }
                        })
                     );
                  }
                  await X.pause(250);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:13':
            case 'battle1:2': {
               await TORIEL.resize({ x: 50, y: 100 }, 100);
               await Firebol.sequence(10, async (promises, index) => {
                  // skip some random numbers for harder start pattern
                  r();
                  r();
                  r();
                  r();
                  const chosen = Math.floor(r() * 7);
                  const direction = index % 2 === 0 ? 1 : -1;
                  promises.push(
                     Firebol.sequence(7, async (promises, index) => {
                        promises.push(
                           Firebol.simple({
                              sound: true,
                              damage: 10,
                              position: { y: 90 + index * 15, x: 160 + direction * 70 },
                              type: index === chosen ? 'orange' : 'white',
                              async handler (garbo) {
                                 const base = garbo.position.x;
                                 const destie = { x: base - direction * 100, y: garbo.position.y };
                                 await garbo.position.modulate(
                                    1500,
                                    { x: base + direction * 10, y: garbo.position.y },
                                    destie,
                                    destie
                                 );
                              }
                           })
                        );
                        await X.pause(20);
                     })
                  );
                  await X.pause(1e3);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:14':
            case 'battle1:3': {
               await TORIEL.resize({ x: 100, y: 40 }, 300);
               await Firebol.sequence(9, async (promises, index) => {
                  promises.push(
                     Firebol.simple({
                        scale: 1.3,
                        position: { x: 160 + ((r() < 0.5 ? -1 : 1) * 90 + (r() * 2 * 10 - 10)), y: -30 },
                        damage: 10,
                        trail: true,
                        async handler (flame) {
                           await flame.position.modulate(800, { x: flame.position.x, y: r() * 30 + 180 });
                           // sounds.boom.start();
                           promises.push(
                              Firebol.sequence(25, async (promises, index) => {
                                 promises.push(
                                    Firebol.simple({
                                       scale: 0.5 + r() * 0.2,
                                       position: flame.position.value(),
                                       damage: 8,
                                       async handler (extra) {
                                          const destination = extra.position.endpoint((index / 25) * 360, 300);
                                          await extra.position.modulate(3000 / (1 + r() * 3), destination);
                                       }
                                    })
                                 );
                              })
                           );
                        }
                     })
                  );
                  await X.pause(1e3);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:15':
            case 'battle1:4': {
               await TORIEL.resize({ x: 120, y: 120 }, 300);
               const center = new XVector(160, 133.75);
               const baseTop = center.endpoint(-90, 80).endpoint(180, 60);
               const baseLeft = center.endpoint(180, 80).endpoint(-90, 60);
               await Firebol.sequence(8, async (promises, index) => {
                  const destieTop = baseTop.endpoint(0, r() * 120);
                  const destieLeft = baseLeft.endpoint(90, r() * 120);
                  const arcPos = new XVector(destieTop.x, destieLeft.y);
                  const indicatorTop = Firebol.simple({
                     trail: true,
                     position: destieTop.endpoint(-90 + (r() * 2 * 5 - 5), 150),
                     async handler (d) {
                        await d.position.modulate(750, d.position.value(), destieTop, destieTop);
                        await X.pause(200);
                        d.alpha.value = 0;
                        await X.pause(60);
                        d.alpha.value = 1;
                        await X.pause(60);
                        d.alpha.value = 0;
                        await X.pause(60);
                        d.alpha.value = 1;
                        await X.pause(60);
                        d.alpha.value = 0;
                     }
                  });
                  const indicatorLeft = Firebol.simple({
                     trail: true,
                     position: destieLeft.endpoint(180 + (r() * 2 * 5 - 5), 150),
                     async handler (d) {
                        await d.position.modulate(750, d.position.value(), destieLeft, destieLeft);
                        sounds.noise.start();
                        await X.pause(200);
                        d.alpha.value = 0;
                        await X.pause(60);
                        sounds.noise.start();
                        d.alpha.value = 1;
                        await X.pause(60);
                        d.alpha.value = 0;
                        await X.pause(60);
                        sounds.noise.start();
                        d.alpha.value = 1;
                        await X.pause(60);
                        d.alpha.value = 0;
                     }
                  });
                  promises.push(
                     Promise.all([ indicatorTop, indicatorLeft ]).then(() => {
                        Firebol.sequence(4, async (promises, index1) => {
                           promises.push(
                              Firebol.sequence(30, async (promises, index2) => {
                                 promises.push(
                                    Firebol.simple({
                                       damage: 8,
                                       position: arcPos.endpoint(index1 * 90 + 360 * (index2 / 30), 25),
                                       async handler (p) {
                                          const finalDestie = p.position.add(p.position.subtract(arcPos).multiply(10));
                                          await p.position.modulate(500, p.position.value(), finalDestie, finalDestie);
                                          await p.alpha.modulate(200, 0);
                                       }
                                    })
                                 );
                                 await X.pause(20);
                              })
                           );
                        });
                     })
                  );
                  await X.pause(2000);
               });
               await X.pause(1000);
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:16':
            case 'battle1:5': {
               await TORIEL.resize({ x: 40, y: 20 }, 100);
               await Firebol.sequence(30, async (promises, index) => {
                  let creep = 60;
                  const side = r() < 0.5 ? -1 : 1;
                  promises.push(
                     Firebol.simple({
                        damage: 8,
                        position: { x: 160 + side * 10, y: (creep += 5) },
                        scale: 0.8,
                        trail: true,
                        sound: true,
                        async handler (x) {
                           const destie = x.position.endpoint(90, 250);
                           await x.position.modulate(1200, x.position.value(), destie);
                        }
                     })
                  );
                  await X.pause(250);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:6':
            case 'battle1:6': {
               /*
               await TORIEL.resize({ x: 100, y: 100 }, 100);
               const offset = new XNumber();
               const center = new XVector(160, 143.75);
               const baseAngle = r() * 360;
               Firebol.sequence(5, async (promises, index1) => {
                  const startAngle = baseAngle + index1 * (360 / 5);
                  Firebol.sequence(4, async (promises, index2) => {
                     Firebol.simple({
                        sound: index1 === 0,
                        scale: 0.6,
                        damage: 6,
                        async handler (bullet) {
                           bullet.on('tick', () => {
                              bullet.position = center.endpoint(startAngle + offset.value, 10 + (3 - index2) * 15);
                              bullet.metadata.damage = 3;
                           });
                           while (offset.value < 1500) {
                              await X.pause(50);
                           }
                        }
                     });
                     await X.pause(200);
                  });
               });
               await X.pause(800);
               await offset.modulate(10000, 0, 1500, 1500);
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
               */
               await TORIEL.resize({ x: 100, y: 80 }, 300);
               await Firebol.sequence(35, async (promises, index) => {
                  promises.push(
                     Firebol.simple({
                        scale: 0.8,
                        position: { x: 160 + (r() * 2 * 60 - 60), y: 30 },
                        type: 'white',
                        damage: 5,
                        async handler (flame) {
                           await Promise.all([ flame.alpha.modulate(100, 1), flame.scale.modulate(150, { x: 1, y: 1 }) ]);
                           await flame.position.modulate(
                              500,
                              flame.position.value(),
                              flame.position.endpoint(
                                 game.player.position.direction(flame.position) + (r() * 2 * 5 - 5),
                                 300
                              )
                           );
                        }
                     })
                  );
                  await X.pause(250);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:7':
            case 'battle1:7': {
               await TORIEL.resize({ x: 80, y: 100 }, 100);
               await Firebol.sequence(10, async (promises, index) => {
                  // skip some random numbers for harder start pattern
                  r();
                  r();
                  r();
                  r();
                  const chosen = Math.floor(r() * 7);
                  const direction = index % 2 === 0 ? 1 : -1;
                  promises.push(
                     Firebol.sequence(7, async (promises, index) => {
                        promises.push(
                           Firebol.simple({
                              sound: true,
                              damage: 10,
                              position: { y: 90 + (90 - index * 15), x: 160 + direction * 70 },
                              type: index === chosen ? 'orange' : 'white',
                              async handler (garbo) {
                                 const base = garbo.position.x;
                                 const destie = { x: base - direction * 100, y: garbo.position.y };
                                 await garbo.position.modulate(
                                    1400,
                                    { x: base + direction * 10, y: garbo.position.y },
                                    destie,
                                    destie
                                 );
                              }
                           })
                        );
                        await X.pause(20);
                     })
                  );
                  await X.pause(800);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:8':
            case 'battle1:8': {
               await TORIEL.resize({ x: 110, y: 100 }, 100);
               const blaster = new AzzyBlaster({
                  metadata: { kr: true },
                  rotation: 180,
                  position: { x: 160, y: 220 },
                  priority: 10,
                  scale: { x: 1.2, y: 1 }
               });
               X.pause(1350).then(() => {
                  Firebol.sequence(70, async (promises, index) => {
                     const baseY = r() * 90;
                     Firebol.simple({
                        position: { x: 160, y: baseY },
                        scale: 0.5,
                        damage: 4,
                        async handler (eeee) {
                           let tick = 0;
                           const arcSpeed = 2 + r() * 3;
                           const motionDirection = r() < 0.5 ? 1 : -1;
                           while (tick < 3500) {
                              await eeee.on('tick');
                              eeee.position.x += arcSpeed * motionDirection * 0.25;
                              eeee.position.y =
                                 X.math.bezier(
                                    (tick / 3500) * 0.6 + 0.4,
                                    baseY + 1000,
                                    baseY - 200,
                                    baseY - 200,
                                    baseY + 1000
                                 ) - 100;
                              tick += 1000 / 30;
                           }
                        }
                     });
                     await X.pause(9000 / 70);
                  });
               });
               await blaster.activate(
                  renderer,
                  'foreground',
                  { x: blaster.position.x, y: 200 },
                  0,
                  300,
                  1000,
                  10000,
                  100,
                  10,
                  50
               );
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:9':
            case 'battle1:9': {
               game.player.position.x = 60;
               let done = 0;
               const upper = 95 + 7 * 15;
               await TORIEL.resize({ x: 250, y: 80 }, 300);
               await Firebol.sequence(8, async (promises, index1) => {
                  const chosen = index1 === 0 ? 0 : Math.floor(r() * 7);
                  promises.push(
                     Firebol.sequence(index1 === 0 ? 6 : 7, async (promises, index2) => {
                        const down = index1 % 2 === 0;
                        promises.push(
                           Firebol.simple({
                              scale: 0.6,
                              damage: 7,
                              trail: index1 === 0,
                              type: index1 === 0 ? 'white' : chosen === index2 ? 'orange' : 'white',
                              position: {
                                 x: index1 === 0 ? 20 : 70 + index1 * 25,
                                 y: 110 + (down ? index2 : 7 - index2) * 15
                              },
                              async handler (garbo) {
                                 if (index1 === 0) {
                                    await garbo.position.modulate(5500, { x: 260, y: garbo.position.y });
                                    done++;
                                 } else {
                                    garbo.on('tick', () => {
                                       garbo.position.y += down ? 1.5 : -1.5;
                                       if (down && garbo.position.y > upper) {
                                          garbo.position.y = 90;
                                       } else if (!down && garbo.position.y < 95) {
                                          garbo.position.y = upper;
                                       }
                                       garbo.alpha.value =
                                          1 - (Math.max(45, Math.abs(garbo.position.y - 155)) - 45) / 15;
                                    });
                                 }
                                 while (done < 6) {
                                    await X.pause(20);
                                 }
                              }
                           })
                        );
                     })
                  );
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:10':
            case 'battle1:10': {
               await TORIEL.resize({ x: 150, y: 60 }, 300);
               await Firebol.sequence(15, async (promises, index) => {
                  promises.push(
                     Firebol.simple({
                        sound: true,
                        damage: 6,
                        position: { x: 40, y: 0 },
                        async handler (dooberBoober) {
                           const speed = 1 + r() * 2;
                           const inertia = 60 + Math.floor(r() * 20);
                           const startTime = Date.now();
                           while (Date.now() - startTime < 5000) {
                              await dooberBoober.on('tick');
                              dooberBoober.position.x += speed;
                              dooberBoober.position.y = X.math.bezier(
                                 (dooberBoober.position.x % inertia) / inertia,
                                 190,
                                 100,
                                 100,
                                 190
                              );
                           }
                        }
                     })
                  );
                  await X.pause(543);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:11':
            case 'battle1:11': {
               game.player.position.x = 90;
               await TORIEL.resize({ x: 165, y: 80 }, 300);
               let index = 0;
               let phase = 1;
               let sussy = '';
               const amogus: X2[] = [];
               const carver = { x: 0, y: Math.floor(r() * 6) };
               while (carver.x < 8) {
                  amogus.push(Object.assign({}, carver));
                  const dirs = [
                     'x',
                     ...(sussy !== 'b' && carver.y > 0 ? [ 'a' ] : []),
                     ...(sussy !== 'a' && carver.y < 5 ? [ 'b' ] : [])
                  ];
                  const selectedDir = dirs[Math.floor(r() * dirs.length)];
                  Object.assign(carver, {
                     x: carver.x + (selectedDir === 'x' ? 1 : 0),
                     y: carver.y + ({ a: -1, b: 1 }[selectedDir] || 0)
                  });
                  sussy = selectedDir;
               }
               await X.pause(1000);
               const zestie = Date.now();
               const meDaChungusNahYouDaChungusYouGotDaWorstSmellinNutCheeseFungusAmogus = amogus.length * 500;
               await Firebol.sequence(8, async (promises, x) => {
                  promises.push(
                     Firebol.sequence(6, async (promises, y) => {
                        promises.push(
                           Firebol.simple({
                              scale: 0.6,
                              damage: 6,
                              position: { x: 107.5 + x * 15, y: 110 + y * 15 },
                              async handler (garbo) {
                                 async function xe () {
                                    await garbo.on('tick');
                                    if (phase < 3) {
                                       const position = amogus[index];
                                       if (position.x === x && position.y === y) {
                                          sounds.noise.start();
                                          garbo.type = phase === 1 ? 'blue' : 'orange';
                                          await X.pause(phase === 1 ? 100 : 300);
                                          if (++index === amogus.length) {
                                             index = 0;
                                             if (++phase === 2) {
                                                new AzzyBlaster({
                                                   position: { x: 90, y: 60 },
                                                   scale: { y: 1.3 }
                                                }).activate(
                                                   renderer,
                                                   'foreground',
                                                   { x: 0, y: -200 },
                                                   360,
                                                   300,
                                                   700,
                                                   3500,
                                                   300,
                                                   200,
                                                   75
                                                );
                                             }
                                          }
                                          X.pause(phase === 1 ? 100 : 600).then(() => {
                                             garbo.type = 'white';
                                          });
                                       }
                                       xe();
                                    }
                                 }
                                 xe();
                                 while (
                                    Date.now() - zestie <
                                    meDaChungusNahYouDaChungusYouGotDaWorstSmellinNutCheeseFungusAmogus
                                 ) {
                                    await X.pause(20);
                                 }
                              }
                           })
                        );
                     })
                  );
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'mercy:18':
            case 'battle1:12': {
               if (stateKey === 'mercy:18') {
                  await TORIEL.resize({ x: 120, y: 120 }, 300);
                  const center = new XVector(160, 133.75);
                  const baseTop = center.endpoint(-90, 80).endpoint(180, 60);
                  const baseLeft = center.endpoint(180, 80).endpoint(-90, 60);
                  await Firebol.sequence(16, async (promises, index) => {
                     const destieTop = baseTop.endpoint(0, r() * 120);
                     const destieLeft = baseLeft.endpoint(90, r() * 120);
                     const arcPos = new XVector(destieTop.x, destieLeft.y);
                     const indicatorTop = Firebol.simple({
                        trail: true,
                        position: destieTop.endpoint(-90 + (r() * 2 * 5 - 5), 150),
                        async handler (d) {
                           await d.position.modulate(750, d.position.value(), destieTop, destieTop);
                           await X.pause(200);
                           d.alpha.value = 0;
                           await X.pause(60);
                           d.alpha.value = 1;
                           await X.pause(60);
                           d.alpha.value = 0;
                           await X.pause(60);
                           d.alpha.value = 1;
                           await X.pause(60);
                           d.alpha.value = 0;
                        }
                     });
                     const indicatorLeft = Firebol.simple({
                        trail: true,
                        position: destieLeft.endpoint(180 + (r() * 2 * 5 - 5), 150),
                        async handler (d) {
                           await d.position.modulate(750, d.position.value(), destieLeft, destieLeft);
                           sounds.noise.start();
                           await X.pause(200);
                           d.alpha.value = 0;
                           await X.pause(60);
                           sounds.noise.start();
                           d.alpha.value = 1;
                           await X.pause(60);
                           d.alpha.value = 0;
                           await X.pause(60);
                           sounds.noise.start();
                           d.alpha.value = 1;
                           await X.pause(60);
                           d.alpha.value = 0;
                        }
                     });
                     promises.push(
                        Promise.all([ indicatorTop, indicatorLeft ]).then(() => {
                           Firebol.sequence(4, async (promises, index1) => {
                              promises.push(
                                 Firebol.sequence(30, async (promises, index2) => {
                                    promises.push(
                                       Firebol.simple({
                                          damage: 8,
                                          position: arcPos.endpoint(index1 * 90 + 360 * (index2 / 30), 25),
                                          async handler (p) {
                                             const finalDestie = p.position.add(
                                                p.position.subtract(arcPos).multiply(10)
                                             );
                                             await p.position.modulate(
                                                500,
                                                p.position.value(),
                                                finalDestie,
                                                finalDestie
                                             );
                                             await p.alpha.modulate(200, 0);
                                          }
                                       })
                                    );
                                    await X.pause(20);
                                 })
                              );
                           });
                        })
                     );
                     await X.pause(1800);
                  });
                  await X.pause(1000);
                  await TORIEL.resize({ x: 60, y: 100 }, 100);
                  await Firebol.sequence(7, async (promises, index) => {
                     const chosen = Math.floor(r() * 7);
                     const direction = index % 2 === 0 ? 1 : -1;
                     promises.push(
                        Firebol.sequence(7, async (promises, index) => {
                           promises.push(
                              Firebol.simple({
                                 sound: true,
                                 damage: 10,
                                 position: { y: 90 + index * 15, x: 160 + direction * 70 },
                                 type: index === chosen ? 'orange' : 'white',
                                 async handler (garbo) {
                                    const base = garbo.position.x;
                                    const destie = { x: base - direction * 100, y: garbo.position.y };
                                    await garbo.position.modulate(
                                       1500,
                                       { x: base + direction * 10, y: garbo.position.y },
                                       destie,
                                       destie
                                    );
                                 }
                              })
                           );
                           await X.pause(20);
                        })
                     );
                     await X.pause(900);
                  });
                  await TORIEL.resize({ x: 90, y: 100 }, 100);
                  await Firebol.sequence(6, async (promises, index) => {
                     const chosen = Math.floor(r() * 7);
                     const direction = index % 2 === 0 ? 1 : -1;
                     promises.push(
                        Firebol.sequence(7, async (promises, index) => {
                           promises.push(
                              Firebol.simple({
                                 sound: true,
                                 damage: 10,
                                 position: { y: 90 + (90 - index * 15), x: 160 + direction * 70 },
                                 type: index === chosen ? 'orange' : 'white',
                                 async handler (garbo) {
                                    const base = garbo.position.x;
                                    const destie = { x: base - direction * 100, y: garbo.position.y };
                                    await garbo.position.modulate(
                                       1400,
                                       { x: base + direction * 10, y: garbo.position.y },
                                       destie,
                                       destie
                                    );
                                 }
                              })
                           );
                           await X.pause(20);
                        })
                     );
                     await X.pause(700);
                  });
                  await TORIEL.resize({ x: 100, y: 40 }, 300);
                  await Firebol.sequence(9, async (promises, index) => {
                     promises.push(
                        Firebol.simple({
                           scale: 1.3,
                           position: { x: 160 + ((r() < 0.5 ? -1 : 1) * 90 + (r() * 2 * 10 - 10)), y: -30 },
                           damage: 10,
                           trail: true,
                           async handler (flame) {
                              await flame.position.modulate(800, { x: flame.position.x, y: r() * 30 + 180 });
                              // sounds.boom.start();
                              promises.push(
                                 Firebol.sequence(25, async (promises, index) => {
                                    promises.push(
                                       Firebol.simple({
                                          scale: 0.5 + r() * 0.2,
                                          position: flame.position.value(),
                                          damage: 8,
                                          async handler (extra) {
                                             const destination = extra.position.endpoint((index / 25) * 360, 300);
                                             await extra.position.modulate(3000 / (1 + r() * 3), destination);
                                          }
                                       })
                                    );
                                 })
                              );
                           }
                        })
                     );
                     await X.pause(1e3);
                  });
               } else {
                  await TORIEL.resize({ x: 100, y: 40 }, 300);
               }
               await Firebol.sequence(20, async (promises, index) => {
                  promises.push(
                     Firebol.simple({
                        scale: 1.4,
                        position: { x: 160 + ((r() < 0.5 ? -1 : 1) * 90 + (r() * 2 * 10 - 10)), y: -30 },
                        damage: 10,
                        trail: true,
                        async handler (flame) {
                           await flame.position.modulate(800, { x: flame.position.x, y: r() * 30 + 180 });
                           // sounds.boom.start();
                           promises.push(
                              Firebol.sequence(4, async (garbo, index) => {
                                 promises.push(
                                    Firebol.simple({
                                       scale: 1.1,
                                       position: flame.position.value(),
                                       damage: 8,
                                       trail: true,
                                       async handler (extra) {
                                          const destination = extra.position.endpoint(index * 90 + 45, 200);
                                          await extra.position.modulate(600, destination);
                                          promises.push(
                                             Firebol.sequence(20, async (promises, index) => {
                                                promises.push(
                                                   Firebol.simple({
                                                      scale: 0.5 + r() * 0.2,
                                                      position: extra.position.value(),
                                                      damage: 2,
                                                      async handler (garbo) {
                                                         const destination = garbo.position.endpoint(
                                                            (index / 20) * 360,
                                                            300
                                                         );
                                                         await garbo.position.modulate(
                                                            3000 / (1 + r() * 3),
                                                            destination
                                                         );
                                                      }
                                                   })
                                                );
                                             })
                                          );
                                       }
                                    })
                                 );
                              })
                           );
                        }
                     })
                  );
                  await X.pause(index === 19 ? 5e3 : 1e3);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            /*
            case 'battle1:3': {
               await TORIEL.resize({ x: 100, y: 80 }, 300);
               await Firebol.sequence(7, async () => {
                  const base = new XVector(160, 160).endpoint(r() * 360, 40);
                  const time = 1200;
                  await Firebol.sequence(20, async (promises, index) => {
                     promises.push(
                        Firebol.simple({
                           damage: 4,
                           scale: 0.8,
                           position: { x: 0, y: 1000 },
                           async handler (self) {
                              let d = 0;
                              let runs = 0;
                              let angle = index / 20 * 360;
                              self.on('tick', () => {
                                 self.position = base.endpoint(
                                    (angle += 4),
                                    X.math.bezier(d / (3 * (time / 100)), 86, 15, 15, 15, 86)
                                 );
                                 if (runs === 3) d++;
                              });
                              while (runs < 3) {
                                 await X.pause(90);
                                 self.alpha.value = 0;
                                 await X.pause(90);
                                 self.alpha.value = 1;
                                 runs++;
                              }
                              await X.pause(time);
                           }
                        }).then(() => {
                           return X.pause(200);
                        })
                     );
                  });
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'battle1:4': {
               await TORIEL.resize({ x: 16, y: 16 }, 100);
               await Firebol.simple({
                  damage: 5,
                  sound: true,
                  position: { x: 160, y: 160 },
                  async handler (bol) {
                     await X.pause(2000);
                  }
               });
               break;
            }
            case 'battle1:5': {
               await TORIEL.resize({ x: 200, y: 32 }, 100);
               await Firebol.sequence(5, async (promises, index) => {
                  const luBlaster = new AzzyBlaster({
                     position: { y: 220, x: 160 - 100 + index * 20 },
                     rotation: 180,
                     scale: { x: 0.5 }
                  });
                  const ruBlaster = new AzzyBlaster({
                     position: { y: 220, x: 160 + 100 - index * 20 },
                     rotation: 180,
                     scale: { x: 0.5 }
                  });
                  for (const blaster of [ luBlaster, ruBlaster ]) {
                     blaster.activate(
                        renderer,
                        'foreground',
                        { x: 0, y: 50 },
                        0,
                        250,
                        0,
                        index === 4 ? 6000 : 500,
                        200,
                        1000,
                        100,
                        true
                     );
                  }
                  const ldBlaster = new AzzyBlaster({
                     position: { y: 20, x: 160 - 100 + index * 20 },
                     rotation: 0,
                     scale: { x: 0.5 }
                  });
                  const rdBlaster = new AzzyBlaster({
                     position: { y: 20, x: 160 + 100 - index * 20 },
                     rotation: 0,
                     scale: { x: 0.5 }
                  });
                  for (const blaster of [ ldBlaster, rdBlaster ]) {
                     blaster.activate(
                        renderer,
                        'foreground',
                        { x: 0, y: -50 },
                        0,
                        250,
                        0,
                        index === 4 ? 6000 : 500,
                        200,
                        200,
                        100,
                        blaster === ldBlaster
                     );
                  }
                  await X.pause(250);
                  if (index === 4) {
                     let y = 0;
                     promises.push(
                        Firebol.sequence(20, async promises => {
                           const x = r() < 0.5 ? 150 : 170;
                           promises.push(
                              Firebol.simple({
                                 damage: 5,
                                 sound: true,
                                 position: { x, y: X.math.bezier(y++ / 20, 80, 130) },
                                 async handler (bol) {
                                    const destie = { x, y: bol.position.y + 500 };
                                    await bol.position.modulate(2e3, bol.position.value(), destie);
                                 }
                              })
                           );
                           await X.pause(250);
                        })
                     );
                  }
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'battle1:6': {
               await TORIEL.resize({ x: 50, y: 100 }, 100);
               await Firebol.sequence(10, async (promises, index) => {
                  // skip some random numbers for harder start pattern
                  r();
                  r();
                  r();
                  r();
                  let chosen = Math.floor(r() * 7);
                  const direction = index % 2 === 0 ? 1 : -1;
                  promises.push(
                     Firebol.sequence(7, async (promises, index) => {
                        promises.push(
                           Firebol.simple({
                              sound: true,
                              damage: 3,
                              position: { y: 90 + index * 15, x: 160 + direction * 70 },
                              type: index === chosen ? 'orange' : 'white',
                              async handler (garbo) {
                                 const base = garbo.position.x;
                                 const destie = { x: base - direction * 100, y: garbo.position.y };
                                 await garbo.position.modulate(
                                    1500,
                                    { x: base + direction * 10, y: garbo.position.y },
                                    destie,
                                    destie
                                 );
                              }
                           })
                        );
                        await X.pause(20);
                     })
                  );
                  await X.pause(1e3);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            case 'battle1:7': {
               Object.assign(game.player.position, game.player.position.endpoint(Math.floor(r() * 4) * 90 + 45, 50));
               await TORIEL.resize({ x: 100, y: 100 }, 100);
               await Firebol.sequence(9, async (promises, index1) => {
                  const center = new XVector(160, 160);
                  promises.push(
                     Firebol.sequence(4, async (promises, index2) => {
                        promises.push(
                           Firebol.sequence(4, async (promises, index3) => {
                              const angle = index2 * 90;
                              promises.push(
                                 Firebol.simple({
                                    scale: 0.8,
                                    sound: index2 === 0,
                                    damage: 3,
                                    position: center.endpoint(angle, 20 * index3),
                                    type: r() < 0.5 ? 'orange' : 'blue',
                                    trail: true,
                                    async handler (garbo) {
                                       await X.pause(300);
                                       const pushAngle = angle + 90;
                                       const destie = garbo.position.endpoint(pushAngle, 150);
                                       await garbo.position.modulate(
                                          1000 / (3 + r() * 2),
                                          garbo.position.value(),
                                          destie,
                                          destie
                                       );
                                    }
                                 })
                              );
                              await X.pause(100);
                           })
                        );
                     })
                  );
                  await X.pause(1200);
               });
               TORIEL.destroy();
               await TORIEL.resize({}, 300);
               break;
            }
            /*
            case 'battle1:1':
               {
                  await TORIEL.resize({ x: 100, y: 80 }, 300);
                  game.player.alpha.value = 1;
                  await Promise.all([
                     (async () => {
                        await X.pause(1000);
                        await attacks.inwardCross(3);
                        await X.pause(2000);
                        await attacks.inwardCross(3);
                     })(),
                     attacks.rotatingCircle(10, 2, 30, 15)
                  ]);
                  await TORIEL.resize({}, 300);
               }
               break;
            case 'battle1:2':
               {
                  await TORIEL.resize({ x: 200, y: 80 }, 300);
                  game.player.position.x = 85;
                  game.player.alpha.value = 1;
                  await Promise.all([ attacks.flameGauntlet() ]);
                  await TORIEL.resize({}, 300);
               }
               break;
            case 'battle1:3':
               {
                  await TORIEL.resize({ x: 150, y: 150 }, 300);
                  game.player.alpha.value = 1;
                  await Promise.all([ attacks.randomBlasters(10, 50, 50, 7, 1250) ]);
                  await TORIEL.resize({}, 300);
               }
               break;
            case 'battle1:4':
               {
                  await TORIEL.resize({ x: 200, y: 125 }, 300);
                  game.player.alpha.value = 1;
                  await attacks.circleCity(15, 750, 100, 3);
                  await X.pause(800);
                  await TORIEL.resize({}, 300);
               }
               break;
            case 'battle1:5':
               {
                  await TORIEL.resize({ x: 150, y: 150 }, 300);
                  game.player.alpha.value = 1;
                  await Promise.all([ attacks.randomBlasters(10, 50, 50, 7, 1000) ]);
                  await TORIEL.resize({}, 300);
               }
               break;
            case 'battle1:6':
               {
                  await TORIEL.resize({ x: 80, y: 70 }, 300);
                  const angle = Math.floor(Math.random() * 4) * 90 + 45;
                  game.player.alpha.value = 1;
                  attacks.bouncer(
                     6,
                     angle,
                     { x: game.player.position.x, y: game.player.position.y + 50 },
                     8e3,
                     () => {},
                     assets.bullets.orangeFireball
                  );
                  attacks.bouncer(
                     6,
                     angle + 45,
                     { x: game.player.position.x, y: game.player.position.y + 50 },
                     8e3,
                     () => {},
                     assets.bullets.orangeFireball
                  );
                  await TORIEL.resize({ x: 130, y: 80 }, 300);
                  await attacks.horizontalPath(
                     6.7433333333333333333333333333343433323334,
                     -5,
                     7,
                     6,
                     { size: 3, points: [ 80, 80 ] },
                     { size: 5, points: [ 80, 100 ] },
                     { size: 7, points: [ 100, 100, 100, 50, 50, 80 ] },
                     { size: 7, points: [ 65, 90, 90 ] },
                     { size: 5, points: [ 90, 70 ] },
                     { size: 7, points: [ 65, 90, 100 ] },
                     { size: 3, points: [ 100, 100, 70 ] },
                     { size: 4, points: [ 70, 70, 85 ] },
                     { size: 2, points: [ 85, 120 ] },
                     { size: 3, points: [ 120, 120 ] },
                     { size: 4, points: [ 120, 70, 50 ] },
                     { size: 8, points: [ 60 ] },
                     { size: 5, points: [ 60, 80, 80 ] }
                  );
                  await X.pause(800);
                  await TORIEL.resize({}, 300);
               }
               break;
            */
            default:
               TORIEL.destroy();
               await TORIEL.resize({ x: 100 }, 300);
               await X.pause(800);
               await TORIEL.resize({}, 300);
            // case 'battle1:7':
            // case 'battle1:8':
            // case 'battle1:9':
            // case 'battle1:10':
            // case 'battle1:11':
            // case 'battle1:12':
            // case 'battle2:1':
            // case 'battle2:2':
            // case 'battle2:3':
            // case 'battle2:4':
            // case 'battle2:5':
            // case 'battle2:6':
            // case 'battle2:7':
            // case 'battle2:8':
            // case 'battle2:9':
            // case 'battle2:10':
            // case 'battle2:11':
            // case 'battle2:12':
            // case 'battle2:13':
            // case 'battle2:14':
            // case 'battle2:15':
            // case 'battle2:16':
            // case 'battle2:17':
            // case 'mercy:1':
            // case 'mercy:2':
            // case 'mercy:3':
            // case 'mercy:4':
            // case 'mercy:5':
            // case 'mercy:6':
            // case 'mercy:7':
            // case 'mercy:8':
            // case 'mercy:9':
            // case 'mercy:10':
            // case 'mercy:11':
            // case 'mercy:12':
            // case 'mercy:13':
            // case 'mercy:14':
            // case 'mercy:15':
            // case 'mercy:16':
            // case 'mercy:17':
            // case 'mercy:18':
         }
         switch (stateKey) {
            case 'battle1:1':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Toriel readies her next attack.' ];
               break;
            case 'battle1:2':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Conflicted.' ];
               break;
            case 'battle1:3':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* He deserved to die, didn't\n  he?" ];
               break;
            case 'battle1:4':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Toriel is thinking about\n  dead fish.' ];
               break;
            case 'battle1:5':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Hee hee... that dirty rotten\n  trashbag.' ];
               break;
            case 'battle1:6':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* So that's what happened..." ];
               break;
            case 'battle1:7':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Still conflicted.' ];
               break;
            case 'battle1:8':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* As if that'll really make\n  a difference." ];
               break;
            case 'battle1:9':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Smells like... weakness.' ];
               break;
            case 'battle1:10':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Asriel would DEFINITELY not\n  approve of this.' ];
               break;
            case 'battle1:11':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Toriel readies something...\n  massive.' ];
               break;
            case 'mercy:6':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* ...' ];
               break;
            case 'mercy:7':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* What are you doing.' ];
               break;
            case 'mercy:8':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* This wasn't part of the\n  plan..." ];
               break;
            case 'mercy:9':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* ...' ];
               break;
            case 'mercy:10':
               TORIEL.status = [
                  '{#c:storyteller}{#i:40}* Unless... are you giving her a\n  false sense of hope? Hee hee.'
               ];
               break;
            case 'mercy:11':
               TORIEL.status = [
                  "{#c:storyteller}{#i:40}* I can't WAIT to see the look on\n  her face when you finally\n  backstab her..."
               ];
               break;
            case 'mercy:12':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* ...' ];
               break;
            case 'mercy:13':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Smells like doubt.' ];
               break;
            case 'mercy:14':
               TORIEL.status = [
                  "{#c:storyteller}{#i:40}* Hahaha, she doesn't even know\n  what to say any more! Brilliant!"
               ];
               break;
            case 'mercy:15':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Speechless.' ];
               break;
            case 'mercy:16':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Freudian slip.' ];
               break;
            case 'mercy:17':
               TORIEL.status = [
                  "{#c:storyteller}{#i:40}* She's losing the will to\n  fight.... that'll only make it\n  easier."
               ];
               break;
            case 'mercy:18':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* Now's your chance! Get her!" ];
               break;
            case 'mercy:19':
               TORIEL.status = [ '{#c:storyteller}{#i:40}* Come on... just kill her and\n  get it over with!' ];
               break;
            case 'mercy:20':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* ...something's wrong." ];
               break;
            case 'mercy:21':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* ...you're not the one who\n  killed the others, are you?" ];
               break;
            case 'mercy:22':
               TORIEL.status = [ "{#c:storyteller}{#i:40}* No! Don't you DARE...!" ];
               break;
         }
      } else {
         // handle end-of-phase scenarios
         switch (state.phase) {
            /*
            case 'asgore1':
               state.phase = 'asgore2';
               break;
            case 'asgore2':
               state.phase = 'asgore3';
               break;
            case 'asgore3':
               // asgore gaming joke mode ending
               return false;
            */
            case 'battle1':
               /*
               await TORIEL.speak(...extraDialogues.prePhase2);
               state.phase = 'battle2';
               */
               // end of storyteller edition
               return false;
            /*
            case 'battle2':
               state.phase = 'battle3';
               break;
            case 'battle3':
               // normal ending
               return false;
            case 'efe1':
               state.phase = 'efe2';
               break;
            case 'efe2':
               state.phase = 'efe3';
               break;
            case 'efe3':
               // efe kaya joke mode ending
               return false;
            case 'shc1':
               state.phase = 'shc2';
               break;
            case 'shc2':
               state.phase = 'shc3';
               break;
            case 'shc3':
               // shc joke mode ending
               return false;
            */
            case 'mercy':
               await TORIEL.speak(...extraDialogues.pacifistEnding);
               // secret mercy ending
               return false;
         }
      }
      if (dead) {
         await new Promise(resolve => {});
      }
      dialoguer.text('');
      atlas.switch('battle');
      return true;
   }
});

function justify (string: string, length: number) {
   let index = 0;
   const segments = string.split(' ');
   const output = [ segments.shift() ];
   for (const word of segments) {
      if (word) {
         const line = `${output[index]} ${word}`;
         if (line.length <= length) {
            output[index] = line;
         } else {
            output[++index] = word;
         }
      }
   }
   return output.join('\n');
}

/*
function justify (string: string, max: number) {
   let index = 0;
   let current = 0;
   const output = [ '' ];
   for (const word of string.split(' ')) {
      if (word.length > max) {
         current > 0 && ++index;
         output[index] = word;
         current = 0;
         ++index;
         output[index] = '';
      } else {
         current += word.length;
         if (current > max) {
            current = 0;
            output[++index] = '';
         }
         output[index] += `${output[index] === '' ? '' : ' '}${word}`;
      }
   }
   return output.join('\n');
}
*/

export const TORIEL = {
   async start () {
      let inv = false;
      const position = { x: 0, y: 0 };
      const listener = async () => {
         const moved = game.player.position.distance(position) > 0;
         moved && Object.assign(position, game.player.position.value());
         for (const {
            metadata: { damage, type, kr }
         } of game.player.detect(
            renderer,
            ...renderer.calculate(hitbox => typeof hitbox.metadata.damage === 'number')
         )) {
            if (!inv || kr) {
               // me when the variable name is   R   O   U   X   L   S   I   A   N
               let willHurte = false;
               switch (type) {
                  case 'blue':
                     moved && (willHurte = true);
                     break;
                  case 'orange':
                     moved || (willHurte = true);
                     break;
                  default:
                     willHurte = true;
                     break;
               }
               if (willHurte) {
                  kr || (inv = true);
                  manager.damage(damage as number);
                  sounds.hurt.start();
                  if (manager.data.hp <= 0 && dead === false) {
                     dead = true;
                     await X.pause(75);
                     try {
                        sounds.blaster.fire.source.gain.value = 0;
                     } catch (error) {}
                     try {
                        sounds.blaster.charge.source.gain.value = 0;
                     } catch (error) {}
                     try {
                        sounds.dialoguer.narrator.source.gain.value = 0;
                     } catch (error) {}
                     try {
                        sounds.dialoguer.tori.source.gain.value = 0;
                     } catch (error) {}
                     try {
                        sounds.noise.source.gain.value = 0;
                     } catch (error) {}
                     try {
                        sounds.hurt.source.gain.value = 0;
                     } catch (error) {}
                     sounds.blaster.fire.daemon.gain = 0;
                     sounds.blaster.charge.daemon.gain = 0;
                     sounds.dialoguer.narrator.daemon.gain = 0;
                     sounds.dialoguer.tori.daemon.gain = 0;
                     sounds.hurt.daemon.gain = 0;
                     for (const [ name, { context, objects } ] of Object.entries(renderer.layers)) {
                        renderer.detach(name, ...objects);
                        context.resetTransform();
                        context.clearRect(0, 0, context.canvas.width, context.canvas.height);
                     }
                     renderer.alpha.value = 0;
                     renderer.layers = {};
                     renderer.stop();
                     const renderer2 = new XRenderer({
                        alpha: 1,
                        auto: true,
                        camera: { x: 160, y: 120 },
                        container: document.body,
                        framerate: 30,
                        layers: {
                           main: [ 'static' ]
                        }
                     });
                     music.phase1.stop();
                     global.interact = true;
                     sounds.death.start();
                     await X.pause(1000);
                     sounds.shatter.start();
                     await X.pause(1500);
                     if (location.hash === '#amogus') {
                        location.reload();
                     } else {
                        renderer2.alpha.value = 1;
                        music.death.daemon.gain = 0.2;
                        await deathLoader;
                        music.death.start().loop = true;
                        music.death.source.gain.modulate(400, 0.2);
                        const deathText = dialoguer.text(
                           `{#c:gorey}{#i:90}{*}* ${manager.data.name}, don't give up!{^10}`,
                           '{#c:gorey}{#i:90}{*}* You have to stay\n  determined...!{^10}'
                        );
                        new XInput({ codes: [ 'z', 'Z', 'Enter' ] }).on('down', () => {
                           dialoguer.read();
                        });
                        atlas.attach(renderer2, 'main', 'deathDialoguer');
                        await deathText;
                        await music.death.source.gain.modulate(2000, 0);
                        location.search = '?respawn';
                     }
                     await X.pause(15000);
                  }
                  if (!kr) {
                     // sprite
                     swapSprites('battlerHurt');
                     await X.pause(200);
                     swapSprites('battler');
                     await X.pause(200);
                     swapSprites('battlerHurt');
                     await X.pause(200);
                     swapSprites('battler');
                     await X.pause(200);
                     swapSprites('battlerHurt');
                     await X.pause(200);
                     swapSprites('battler');
                     inv = false;
                  }
               }
            }
         }
      };
      atlas.menu = null;
      global.interact = true;
      await phase1Loader;
      const metTori = (manager.getFlag('metTori') || 0) as number;
      if (location.hash !== '#amogus') {
         player.walk({ x: 0, y: 0 }, renderer);
         // put dummy player to move camera
         game.player = new XWalker({
            position: Object.assign({}, player.position)
         });
         renderer.attach('foreground', game.player);
         await X.pause(1e3);
         player.face('up');
         {
            let counter = 0;
            while (counter++ < 100) {
               game.player.position.y -= 1;
               if (metTori < 2) {
                  await X.pause(10);
               } else {
                  await X.pause(5);
               }
            }
         }
         if (metTori < 2) {
            await X.pause(2000);
         } else {
            await X.pause(800);
         }
         // GOAT MOM! I LOVE U GOAT MOM I WANNA LIVE WITH U FOREVER GOAT MOM! AHHH
         const goatmom = new XWalker({
            alpha: 0,
            priority: 5,
            position: { x: player.position.x, y: 320 },
            sprites: {
               up: new XSprite({
                  steps: 20,
                  anchor: { x: 0 },
                  frames: [ content.goatmom0 ]
               }),
               down: new XSprite({
                  steps: 20,
                  anchor: { x: 0 },
                  frames: [ content.goatmom1 ]
               })
            }
         });
         renderer.attach('foreground', goatmom);
         sounds.appear.start();
         music.birds.source.gain.modulate(8 * 50, 0);
         {
            goatmom.face('up');
            let counter = 0;
            while (counter++ < 13) {
               goatmom.alpha.value = counter / 13;
               await X.pause(50);
            }
         }
         if (metTori < 1) {
            await X.pause(1000);
            const promise1 = dialoguer.text(
               '{*}{#c:tori}{#i:55}* Hello, {^4}my child...',
               '{*}* You were probably not\n  expecting to see me like this.',
               '{*}* ...',
               '{*}* But expectations no longer\n  matter, {^4}not anymore.',
               '{*}* As you can see, {^4}I am fully\n  aware of your actions thus far.',
               '{*}* It is up to me to put an end\n  to this...',
               '{*}* It is up to me to stop {^6}{@fill:#ff0000}you...'
            );
            atlas.switch('triviaDialoguer');
            await promise1;
         }
         await X.pause(500);
         goatmom.face('down');
         if (metTori < 1) {
            await X.pause(1500);
            const promise2 = dialoguer.text('{*}{@random:1,2}* No matter{^6} what.');
            atlas.switch('triviaDialoguer');
            await promise2;
         }
         manager.setFlag('metTori', metTori + 1);
         if (metTori < 2) {
            await X.pause(1000);
         } else {
            await X.pause(500);
         }
      }
      let index1 = 0;
      let index2 = 0;
      const overlay = new XRectangle({
         alpha: 0,
         anchor: { x: 0, y: 0 },
         fill: '#000000ff',
         stroke: '#00000000',
         size: { x: 320, y: 240 },
         position: game.player.position.value()
      });
      const SOUL = new XWalker({
         anchor: { x: 0, y: 0 },
         alpha: 0,
         size: { x: 8, y: 8 },
         priority: 10,
         step: 0.5,
         position: { x: player.position.x, y: player.position.y - 14.5 },
         sprites: whatthewhat.battler
      }).on('tick', () => {
         if (!global.interact) {
            const w = TORIEL.box.x / 2;
            const minX = 164 - w;
            const minY = 188.5 - Math.max(0, TORIEL.box.y - 8);
            const maxX = 156 + w;
            const maxY = 188.5;
            SOUL.position.x < minX && (SOUL.position.x = minX);
            SOUL.position.x > maxX && (SOUL.position.x = maxX);
            SOUL.position.y < minY && (SOUL.position.y = minY);
            SOUL.position.y > maxY && (SOUL.position.y = maxY);
         }
      });
      SOUL.face('down');
      if (location.hash === '#amogus') {
         renderer.attach('foreground', SOUL);
         SOUL.alpha.value = 1;
         player.alpha.value = 0;
         SOUL.position = new XVector(24, game.player.position.y + 116 - 9);
         await game.room('battle');
      } else {
         player.priority.value = 5;
         renderer.attach('foreground', overlay);
         renderer.refresh();
         if (metTori < 2) {
            while (index1++ < 4) {
               sounds.noise.start();
               overlay.alpha.value += 0.25;
               await X.pause(320);
            }
            await X.pause(640);
         } else {
            overlay.alpha.value = 1;
         }
         renderer.clear('overlay');
         renderer.attach('foreground', SOUL);
         while (index2++ < 2) {
            SOUL.alpha.value = 1;
            sounds.noise.start();
            await X.pause(70);
            SOUL.alpha.value = 0;
            await X.pause(70);
         }
         SOUL.alpha.value = 1;
         player.alpha.value = 0;
         sounds.noise.start();
         sounds.battleFall.start();
         await SOUL.position.modulate(540, new XVector(24, game.player.position.y + 116 - 9));
         await game.room('battle', 300);
         renderer.detach('foreground', overlay);
      }
      game.player = SOUL;
      music.phase1.start().loop = true;
      atlas.switch('battle');
      atlas.attach(renderer, 'foreground', 'battle');
      renderer.on('tick', listener);
      if (location.hash === '#amogus') {
         TORIEL.state.progress.battle1 = Math.round(Number(new URLSearchParams(location.search).get('attack')) || 0);
      }
      await battler.loop();
      renderer.off('tick', listener);
      await game.room('castleRoad', 300);
      atlas.switch(null);
      atlas.detach(renderer, 'foreground', 'battle');
      renderer.detach('foreground', SOUL);
      player.alpha.value = 1;
      game.player = player;
      global.interact = false;
      atlas.menu = 'sidebar';
   },
   body: {
      state: { arm0: -1, arm1: -1, brow: -1, eyes: -1, mouth: -1, xtra: -1 },
      base: new XSprite({
         anchor: { x: 0, y: 0 },
         position: { x: 160, y: 65 },
         frames: [ content.torielBody ],
         objects: [
            new XSprite({ anchor: { x: 0, y: 0 } }),
            new XSprite({ anchor: { x: 0, y: 0 } }),
            new XSprite({ anchor: { x: 0, y: 0 } }),
            new XSprite({ anchor: { x: 0, y: 0 } }),
            new XSprite({ anchor: { x: 0, y: 0 } }),
            new XSprite({ anchor: { x: 0, y: 0 } })
         ]
      }),
      get arm0 () {
         return TORIEL.body.state.arm0;
      },
      set arm0 (value) {
         TORIEL.body.state.arm0 = value;
      },
      get arm1 () {
         return TORIEL.body.state.arm1;
      },
      set arm1 (value) {
         TORIEL.body.state.arm1 = value;
      },
      get brow () {
         return TORIEL.body.state.brow;
      },
      set brow (value) {
         (TORIEL.body.base.objects[2] as XSprite).frames = [ TORIEL.body.components.brow[value] ];
         TORIEL.body.state.brow = value;
      },
      get eyes () {
         return TORIEL.body.state.eyes;
      },
      set eyes (value) {
         (TORIEL.body.base.objects[3] as XSprite).frames = [ TORIEL.body.components.eyes[value] ];
         TORIEL.body.state.eyes = value;
      },
      get mouth () {
         return TORIEL.body.state.mouth;
      },
      set mouth (value) {
         (TORIEL.body.base.objects[4] as XSprite).frames = [ TORIEL.body.components.mouth[value] ];
         TORIEL.body.state.mouth = value;
      },
      get xtra () {
         return TORIEL.body.state.xtra;
      },
      set xtra (value) {
         TORIEL.body.state.xtra = value;
      },
      components: {
         brow: [
            content.torielBrowFair,
            content.torielBrowFurrow,
            content.torielBrowNeutral,
            content.torielBrowOpen,
            content.torielBrowPerked,
            content.torielBrowPushed,
            content.torielBrowRaised,
            content.torielBrowWild,
            content.torielBrowSlant,
            content.torielBrowFlat,
            content.torielBrowWhatL,
            content.torielBrowWhatR
         ],
         eyes: [
            content.torielEyesClosed,
            content.torielEyesOpenA,
            content.torielEyesOpenB,
            content.torielEyesOpenC,
            content.torielEyesWtfL,
            content.torielEyesWtfR,
            content.torielEyesUp
         ],
         mouth: [
            content.torielMouthNeutral,
            content.torielMouthSmile,
            content.torielMouthSmileOpen,
            content.torielMouthSmileSus,
            content.torielMouthSmileWide,
            content.torielMouthNeutralOpen,
            content.torielMouthSmol
         ]
      }
   },
   spawn () {
      global.interact = false;
      game.player.alpha.value = 1;
   },
   destroy () {
      global.interact = true;
      game.player.alpha.value = 0;
   },
   async resize ({ x = 283, y = 65 } = {}, duration: number) {
      await TORIEL.box.modulate(duration, { x, y });
   },
   async advance () {
      const { state } = TORIEL;
      if (state.progress[state.phase] === dialogue[state.phase].length) {
         return false;
      } else {
         await TORIEL.speak(...dialogue[state.phase][state.progress[state.phase]++]);
         return true;
      }
   },
   async speak (...lines: string[]) {
      const prev = atlas.state.navigator;
      const promise = dialoguer.text(
         ...lines.map((text, index) => {
            text =
               (text[0] === '{' ? `${text.split('}').slice(0, -1).join('}')}}` : '') +
               justify(text[0] === '{' ? text.split('}').slice(-1)[0] : text, 19)
                  .split(', ')
                  .join(',{^3} ')
                  .split('. ')
                  .join('.{^4} ');
            index === 0 && (text = `{#c:tori}{#i:50}${text}`);
            return text;
         })
      );
      atlas.switch('toriDialoguer');
      await promise;
      atlas.switch(prev);
   },
   status: [ '{#c:storyteller}{#i:40}* Toriel now stands before you.' ],
   text: {
      FIGHT: '',
      FIGHT2: '* Toriel',
      ACT: '',
      ACT2: '* Toriel',
      check: '* Check',
      ITEM: '',
      ITEM2: '',
      MERCY: '',
      spare: '* Spare',
      flee: '* Flee'
   },
   box: new XVector(283, 65),
   state: {
      // whether or not an attack was made
      attack: false,
      // the index of the next attack
      progress: {
         mercy: 0,
         battle1: 0,
         battle2: 0,
         battle3: 0,
         get shc1 () {
            return TORIEL.state.progress.battle1;
         },
         set shc1 (value) {
            TORIEL.state.progress.battle1 = value;
         },
         get shc2 () {
            return TORIEL.state.progress.battle2;
         },
         set shc2 (value) {
            TORIEL.state.progress.battle2 = value;
         },
         get shc3 () {
            return TORIEL.state.progress.battle3;
         },
         set shc3 (value) {
            TORIEL.state.progress.battle3 = value;
         },
         get efe1 () {
            return TORIEL.state.progress.battle1;
         },
         set efe1 (value) {
            TORIEL.state.progress.battle1 = value;
         },
         get efe2 () {
            return TORIEL.state.progress.battle2;
         },
         set efe2 (value) {
            TORIEL.state.progress.battle2 = value;
         },
         get efe3 () {
            return TORIEL.state.progress.battle3;
         },
         set efe3 (value) {
            TORIEL.state.progress.battle3 = value;
         },
         get asgore1 () {
            return TORIEL.state.progress.battle1;
         },
         set asgore1 (value) {
            TORIEL.state.progress.battle1 = value;
         },
         get asgore2 () {
            return TORIEL.state.progress.battle2;
         },
         set asgore2 (value) {
            TORIEL.state.progress.battle2 = value;
         },
         get asgore3 () {
            return TORIEL.state.progress.battle3;
         },
         set asgore3 (value) {
            TORIEL.state.progress.battle3 = value;
         }
      },
      // current phase
      phase: 'battle1' as TorielPhases,
      // true if joke mode has been enabled
      dustrus: false,
      // true if pacifist route has not been aborted
      mercy: true
   }
};

loadout('isMad');
