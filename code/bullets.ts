import { bullets, content, sounds } from './assets';
import { game, renderer } from './core';
import { X, X2, XDefined, XHitbox, XObject, XObjectProperties, XRectangle, XRenderer, XSprite, XSpriteProperties } from './storyteller';

type AzzyBlasterProp = {
   [x in Exclude<
      keyof XDefined<XSpriteProperties>,
      'alpha' | 'steps' | 'step' | 'anchor' | 'frames' | 'objects'
   >]?: XDefined<XSpriteProperties>[x];
};

export class AzzyBlaster extends XSprite {
   constructor (properties: AzzyBlasterProp = {}) {
      super(
         Object.assign(properties, {
            alpha: 0,
            steps: 4,
            anchor: { x: 0, y: 0 },
            objects: [
               new XRectangle({
                  anchor: { x: 0 },
                  fill: 'white',
                  stroke: 'transparent',
                  size: { y: 2000, x: 0 }
               }),
               new XHitbox({
                  anchor: { x: 0 },
                  metadata: { damage: 1, kr: true },
                  size: { y: 2000, x: 0 }
               })
            ],
            frames: [
               content.azzyBlaster0,
               content.azzyBlaster1,
               content.azzyBlaster2,
               content.azzyBlaster3,
               content.azzyBlaster4
            ]
         })
      );
      this.on('tick', () => {
         this.state.index === this.frames.length - 1 && this.disable();
      });
   }
   async activate (
      renderer: XRenderer,
      layer: string,
      offsetPos: X2,
      offsetRot: number,
      spawnTime: number,
      chargeTime: number,
      fireTime: number,
      recoilTime: number,
      recoilDist: number,
      fadeTime: number,
      silent?: boolean
   ) {
      const beam = this.objects[0] as XRectangle;
      const hitbox = this.objects[1] as XHitbox;
      // play "swing in" animation
      const spawnPos = this.position.value();
      this.position = this.position.add(offsetPos);
      const spawnRot = this.rotation.value;
      this.rotation = this.rotation.add(offsetRot);
      // reset sprite
      this.reset();
      // add blaster to game
      renderer.attach(layer, this);
      silent || sounds.blaster.charge.start();
      // wait for swing in to complete
      await Promise.all([
         this.alpha.modulate(fadeTime, 0, 1, 1),
         this.position.modulate(spawnTime, this.position.value(), spawnPos, spawnPos),
         this.rotation.modulate(spawnTime, this.rotation.value, spawnRot, spawnRot)
      ]);
      // extra charge time
      await X.pause(chargeTime);
      // begin blaster firing animation
      this.enable();
      beam.alpha.value = 1;
      silent || sounds.blaster.fire.start();
      // add damage hitbox
      this.objects.push(hitbox);
      await Promise.all([
         // minimum pause time
         X.pause(fireTime),
         // increase blaster size
         beam.size.modulate(fadeTime, { x: 28, y: 2000 }),
         hitbox.size.modulate(fadeTime, { x: 28, y: 2000 }),
         renderer.shake.modulate(fadeTime, 1).then(async () => {
            await X.pause(fadeTime * 2);
            await renderer.shake.modulate(fadeTime, 0);
         }),
         // blaster recoil
         this.position.modulate(
            recoilTime,
            this.position.value(),
            this.position.endpoint(this.rotation.value + 90, -recoilDist),
            this.position.endpoint(this.rotation.value + 90, -recoilDist)
         )
      ]);
      await Promise.all([
         // decrease blaster size
         beam.size.modulate(fadeTime, { x: 0, y: 2000 }),
         hitbox.size.modulate(fadeTime, { x: 0, y: 2000 }),
         // fade out
         this.alpha.modulate(fadeTime, 1, 0, 0)
      ]);
      // remove damage hitbox
      this.objects.splice(1, 1);
      // remove blaster from game
      renderer.detach(layer, this);
      Object.assign(this.position, spawnPos);
   }
}

type FirebolType = 'orange' | 'blue' | 'white'; // | 'green';

type FirebolProp = {
   [x in Exclude<
      keyof XDefined<XSpriteProperties>,
      'step' | 'steps' | 'anchor' | 'frames' | 'objects'
   >]?: XDefined<XSpriteProperties>[x];
} & { damage?: number; type?: 'orange' | 'blue' | 'white' /* | 'green' */; trail?: boolean };

export class Firebol extends XSprite {
   trail: boolean;
   get damage () {
      return this.objects[0].metadata.damage as number;
   }
   get type () {
      return this.objects[0].metadata.type as FirebolType;
   }
   set damage (value) {
      this.objects[0].metadata.damage = value;
   }
   set type (value) {
      this.objects[0].metadata.type = value;
      this.frames = {
         white: bullets.normalFireball,
         blue: bullets.blueFireball,
         orange: bullets.orangeFireball
         /// green: bullets.greenFireball
      }[value];
   }
   constructor (properties: FirebolProp = {}) {
      super(
         Object.assign(properties, {
            anchor: { x: 0, y: 0 },
            step: 3,
            steps: 5,
            objects: [
               new XHitbox({
                  size: { x: 13, y: -13 },
                  anchor: { x: 0, y: 1 }
               })
            ]
         })
      );
      this.damage = properties.damage || 0;
      this.trail = properties.trail || false;
      this.type = properties.type || 'white';
      this.on('tick', () => {
         if (this.trail) {
            const newEcho = new XSprite({
               alpha: this.alpha.value,
               metadata: { stage: 0 },
               anchor: { x: 0, y: 0 },
               step: 3,
               steps: 5,
               parallax: this.parallax.value(),
               position: this.position.value(),
               scale: this.scale.value(),
               frames: [ this.frames[this.state.index] ]
            });
            renderer.attach('foreground', newEcho);
            Firebol.echoes.push(newEcho);
         }
      });
      Firebol.ready || Firebol.init();
   }
   static ready = false;
   static echoes = [] as XSprite[];
   static init () {
      Firebol.ready = true;
      renderer.on('tick', () => {
         for (const echo of Firebol.echoes) {
            const meta = echo.metadata as { stage: number };
            echo.alpha.value /= 2;
            if (meta.stage++ === 3) {
               Firebol.echoes.splice(Firebol.echoes.indexOf(echo), 1);
               renderer.detach('foreground', echo);
            }
         }
      });
   }
   static async sequence (count: number, generator: (promises: Promise<void>[], index: number) => Promise<void>) {
      let index = 0;
      const promises = [] as Promise<void>[];
      while (index < count) {
         await generator(promises, index++);
      }
      await Promise.all(promises);
   }
   static async simple ({
      damage = 0,
      sound = false,
      handler = async (bol: Firebol) => {},
      layer = 'foreground',
      trail = false,
      position = game.player.position.value() as X2,
      scale = 0.75,
      type = 'white' as FirebolType
   }) {
      const bol = new Firebol({ damage, position, trail, scale: { x: scale, y: scale }, type }).enable();
      sound && sounds.noise.start();
      renderer.attach(layer, bol);
      await handler(bol);
      renderer.detach(layer, bol);
   }
}

export class UntetheredRotator extends XObject {
   constructor (properties: XObjectProperties = {}) {
      super(properties);
      this.on('tick', () => {
         for (const object of this.objects) {
            object.rotation.value = this.rotation.value * -1;
         }
      });
   }
}

/*
export function fireball (
   size: number,
   extra: ConstructorParameters<typeof XEntity>[0] = {},
   objects: XDrawn<any>[],
   metadata: XKeyed = {}
) {
   const entity = new XEntity(
      Object.assign(
         {
            attributes: { trigger: true },
            bounds: { x: 9.5 * size, h: 16 * size, y: 3 * size, w: 13 * size },
            metadata: Object.assign({ bullet: true, damage: 4 }, metadata),
            renderer: 'foreground',
            sprite: new XSprite({
               interval: 5,
               scale: { x: size, y: size },
               objects: objects
            }).enable()
         },
         extra
      )
   );
   GAME.overworld.entities.add(entity);
   entity.on('tick', () => {
      if (entity.metadata.bounce === true) {
         const bounds = X.bounds(entity);
         let direction = entity.direction % 360;
         direction < 0 && (direction = 360 + direction);
         if (X.intersection(bounds, TORIEL.corders.left).size > 0) {
            if (direction > 180) {
               direction += 2 * (270 - direction);
               direction += 180;
            }
         } else if (X.intersection(bounds, TORIEL.corders.right).size > 0) {
            if (direction < 181) {
               direction += 2 * (90 - direction);
               direction += 180;
            }
         } else if (X.intersection(bounds, TORIEL.corders.top).size > 0) {
            if (direction > 90 && direction < 271) {
               direction += 2 * (180 - direction);
               direction += 180;
            }
         } else if (X.intersection(bounds, TORIEL.corders.bottom).size > 0) {
            if (direction > 270 || direction < 91) {
               direction += 2 * (0 - direction);
               direction += 180;
            }
         }
         entity.direction = direction;
      }
      if (entity.metadata.trail === true) {
         const echo = TORIEL.bullets.fireball(
            size,
            {
               alpha: entity.alpha / 2,
               depth: entity.depth - 1,
               attributes: { trigger: false },
               position: Object.assign({}, entity.position)
            },
            objects,
            { iteration: 0 }
         );
         const listener = () => {
            if (echo.metadata.iteration === 3) {
               GAME.overworld.entities.delete(echo);
               echo.off('tick', listener);
            } else {
               echo.alpha /= 2;
               echo.metadata.iteration++;
            }
         };
         echo.on('tick', listener);
      }
   });
   return entity;
}
*/
