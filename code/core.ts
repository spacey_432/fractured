import { content, music, whatthewhat } from './assets';
import { manager } from './data';
import { atlas } from './menu';
import { player } from './player';
import { rooms } from './rooms';
import { X } from './storyteller';
import { XGame } from './undertale';

export function swapSprites (key: keyof typeof whatthewhat) {
   player.sprites = whatthewhat[key];
   player.face('down');
}

export const global = { speed: 1, item: 0, dev: false, interact: false, room: void 0 as string | void };

export const friskAssets = X.inventory(
   content.friskDown0,
   content.friskDown1,
   content.friskDown2,
   content.friskDown3,
   content.friskLeft0,
   content.friskLeft1,
   content.friskRight0,
   content.friskRight1,
   content.friskUp0,
   content.friskUp1,
   content.friskUp2,
   content.friskUp3
);

export const friskSilhouetteAssets = X.inventory(
   content.friskSilhouetteDown0,
   content.friskSilhouetteDown1,
   content.friskSilhouetteDown2,
   content.friskSilhouetteDown3,
   content.friskSilhouetteLeft0,
   content.friskSilhouetteLeft1,
   content.friskSilhouetteRight0,
   content.friskSilhouetteRight1,
   content.friskSilhouetteUp0,
   content.friskSilhouetteUp1,
   content.friskSilhouetteUp2,
   content.friskSilhouetteUp3
);

export const friskStarkAssets = X.inventory(
   content.friskStarkDown0,
   content.friskStarkDown1,
   content.friskStarkDown2,
   content.friskStarkDown3,
   content.friskStarkLeft0,
   content.friskStarkLeft1,
   content.friskStarkRight0,
   content.friskStarkRight1,
   content.friskStarkUp0,
   content.friskStarkUp1,
   content.friskStarkUp2,
   content.friskStarkUp3
);

export const storyAssets = X.inventory(content.introMusic, content.storyVoice).load();

export const castleRoadAssets = X.inventory(
   friskAssets,
   content.castleRoadBackground,
   content.castleRoadBackdrop1,
   content.castleRoadBackdrop2,
   content.castleRoadBackdrop3,
   content.castleRoadBackdrop4,
   content.castleRoadBackdrop5,
   content.castleRoadBackdrop6,
   content.castleRoadBackdrop7
);

export const lastCorridorAssets = X.inventory(
   content.lastCorridorBackground,
   content.pillar,
   content.sans0,
   content.sans1,
   content.sans2,
   content.sans3,
   friskSilhouetteAssets,
   content.sansVoice,
   content.sfxBell
);

export const castleHookAssets = X.inventory(
   content.castleHookBackground,
   content.castleHookScribbleBackground,
   friskAssets,
   content.asgoreVoice,
   content.floweyVoice
);

export const coffinRoomAssets = X.inventory(
   content.coffinRoomBackground,
   content.coffinRoomMonsterBackground,
   friskAssets
);

export const throneRoomAssets = X.inventory(
   content.throneRoomBackground,
   content.throneRoomOverlay,
   friskStarkAssets,
   content.goatmom0,
   content.goatmom1,
   content.torielVoice,
   content.sfxNoise,
   content.sfxBattleFall,
   content.throneRoomDecoration,
   content.sfxAppear
);

export const battleAssets = X.inventory(
   // battle ui
   content.buttonFight0,
   content.buttonFight1,
   content.buttonAct0,
   content.buttonAct1,
   content.buttonItem0,
   content.buttonItem1,
   content.buttonMercy0,
   content.buttonMercy1,
   content.hp,
   content.soulInvulnerable,
   content.soulNormal,
   content.speechRight,
   content.deadeye,

   // sfx
   content.sfxSwing,
   content.sfxHit,
   content.sfxDeath,
   content.sfxShatter,
   content.sfxHurt,
   content.sfxCharge,
   content.sfxFire,
   content.sfxNoise,

   // fireballs
   content.blueFireball0,
   content.blueFireball1,
   content.blueFireball2,
   content.blueFireball3,
   content.normalFireball0,
   content.normalFireball1,
   content.normalFireball2,
   content.normalFireball3,
   content.orangeFireball0,
   content.orangeFireball1,
   content.orangeFireball2,
   content.orangeFireball3,

   // blaster
   content.azzyBlaster0,
   content.azzyBlaster1,
   content.azzyBlaster2,
   content.azzyBlaster3,
   content.azzyBlaster4,

   // sstriker
   content.sstriker1,
   content.sstriker2,
   content.sstriker3,
   content.sstriker4,
   content.sstriker5,
   content.sstriker6,

   // tori sprites
   content.torielBody,
   content.torielBrowFair,
   content.torielBrowFlat,
   content.torielBrowFurrow,
   content.torielBrowNeutral,
   content.torielBrowOpen,
   content.torielBrowPerked,
   content.torielBrowPushed,
   content.torielBrowRaised,
   content.torielBrowSlant,
   content.torielBrowWhatL,
   content.torielBrowWhatR,
   content.torielBrowWild,
   content.torielMouthNeutral,
   content.torielMouthNeutralOpen,
   content.torielMouthSmile,
   content.torielMouthSmileOpen,
   content.torielMouthSmileSus,
   content.torielMouthSmileWide,
   content.torielEyesClosed,
   content.torielEyesOpenA,
   content.torielEyesOpenB,
   content.torielEyesOpenC,
   content.torielEyesUp,
   content.torielEyesWtfL,
   content.torielEyesWtfR
);

export const owAssets = X.inventory(
   content.birdsAmbience,
   content.save0,
   content.save1,
   content.narratorVoice,
   content.menu,
   content.sfxSelect,
   content.sfxMenu,
   content.sfxBox,
   content.sfxHeal,
   content.sfxSave,
   content.sfxInfo
).load();

export const game = XGame.build({
   auto: true,
   layers: {
      background: [ 'ambient' ],
      foreground: [],
      overlay: [ 'ambient' ],
      menu: [ 'static' ]
   },
   player,
   rooms
});

export const renderer = game.renderer;

export const inputState = {
   value: true
};

export async function spawn () {
   inputState.value = false;
   await owAssets;
   music.birds.start(true).loop = true;
   renderer.attach('foreground', player);
   switch (manager.data.room) {
      case 'lastCorridor':
         await lastCorridorAssets.load();
         break;
      case 'castleHook':
         await castleHookAssets.load();
         break;
   }
   await game.room(manager.data.room);
   atlas.switch(null);
   inputState.value = true;
}
