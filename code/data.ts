import { global } from './core';
import { XSave } from './undertale';

function randomItem () {
   switch (Math.floor(Math.random() * 7)) {
      case 0:
         return 'Bisicle';
      case 1:
         return 'Cinnabun';
      case 2:
         return 'Crab Apple';
      case 3:
         return 'Glamburger';
      case 4:
         return 'Nice Cream';
      case 5:
         return 'Sea Tea';
      default:
         return 'Starfait';
   }
}

const levels = [
   10,
   30,
   70,
   120,
   200,
   300,
   500,
   800,
   1200,
   1700,
   2500,
   3500,
   5000,
   7000,
   10000,
   15000,
   25000,
   50000,
   99999,
   Infinity
];

export const manager = XSave.build({
   at () {
      return this.lv() * 2 + 8;
   },
   default () {
      return {
         armor: 'Heart Locket',
         boxes: [
            // dimensional box 1
            [
               'Golden Flower Tea',
               'Golden Flower Tea',
               'Golden Flower Tea',
               'Snowman Piece',
               'Stick',
               'Gross Bandage',
               randomItem(),
               randomItem(),
               randomItem(),
               randomItem()
            ],
            // dimensional box 2
            [ randomItem(), randomItem(), randomItem(), randomItem() ]
         ],
         name: '',
         flags: {
            // tori has been met
            tori_met_0: false,
            // tori has been met in a previous timeline
            $tori_met_1: 0,
            // sans has been met
            sans_met_0: false,
            // sans has been met in a previous timeline
            $sans_met_1: 0,
            // current battle phase
            goatmama_battle_phase: 0,
            // is the pacifist ending still possible
            goatmama_battle_mercy: true,
            // battle deaths count
            goatmama_battle_deaths: 0,
            // battle hit count (for no-hit dialogue, one-hit dialogue, etc.)
            goatmama_battle_hits: 0,
            // has completed normal ending before
            complete_standard: false,
            // has completed pacifist ending before
            complete_pacifist: false,
            // language option
            settings_language: 'ENGLISH',
            // debug option
            settings_debug: 'DISABLED'
         },
         fun: 1 + Math.floor(Math.random() * 100),
         g: 501 + Math.floor(Math.random() * 1000),
         hp: 40,
         // inventory
         items: [ randomItem(), randomItem(), randomItem() ],
         room: 'lastCorridor',
         xp: 900,
         weapon: 'Worn Dagger'
      };
   },
   df () {
      return Math.ceil(this.lv() / 4) + 9;
   },
   hp () {
      const lv = this.lv();
      return lv > 19 ? 99 : lv * 4 + 16;
   },
   items: {} as any,
   key: 'FracturedQueenDev3',
   lv () {
      let lv = 1;
      while (levels[lv - 1] < this.data.xp) {
         lv++;
      }
      return lv;
   }
});

export const SAVE = manager.data;

manager.items = XSave.build({
   key: '',
   default: (() => {}) as any,
   items: {
      'Heart Locket': {
         type: 'armor',
         turk: 'Kalp Kolyesi',
         info: [ '* It says "Best Friends Forever."' ],
         value: 15
      },
      'Golden Flower Tea': {
         type: 'consumable',
         turk: 'Altın Çiçek Çayı',
         value: 999,
         info: [ '* Golden Flower Tea, one cup.', '* Made with only the\nfluffiest ingredients.' ],
         use: [ '* You drank the Golden Flower tea.', '* ...', '* HP fully restored.' ],
         drop: [
            '* You threw away the tea.',
            '* ...',
            "* Somehow, it feels like you've\n  just lost something important..."
         ]
      },
      'Snowman Piece': {
         type: 'consumable',
         turk: 'Kardan Adam Parçası',
         value: 45,
         info: [ '* Please take this to the ends of the earth.' ],
         use: () => [
            '* You ate the Snowman Piece.',
            SAVE.hp < 52 - 45 ? '* You recovered 45 HP.' : '* HP fully restored.'
         ],
         drop: [
            '* You threw away the Snowman Piece.',
            '* ...',
            "* Somehow, it feels like you've\n  just saved a life..."
         ]
      },
      Stick: {
         type: 'other',
         turk: 'Çubuk',
         use: [ '* You threw the stick.', '* Nothing happened.' ],
         drop: [ '* You threw away the Stick.' ]
      },
      'Gross Bandage': {
         type: 'consumable',
         turk: 'Bandaj',
         value: 10,
         info: [ '* It has already been used several times.' ],
         use: () => [
            '* You re-applied the Gross Bandage.',
            SAVE.hp < 52 - 10 ? '* You recovered 10 HP.' : '* HP fully restored.'
         ],
         drop: [ '* You threw away the Bandage.' ]
      },
      'Worn Dagger': {
         type: 'weapon',
         turk: 'Hançer',
         info: [ '* Perfect for cutting plants and vines.' ],
         value: 15
      },
      Bisicle: {
         type: 'consumable',
         turk: 'Kurabiye',
         value: 11,
         info: [ "* It's a two-pronged popsicle, so\n  you can eat it twice." ],
         use: () => {
            SAVE.items.push('Unisicle');
            return [
               '* You eat one half of the Bisicle.',
               SAVE.hp < 52 - 11 ? '* You recovered 11 HP.' : '* HP fully restored.'
            ];
         },
         drop: [ '* You threw away the Bisicle.' ]
      },
      Unisicle: {
         type: 'consumable',
         turk: '',
         value: 11,
         info: [ "* It's a SINGLE-pronged popsicle.", "* Wait, that's just normal..." ],
         use: () => [ '* You ate the Unisicle.', SAVE.hp < 52 - 11 ? '* You recovered 11 HP.' : '* HP fully restored.' ],
         drop: [ '* You threw away the Unisicle.' ]
      },
      Cinnabun: {
         type: 'consumable',
         turk: 'Tarçanlı Tavşan',
         value: 22,
         info: [ '* A cinnamon roll in the shape of a bunny.' ],
         use: () => [
            '* You eat the Cinnamon Bunny.',
            SAVE.hp < 52 - 22 ? '* You recovered 22 HP.' : '* HP fully restored.'
         ],
         drop: [ '* You threw away the Cinnamon Bunny.' ]
      },
      'Crab Apple': {
         type: 'consumable',
         turk: 'Yengeç Elması',
         value: 18,
         info: [ '* An aquatic fruit that resembles a crustacean.' ],
         use: () => [
            '* You ate the Crab Apple.',
            SAVE.hp < 52 - 18 ? '* You recovered 18 HP.' : '* HP fully restored.'
         ],
         drop: [ '* You threw away the Crab Apple.' ]
      },
      Glamburger: {
         type: 'consumable',
         turk: 'Glamburger',
         value: 27,
         info: [ '* A hamburger made of edible glitter and sequins.' ],
         use: () => [
            '* You ate the Glamburger.',
            SAVE.hp < 52 - 27 ? '* You recovered 27 HP.' : '* HP fully restored.'
         ],
         drop: [ '* You threw away the Glamburger.', "* let's hope Mettaton doesn't mind." ]
      },
      'Nice Cream': {
         type: 'consumable',
         turk: 'Dondurma',
         value: 15,
         info: [ '* Instead of a joke, the wrapper\n  says something nice.' ],
         use: () => {
            const niceQuotes = [
               [ "{#s:narrator}{#i:50}* You're just great!" ],
               [ '{#s:narrator}{#i:50}* You look nice today!' ],
               [ '{#s:narrator}{#i:50}* Have a wonderful day!' ],
               [ '{#s:narrator}{#i:50}* Have a wonderful day!' ],
               [ '{#s:narrator}{#i:50}* Have a wonderful day!' ],
               [ '{#s:narrator}{#i:50}* Is this as sweet as you?' ],
               [ '{#s:narrator}{#i:50}* Love yourself! I love you!' ],
               [ "{#s:narrator}{#i:50}* Hugs are the best.\n* Here's one from me to you!" ],
               [ "{#s:narrator}{#i:50}* Did you know there's a\n  way to spare her?" ],
               [ "{#s:narrator}{#i:50}* Did you know there's a\n  way to spare her?" ]
            ];
            return [
               ...niceQuotes[Math.floor(Math.random() * niceQuotes.length)],
               SAVE.hp < 52 - 15 ? '* You recovered 15 HP.' : '* HP fully restored.'
            ];
         },
         drop: () => {
            SAVE.items.push('Nice Cream');
            return [ '* You tried to throw away the Nice\n Cream, but...', '* The power of wholesome quotes refused!' ];
         }
      },
      'Sea Tea': {
         type: 'consumable',
         turk: 'Deniz çayı',
         value: 10,
         info: [ '* Made from glowing marsh water.\n  Increases SPEED for one battle.' ],
         use: () => {
            global.speed += 0.15;
            return [
               '* You drank the Sea Tea.',
               SAVE.hp < 52 - 10 ? '* You recovered 10 HP.' : '* HP fully restored. SPEED up.'
            ];
         }
      },
      Starfait: {
         type: 'consumable',
         turk: 'Starfait',
         value: 14,
         info: [ '* A sweet treat made of sparkling stars.' ],
         use: () => [
            '* You consumed the Starfait.',
            SAVE.hp < 52 - 14 ? '* You recovered 14 HP.' : '* HP fully restored.'
         ]
      }
   }
}).items;
