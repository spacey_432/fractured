export const mercy = [
   [],
   [],
   [],
   [],
   [],
   [ '{#s:isNotAsMad}...?' ],
   [ '{#s:isMad}...' ],
   [ '{#s:isNotAsMad}...what are you doing?' ],
   [ '{#s:isMad}...' ],
   [ '{#s:huh}...why are you not fighting back?' ],
   [ '{#s:isNotAsMad}...' ],
   [ '{#s:isMad}......' ],
   [ '{#s:isPissed}.........' ],
   [ '{#s:isMadAtYou}Why are you sparing me!?', 'What have I done...', '...that Asgore and the others have not?' ],
   [ '{#s:isPissed}...' ],
   [
      '{#s:huhWTF}Did As-... I-I mean, Flowey...',
      'Did he... lie to me?',
      'Did you really kill the others like he said?'
   ],
   [ '{#s:hmmWTF}But if he IS lying, then...', '{#s:HardTruth}Perhaps it was wrong of me to fight you.' ],
   [
      '{#s:hmmWTFno}No... I cannot... I will not...',
      '{#s:isREALLYFUCKINGMADOHGOD}{@random:1,1}I will NOT be tricked by you AGAIN!'
   ],
   [ '{#s:isTired}...' ],
   [ '{#s:respectFace}......' ],
   [ '{#s:respectYou}.........' ],
   [
      'Even after all that...',
      "{#s:hopeRestored}You really... don't want to fight me, huh?",
      '{#s:HopeAccepted}...',
      '{#s:happygoat}Perhaps it was not really you who killed those other monsters.',
      '{#s:happygoatC}...',
      '{#s:happygoatD}My child...',
      '{#s:sadgoat}My child, please...',
      '{#s:sadgoatO}Use your power, your determination...',
      '{#s:sadgoat}And wipe this timeline from existence.',
      'Let us forget everything that has happened.',
      '{#s:respectFace}Can you... do that for me?'
   ]
];

export const battle1 = [
   [
      '{#s:isMad}This fight is highly unpleasant.',
      '{#s:isMadClosedEyes}Even when I encouraged myself through all of this...',
      '{#s:isMad}I cannot stop thinking about all the innocent lives...',
      '{#s:isMadClosedEyes}And what you have done.'
   ],
   [
      '{#s:HardTruth}I trusted you, you know...',
      '{#s:sadgoatO}We all did.',
      '{#s:isMadClosedEyes}But...',
      '{#s:isMad}I heard some really bad things happened thanks to you.',
      '{#s:isMadClosedEyes}A little flower told me everything.'
   ],
   [
      '{#s:HardTruthOpenEyes}They told me how you betrayed us monsters...',
      '{#s:HardTruth}When we were about to finally reach the surface.',
      '{#s:isMad}You turned the other humans against us...',
      '{#s:isMadClosedEyes}And then you killed Asgore.',
      '{#s:isNeutralClosedEyes}...',
      '{#s:isNeutral}I never really liked him.',
      '{#s:isMadClosedEyes}However, I would never wish death upon him.'
   ],
   [
      '{#s:isNeutral}And that monster with an eye patch...',
      '{#s:isNeutral}Undyne, I think her name was.',
      '{#s:isMad}She fought you when she noticed the situation...',
      '{#s:isMadClosedEyes}But, I would be a fool to think she survived.'
   ],
   [
      '{#s:isMad}Then there was a skeleton named Sans.',
      '{#s:isMad}The flower told me that he was the one telling me jokes.',
      '{#s:HardTruthOpenEyes}I never got to actually see him, but...',
      '{#s:isNeutral}I know he tried to stop you as well, from what I have heard.',
      '{#s:isMadClosedEyes}But even he was no match, was he?'
   ],
   [
      '{#s:isNeutral}The last one in the area was the scientist, Alphys.',
      '{#s:HardTruthOpenEyes}The flower did not know much of her whereabouts...',
      '{#s:isNeutral}Mainly because the flower decided to run to me for help.',
      '{#s:sadgoat}The flower then told me to kill every monster possible to stop you.',
      '{#s:sadgoat}It was an insane suggestion, but...',
      '{#s:sadgoatO}What other choice did I have after everything you did?'
   ],
   [
      '{#s:sadgoat}I then started to kill the monsters that were in the ruins with me.',
      '{#s:sadgoatO}I had to work up so much courage to do such a task...',
      '{#s:isNeutralClosedEyes}However, I was eventually able to do so.'
   ],
   [
      '{#s:isNeutral}After I killed everyone in of the ruins...',
      '{#s:sadgoat}I then left to kill... many others.',
      '{#s:sadgoatO}To say I was disgusted with myself is an understatement.',
      '{#s:isNeutralClosedEyes}But I did what I had to do.'
   ],
   [
      '{#s:isNeutral}Some monsters I had some difficulties with.',
      '{#s:isNeutralClosedEyes}And I was constantly thinking to myself that I was being too slow.',
      '{#s:isMad}Knowing that you could show up at any moment...',
      '{#s:isMadClosedEyes}And that I was not strong enough to put up a fight.'
   ],
   [
      '{#s:isNeutral}When me and the flower parted ways, they said they wanted to fight you.',
      '{#s:isNeutral}I tried to stop them, but...',
      '{#s:sadgoat}They insisted it was a good idea.',
      '{#s:sadgoatO}That it would stall you for long enough to give me a fighting chance.'
   ],
   [
      '{#s:isMadClosedEyes}Of course, that flower is likely dead because of your selfish actions.',
      '{#s:respectFace}But... their sacrifice gave me the time I needed to level up.',
      '{#s:isNeutral}Now here we are, fighting to the death because you betrayed us...',
      '{#s:isMadClosedEyes}...with little to no reason.'
   ],
   [
      '{#s:isMad}I cannot believe you would do this to us.',
      '{#s:isMadClosedEyes}You ripped a perfectly good life out of our hands.',
      '{#s:respectSad}Why? What was your reason?',
      '{#s:respectSadClosed}How can once a gentle soul become so heartless?',
      '{#s:isMadClosedEyes}...',
      '{#s:isREALLYFUCKINGMADOHGOD}{@random:1,1}HOW COULD YOU!?'
   ]
];
