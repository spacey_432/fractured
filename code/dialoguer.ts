import { sounds } from './assets';
import { loadout, mamaLoadouts } from './battler';
import { XDialoguer } from './undertale';

export const dialoguer = new XDialoguer();
export const dialoguerState = { character: 'narrator' };

dialoguer.on('header', header => {
   switch (header[0]) {
      case 'c':
         dialoguerState.character = header.split(':')[1];
         break;
      case 'i':
         dialoguer.interval.value = +header.split(':')[1];
         break;
      case 's':
         loadout(header.split(':')[1] as keyof typeof mamaLoadouts);
         break;
   }
});

dialoguer.on('text', content => {
   dialoguer.state.mode === 'read' &&
      content[0] &&
      (content[content.length - 1] === ' ' ||
         sounds.dialoguer[dialoguerState.character as keyof typeof sounds.dialoguer].start());
});
