type XResult<X = any> = X extends Promise<infer Y> ? Y : any;
