import './core';

import { content, music, sounds, sprites } from './assets';
import { TORIEL } from './battler';
import {
   battleAssets,
   castleHookAssets,
   castleRoadAssets,
   coffinRoomAssets,
   friskAssets,
   game,
   global,
   inputState,
   lastCorridorAssets,
   renderer,
   spawn,
   storyAssets,
   swapSprites,
   throneRoomAssets
} from './core';
import { SAVE } from './data';
import { dialoguer } from './dialoguer';
import { atlas } from './menu';
import { destinations, detector, spawns } from './rooms';
import { elementJobs, elements } from './story';
import { X, XHitbox, XInput, XText } from './storyteller';
import { XWalker } from './undertale';
import { player } from './player';

const fonts = [
   [ 'ComicSans', 'fonts/comic-sans.woff' ],
   [ 'CryptOfTomorrow', 'fonts/crypt-of-tomorrow.woff' ],
   [ 'DeterminationMono', 'fonts/determination-mono.woff' ],
   [ 'DeterminationSans', 'fonts/determination-sans.woff' ],
   [ 'DiaryOfAn8BitMage', 'fonts/diary-of-an-8-bit-mage.woff' ],
   [ 'DotumChe', 'fonts/dotum-che.woff' ],
   [ 'MarsNeedsCunnilingus', 'fonts/mars-needs-cunnilingus.woff' ],
   [ 'Papyrus', 'fonts/papyrus.woff' ],
   [ 'TimesNewRoman', 'fonts/times-new-roman.woff' ],
   [ 'Wingdings', 'fonts/wingdings.woff' ]
];

game.on('teleport', t => {
   // smart room assets
   switch (t) {
      case 'castleRoad':
         castleRoadAssets.load();
         lastCorridorAssets.load();
         break;
      case 'lastCorridor':
         castleRoadAssets.load();
         lastCorridorAssets.load();
         castleHookAssets.load();
         break;
      case 'castleHook':
         lastCorridorAssets.load();
         castleHookAssets.load();
         content.coffinStairsBackground.load();
         throneRoomAssets.load();
         break;
      case 'coffinStairs':
         castleHookAssets.load();
         content.coffinStairsBackground.load();
         coffinRoomAssets.load();
         break;
      case 'coffinRoom':
         content.coffinStairsBackground.load();
         coffinRoomAssets.load();
         break;
      case 'throneRoom':
         castleHookAssets.load();
         throneRoomAssets.load();
         battleAssets.load();
         break;
      case 'battle':
         throneRoomAssets.load();
         battleAssets.load();
         break;
   }
});

new XInput({ codes: [ 'v', 'V', '\\' ] }).on('down', () => {
   renderer.debug = !renderer.debug;
});

function triviaTrigger (door: XHitbox) {
   const text = door.metadata.text as ((string | (() => string))[] | null)[];
   const flag = door.metadata.flag as string;
   const index = (SAVE.flags[flag] as number) || 0;
   const lines = index < text.length ? text[index] : text[text.length - 1];
   if (lines) {
      global.interact = true;
      dialoguer.text(...lines.map(line => (typeof line === 'string' ? line : line()))).then(() => {
         global.interact = false;
      });
      atlas.switch('triviaDialoguer');
      SAVE.flags[flag] = index + 1;
   }
}

// load fonts
document.head.appendChild(
   Object.assign(document.createElement('style'), {
      textContent: fonts
         .map(([ name, source ]) => {
            const text = new XText({ content: 'X', position: { x: 1e4, y: 1e4 }, text: { font: `1px ${name}` } });
            renderer.attach('base', text);
            renderer.on('tick').then(() => void renderer.detach('base', text));
            return `@font-face{font-family:${name};src:url('${source}')}`;
         })
         .join('')
   })
);

const quitText = new XText({
   alpha: 0,
   priority: 1000,
   anchor: { y: 1 },
   position: { x: 4, y: 10 },
   fill: '#fff',
   stroke: '#0000',
   content: SAVE.flags.language === 'TURKISH' ? 'Çıkılıyor' : 'Quitting',
   text: {
      font: '10px BattleName'
   }
});

renderer.attach('menu', quitText);

new XInput({ codes: [ 'Escape' ] }).wrapOn('down', self => {
   return async () => {
      let quit = true;
      self.on('up').then(() => {
         quit = false;
         quitText.alpha.value = 0;
         quitText.alpha.modulate(150, 0).then(() => {
            quitText.content = 'Quitting';
         });
      });
      if (quit) {
         await quitText.alpha.modulate(300, 1);
      }
      if (quit) {
         quitText.content += '.';
         await X.pause(300);
      }
      if (quit) {
         quitText.content += '.';
         await X.pause(300);
      }
      if (quit) {
         window.close();
      }
   };
});

(async () => {
   if (location.hash !== '#amogus' && !location.search.includes('respawn')) {
      let skip = false;
      const intro = music.intro;
      const skipper = new XInput({ codes: [ 'x', 'X', 'Shift', 'z', 'Z', 'Enter' ] });

      await storyAssets;
      intro.start(true);
      intro.source.gain.modulate(1e3, 1);

      atlas.switch('storyDialoguer');

      function textBalancer (text: string) {
         return text.split(', ').join(',{^3} ').split('...').join('.{^3}.{^3}.').split('. ').join('.{^5} ');
      }

      // iterate over panels
      for (const element of elements) {
         if (!skip) {
            // get object
            const object = elementJobs.get(element)!;
            const textContainer = atlas.navigators.storyDialoguer.objects[0];

            // idk
            const skippie = skipper.on('down').then(() => {
               skip = true;
               const time = object.alpha.value * 1000;
               intro.source.gain.modulate(time, 0).then(() => {
                  intro.stop();
                  content.introMusic.unload();
               });
               sounds.dialoguer.storyteller.source.gain.modulate(time, 0);
            });

            // show panel text
            if (!skip) {
               // attach and unfade panel
               renderer.attach('menu', object);
               // object.alpha.modulate(1e3, 1);
               atlas.navigators.storyDialoguer.objects[0].alpha.modulate(1e3, 1);

               // handle text stop
               const handler = () => X.pause(1e3).then(() => dialoguer.read());
               dialoguer.on('idle', handler);

               // wait for text or skip button
               await Promise.race([ skippie, dialoguer.text(...element.lines.map(textBalancer)) ]);

               // do the unhandle, hide and fade panel
               dialoguer.off('idle', handler);
            }

            // get fade decay time
            const decay = object.alpha.value * 1e3;

            // trigger fade
            skip && textContainer.alpha.modulate(decay, 0);
            await object.alpha.modulate(decay, 0).then(() => renderer.detach('menu', object));
         }
      }

      await X.pause(300);
   }

   player.on('tick', () => {
      if (!global.interact) {
         const original = player.priority.value;
         if (global.room === 'throneRoom') {
            if (player.position.y > 520) {
               player.priority.value = 1;
            } else if (player.position.y > 310) {
               player.priority.value = -1;
            } else if (player.position.y > 250) {
               player.priority.value = 1;
            } else {
               player.priority.value = -1;
            }
         } else {
            player.priority.value = 0;
         }
         if (player.priority.value !== original) {
            renderer.refresh();
         }
      }
   });

   detector.on('tick', async door => {
      if (!global.interact && !door.metadata.interact && door.detect(renderer, player).length > 0) {
         switch (door.metadata.action) {
            case 'trivia':
               triviaTrigger(door);
               break;
            case 'door':
               {
                  global.interact = true;
                  const { destination: room, key } = door.metadata as { destination: string; key: string };
                  switch (global.room) {
                     case 'throneRoom':
                        music.birds.source.gain.modulate(300, 0);
                        break;
                  }
                  game.room(room, 300).then(() => {
                     const destie = destinations[room][key[0] === '$' ? key.slice(1) : key];
                     Object.assign(player.position, destie.position);
                     player.walk({ x: 0, y: 1 }, renderer);
                     player.walk({ x: 0, y: -1 }, renderer);
                     player.face(destie.direction);
                     global.interact = false;
                  });
               }
               break;
            case 'snas':
               if (!SAVE.flags.sans_met_0) {
                  SAVE.flags.sans_met_0 = true;
                  SAVE.flags.$sans_met_1 = ((SAVE.flags.$sans_met_1 as number) || 0) + 1;
                  atlas.menu = null;
                  global.interact = true;
                  // put dummy player to move camera
                  player.walk({ x: 0, y: 0 }, renderer);
                  game.player = new XWalker({
                     position: Object.assign({}, player.position)
                  });
                  renderer.attach('foreground', game.player);
                  await X.pause(1e3);
                  player.face('right');
                  {
                     let counter = 0;
                     while (counter++ < 60) {
                        game.player.walk({ x: 1, y: 0 }, renderer);
                        await X.pause(25);
                     }
                  }
                  await X.pause(2000);
                  const sansy = new XWalker({
                     alpha: 0,
                     position: { x: 920, y: player.position.y },
                     sprites: { left: sprites.snas }
                  });
                  // ADD SANS
                  renderer.attach('foreground', sansy);
                  {
                     let counter = 0;
                     while (counter++ < 100) {
                        sansy.alpha.value = (counter / 100) * 0.6;
                        sansy.walk({ x: -0.3, y: 0 }, renderer);
                        await X.pause(50);
                     }
                  }
                  sansy.walk({ x: 0, y: 0 }, renderer);
                  await X.pause(1000);
                  sounds.bell.start();
                  await X.pause(8500);
                  atlas.switch('sansDialoguer');
                  await dialoguer.text('');
                  await dialoguer.text(
                     SAVE.flags.settings_language === 'TURKISH'
                        ? '{#c:sans}{#i:60}* .{^10}.{^10}.{^20}mutlu musun, {^5}çocuk?'
                        : '{#c:sans}{#i:60}* .{^10}.{^10}.{^20}you proud of yourself, {^5}kid?'
                  );
                  atlas.switch(null);
                  {
                     let counter = 0;
                     while (counter++ < 100) {
                        sansy.alpha.value = (1 - counter / 100) * 0.6;
                        sansy.walk({ x: -0.3, y: 0 }, renderer);
                        await X.pause(50);
                     }
                  }
                  renderer.detach('foreground', sansy);
                  {
                     let counter = 0;
                     while (counter++ < 60) {
                        game.player.walk({ x: -1, y: 0 }, renderer);
                        await X.pause(25);
                     }
                  }
                  game.player = player;
                  global.interact = false;
                  atlas.menu = 'sidebar';
               }
               break;
            case 'goatmom':
               if (!SAVE.flags.tori_met_0) {
                  SAVE.flags.tori_met_0 = true;
                  SAVE.flags.$tori_met_1 = ((SAVE.flags.$tori_met_1 as number) || 0) + 1;
                  TORIEL.start();
               }
               break;
         }
      }
   });

   game.on('teleport', room => {
      global.room = room;
      room === void 0 || Object.assign(player.position, spawns[room]);
      switch (room) {
         case 'lastCorridor':
            swapSprites('friskSilhouette');
            break;
         case 'throneRoom':
            swapSprites('friskStark');
            music.birds.source.gain.modulate(300, 0.1);
            break;
         case 'battler':
            swapSprites('battler');
            break;
         default:
            swapSprites('frisk');
      }
   });

   new XInput({ codes: [ 'c', 'C', 'Control' ] }).on('down', () => {
      global.interact || (inputState.value && atlas.navigate('menu'));
   });

   new XInput({ codes: [ 'z', 'Z', 'Enter' ] }).on('down', () => {
      if (!global.interact) {
         const objects = new Set<XHitbox>();
         const listener = (door: XHitbox) => {
            if (objects.has(door)) {
               detector.off('tick', listener);
            } else if (door.metadata.interact && door.detect(renderer, player).length > 0) {
               objects.add(door);
               detector.off('tick', listener);
               switch (door.metadata.action) {
                  case 'save':
                     atlas.switch('save');
                     break;
                  case 'trivia':
                     triviaTrigger(door);
                     break;
               }
            }
         };
         detector.on('tick', listener);
         X.pause(1000 / 30).then(() => {
            detector.off('tick', listener);
         });
      }
      inputState.value && atlas.navigator() && atlas.navigate('next');
   });

   const downKey = new XInput({ codes: [ 's', 'S', 'ArrowDown' ] }).on('down', () => {
      inputState.value && atlas.navigator() && atlas.seek({ y: 1 });
   });

   const leftKey = new XInput({ codes: [ 'a', 'A', 'ArrowLeft' ] }).on('down', () => {
      inputState.value && atlas.navigator() && atlas.seek({ x: -1 });
   });

   const rightKey = new XInput({ codes: [ 'd', 'D', 'ArrowRight' ] }).on('down', () => {
      inputState.value && atlas.navigator() && atlas.seek({ x: 1 });
   });

   const specialKey = new XInput({ codes: [ 'x', 'X', 'Shift' ] }).on('down', () => {
      inputState.value && atlas.navigator() && atlas.navigate('prev');
   });

   const upKey = new XInput({ codes: [ 'w', 'W', 'ArrowUp' ] }).on('down', () => {
      inputState.value && atlas.navigator() && atlas.seek({ y: -1 });
   });

   location.hash === '#amogus' ||
      location.search.includes('respawn') ||
      atlas.switch(SAVE.name ? 'frontEnd' : 'frontEndInstructions');

   // remove any remaining story text
   dialoguer.text('');

   // instant respawn if just died
   if (location.search.includes('respawn')) {
      renderer.alpha.value = 0;
      X.pause().then(spawn);
   }

   // wait for initial teleport to overworld
   location.hash === '#amogus' || (await game.on('teleport'));

   if (location.search.includes('respawn')) {
      renderer.alpha.modulate(300, 1);
   }

   // enable player movement
   renderer.on('tick', () => {
      if (global.interact || !inputState.value) {
         game.player.walk({ x: 0, y: 0 }, renderer, () => false);
      } else {
         const y = player.position.y;
         game.player.walk(
            {
               x:
                  (leftKey.active() ? -3 : rightKey.active() ? 3 : 0) *
                  (specialKey.active() ? (player === game.player ? 2 : 0.5) : 1) *
                  (player === game.player ? 1 : global.speed),
               y:
                  (upKey.active() ? -3 : downKey.active() ? 3 : 0) *
                  (specialKey.active() ? (player === game.player ? 2 : 0.5) : 1) *
                  (player === game.player ? 1 : global.speed)
            },
            renderer,
            hitbox => hitbox.metadata.barrier === true
         );
         if (game.player === player && upKey.active() && downKey.active() && y === player.position.y) {
            player.position.y += 1;
            player.face('down');
         }
      }
   });

   // enable menu
   atlas.menu = 'sidebar';

   location.hash === '#amogus' && (await Promise.all([ battleAssets.load(), friskAssets.load() ])) && TORIEL.start();
})();
