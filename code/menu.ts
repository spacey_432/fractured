import { battler, TORIEL } from './battler';
import { manager, SAVE } from './data';
import { dialoguer } from './dialoguer';
import { game, global, renderer, spawn } from './core';
import { roomNames } from './rooms';
import { buttons, content, music, sounds } from './assets';
import { X, X2, XAtlas, XBasic, XHitbox, XKeyed, XNavigator, XObject, XPath, XProvider, XRectangle, XSprite, XText } from './storyteller';

let name = '';
let text = '';
let saved = false;
let fromOther = false;

dialoguer.on('text', content => {
   text = content;
});

dialoguer.on('empty', () => {
   atlas.state.navigator === 'triviaDialoguer' && atlas.switch(null);
});

const letters = 'abcdefghijklmnopqrstuvwxyz'.split('');
const easterEggs = {
   alphys: "Tori's gonna murder you, ehehe...",
   amogus: 'when the impostor has drip',
   asgore: 'G A M I N G',
   asriel: 'Are you keeping everyone safe up there, Frisk?',
   blooky: 'oh.......',
   bob: "Hi. I'm bob.",
   catty: '',
   chara: 'The true name.',
   crybik: () => {
      global.dev = true;
      return 'This name will activate developer mode. Are you';
   },
   demo: 'Demo x Treo confirmed?',
   doctor: 'Doctor Who?',
   efeyan: "I think you wanted to say 'SHC'",
   fdy: 'remake this fangame i dare you',
   flowey: 'Uh... howdy?',
   frisk: "You've lost the right to use that name, anomaly.",
   kasyan: 'DUSTTALE: Fractured Queen - Phase 1-3 COMPLETED![Undertale Fan Game]',
   spacey: 'Credits to Efe Kaya.',
   merg: "Thank you 'cherry' much for playing our game!",
   metta: 'OOH!! ARE YOU PROMOTING MY BRAND!??',
   mtt: 'OOH!! ARE YOU PROMOTING MY BRAND!??',
   mogus: 'mogus funny haha',
   moroz: '[No Hit] DUSTTALE: Fractured Queen by Efe Kaya - phase 1-3',
   napsta: 'oh.......',
   papyru: "I'LL ALLOW IT!",
   player: '\xa7random:2,2\xa7i cannot allow you to use that name',
   pog: 'Poggers?? Poggers Amoggers?',
   pogger: 'Poggers?? Poggers Amoggers?',
   qsv: 'No hits, impossible completions, persistence all the way to the end.',
   sanos: 'Perfectly balanced, as all things should be.',
   sans: 'I like the way you think, partner.',
   shc: "I think you wanted to say 'Efeyank'",
   silvur: 'No hits, impossible completions, persistence all the way to the end.',
   soot: 'UNDERTALE: DUSTTALE: Rejuvination of the Last Breath SWAPPED (Alphys Determination + Gaster Cameo) [Phase 69 Joke Mode Revenge] ~REMASTERED by FDY~',
   sus: 'Sus?? Sus Amogus?',
   tem: 'teM waNt... h0oMaNz to Die!!',
   temmie: 'teMmIe waNt... h0oMaNz to Die!!',
   thanos: 'Perfectly balanced, as all things should be.',
   toriel: 'My child...?',
   treo: 'Treo x Demo confirmed?',
   truedt: 'Wrong AU, buddy.',
   undyne: "She didn't stand a chance."
} as XKeyed<XProvider<string | void>>;

function boxKeys (array: string[]) {
   return Object.keys(array).map(key => `${array.length}:${key}`);
}

function selection (navigator: string, selection: XBasic) {
   return atlas.state.navigator === navigator && atlas.navigator()!.selection() === selection;
}

function fixedArray (array: string[], size: number) {
   return [ ...array.slice(0, size), ...new Array(Math.max(0, size - array.length)).join(' ').split(' ') ];
}

function position (navigator: string, { x, y }: X2) {
   if (atlas.state.navigator === navigator) {
      const nav = atlas.navigator()!;
      return nav.position.x === x && nav.position.y === y;
   } else {
      return false;
   }
}

function menuSoul ({ x = 0, y = 0 }: Partial<X2>, checker: () => boolean) {
   return new XSprite({
      position: { x: x + 1.5, y: y + 1.5 },
      frames: [ content.menu ]
   }).wrapOn('tick', sprite => {
      return () => (sprite.alpha.value = checker() ? 1 : 0);
   });
}

function currentDimBox () {
   switch (atlas.navigators.sidebarCell.selection()) {
      case '0':
         return SAVE.boxes[0];
      case '1':
         return SAVE.boxes[1];
      default:
         return [];
   }
}

function menuBox (
   { h = 0, w = 0, x = 0, y = 0 }: Partial<XKeyed<number, 'h' | 'w' | 'x' | 'y'>>,
   ...objects: XObject[]
) {
   return new XRectangle({
      stroke: '#fff',
      fill: '#000',
      line: { width: 3 },
      position: { x: x + 1.5, y: y + 1.5 },
      size: { x: w + 3, y: h + 3 },
      objects
   });
}

function menuText (
   { x = 0, y = 0 }: Partial<X2>,
   { anchor = { y: -1 } as Partial<X2>, color = '#fff', font = '16px Menu', size = 9, spacing = {} as Partial<X2> },
   provider: XProvider<string>
) {
   const text = new XText({
      anchor,
      stroke: '#0000',
      fill: color,
      text: { font },
      position: { x: x + 1.5, y: y - size / 2 + 2 },
      spacing: { x: spacing.x, y: (spacing.y || 0) / 2 }
   });
   if (typeof provider === 'function') {
      text.on('tick', () => (text.content = provider()));
   } else {
      text.content = provider;
   }
   return text;
}

function cycleOption (object: any, key: string, ...values: string[]) {
   if (object[key] === values[values.length - 1]) {
      object[key] = values[0];
   } else {
      object[key] = values[values.indexOf(object[key]) + 1] || values[0];
   }
}

function turk (turkish: string, english: string) {
   return () => (SAVE.flags.settings_language === 'TÜRKÇE' ? turkish : english);
}

export const atlas: XAtlas = new XAtlas({
   navigators: {
      //////////////////////////////////////////////////////////////////////////////////////////////
      //                                                                                          //
      //    OVERWORLD                                                                             //
      //                                                                                          //
      //////////////////////////////////////////////////////////////////////////////////////////////
      sidebar: new XNavigator({
         grid: [ [ 'sidebarItem', 'sidebarStat', 'sidebarCell' ] ],
         next: self => {
            if (self.selection() === 'sidebarItem' && SAVE.items.length === 0) {
               return void 0;
            } else {
               return self.selection() as string;
            }
         },
         prev: null,
         objects: [
            menuBox(
               { h: 49, w: 65, x: 16, y: 26 },
               menuText({ x: 4, y: 5 }, {}, () => SAVE.name),
               menuText({ x: 4, y: 21 }, { font: '8px Stat', size: 5 }, turk('SV', 'LV')),
               menuText({ x: 22, y: 21 }, { font: '8px Stat', size: 5 }, () => manager.lv().toString()),
               menuText({ x: 4, y: 30 }, { font: '8px Stat', size: 5 }, turk('CAN', 'HP')),
               menuText({ x: 22, y: 30 }, { font: '8px Stat', size: 5 }, () => `${SAVE.hp}/${manager.hp()}`),
               menuText({ x: 4, y: 39 }, { font: '8px Stat', size: 5 }, turk('G', 'A')),
               menuText({ x: 22, y: 39 }, { font: '8px Stat', size: 5 }, () => SAVE.g.toString())
            ),
            menuBox(
               { h: 68, w: 65, x: 16, y: 84 },
               menuSoul({ x: 9, y: 11 }, () => selection('sidebar', 'sidebarItem')),
               menuText(
                  { x: 23, y: 11 },
                  {},
                  () => (SAVE.items.length === 0 ? '\xa7fill:#808080\xa7' : '') + turk('EŞYA', 'ITEM')()
               ),
               menuSoul({ x: 9, y: 29 }, () => selection('sidebar', 'sidebarStat')),
               menuText({ x: 23, y: 29 }, {}, 'STAT'),
               menuSoul({ x: 9, y: 47 }, () => selection('sidebar', 'sidebarCell')),
               menuText({ x: 23, y: 47 }, {}, turk('TEL', 'CELL'))
            )
         ]
      })
         .on('from', (atlas, key) => {
            sounds.menu.start();
            if (!key) {
               global.interact = true;
               atlas.attach(renderer, 'menu', 'sidebar');
            }
         })
         .on('to', (atlas, key) => {
            if (key) {
               sounds.select.start();
            } else {
               atlas.detach(renderer, 'menu', 'sidebar');
               global.interact = false;
            }
         })
         .on('move', () => {
            sounds.menu.start();
         }),
      sidebarItem: new XNavigator({
         grid: () => [ Object.keys(manager.items) ],
         next: 'sidebarItemOptions',
         prev: 'sidebar',
         objects: [
            menuBox(
               { h: 175, w: 167, x: 94, y: 26 },
               ...new Array(16).fill(0).map((x, index) => {
                  const row = Math.floor(index / 2);
                  if (index % 2 === 0) {
                     return menuSoul({ x: 7, y: 15 + row * 16 }, () => position('sidebarItem', { x: 0, y: row }));
                  } else {
                     return menuText({ x: 19, y: 15 + row * 16 }, {}, () => SAVE.items[row] || '');
                  }
               }),
               menuSoul({ x: 7, y: 155 }, () => selection('sidebarItemOptions', 'use')),
               menuText({ x: 19, y: 155 }, {}, () =>
                  manager.items[SAVE.items[atlas.navigators.sidebarItem.position.y]].type === 'consumable'
                     ? turk('KULLAN', 'USE')()
                     : turk('KUŞAN', 'EQUIP')()
               ),
               menuSoul({ x: 55, y: 155 }, () => selection('sidebarItemOptions', 'info')),
               menuText({ x: 67, y: 155 }, {}, turk('BİLGİ', 'INFO')),
               menuSoul({ x: 112, y: 155 }, () => selection('sidebarItemOptions', 'drop')),
               menuText({ x: 124, y: 155 }, {}, turk('BIRAK', 'DROP'))
            )
         ]
      })
         .wrapOn('from', self => {
            return (atlas, key) => {
               if (key === 'sidebar') {
                  self.position = { x: 0, y: 0 };
                  atlas.attach(renderer, 'menu', 'sidebarItem');
               }
            };
         })
         .on('to', (atlas, key) => {
            if (key === 'sidebar') {
               atlas.detach(renderer, 'menu', 'sidebarItem');
            } else {
               sounds.select.start();
            }
         })
         .on('move', () => {
            sounds.menu.start();
         }),
      sidebarItemOptions: new XNavigator({
         grid: [ [ 'use', 'info', 'drop' ] ],
         flip: true,
         next: (self, atlas) => {
            const y = atlas.navigators.sidebarItem.position.y;
            const action = self.selection() as 'use' | 'info' | 'drop';
            X.pause().then(() => {
               const item = manager.items[SAVE.items[y]];
               const text = manager.activate(y, action);
               SAVE.items = SAVE.items.filter(item => item && item in manager.items);
               if (action === 'drop' || (action === 'use' && (item.type === 'armor' || item.type === 'weapon'))) {
                  sounds.info.start();
               } else {
                  sounds.select.start();
               }
               dialoguer
                  .text(...(typeof text === 'function' ? text() : text).map(line => `{#c:narrator}{#i:50}${line}`))
                  .then(() => {
                     atlas.switch(null);
                     global.interact = false;
                  });
               atlas.switch('triviaDialoguer');
            });
            return null;
         },
         prev: 'sidebarItem'
      })
         .wrapOn('from', self => {
            return (x, key) => {
               key === 'sidebarItem' && (self.position = { x: 0, y: 0 });
            };
         })
         .on('to', (atlas, key) => {
            if (key === null) {
               atlas.detach(renderer, 'menu', 'sidebar', 'sidebarItem');
               global.interact = false;
            }
         })
         .on('move', () => {
            sounds.menu.start();
         }),
      sidebarStat: new XNavigator({
         prev: 'sidebar',
         objects: [
            menuBox(
               { h: 203, w: 167, x: 94, y: 26 },
               menuText({ x: 11, y: 17 }, {}, () => `"${SAVE.name}"`),
               menuText({ x: 11, y: 47 }, {}, () => `LV \xa0\xa0\xa0${manager.lv()}`),
               menuText({ x: 11, y: 63 }, {}, () => `HP \xa0\xa0\xa0${SAVE.hp} / ${manager.hp()}`),
               menuText({ x: 11, y: 95 }, {}, () => {
                  return `${turk('SLD', 'AT')()} \xa0\xa0\xa0${manager.at()} (${manager.atx()})`;
               }),
               menuText({ x: 11, y: 111 }, {}, () => {
                  return `${turk('SVN', 'DF')()} \xa0\xa0\xa0${manager.df()} (${manager.dfx()})`;
               }),
               menuText({ x: 95, y: 95 }, {}, () => `EXP: ${SAVE.xp}`),
               menuText({ x: 95, y: 111 }, {}, () => `${turk('SIRADAKI', 'NEXT')()}: ${SAVE.xp}`),
               menuText({ x: 11, y: 132 }, {}, () => `${turk('SİLAH', 'WEAPON')()}: ${SAVE.weapon}`),
               menuText({ x: 11, y: 148 }, {}, () => `${turk('ZIRH', 'ARMOR')()}: ${SAVE.armor}`),
               menuText({ x: 11, y: 168 }, {}, () => `${turk('ALTIN', 'GOLD')()}: ${SAVE.g}`)
            )
         ]
      })
         .on('from', atlas => {
            atlas.attach(renderer, 'menu', 'sidebarStat');
         })
         .on('to', atlas => {
            atlas.detach(renderer, 'menu', 'sidebarStat');
         }),
      sidebarCell: new XNavigator({
         grid: () => [ [ 'sidebarCellToriel', 'sidebarCellPapyrus', '0', '1' ] ],
         next: self => {
            const selection = self.selection() as string;
            return isNaN(+selection) ? selection : 'sidebarCellBox';
         },
         prev: 'sidebar',
         objects: [
            menuBox(
               { h: 129, w: 167, x: 94, y: 26 },
               menuSoul({ x: 7, y: 15 }, () => selection('sidebarCell', 'sidebarCellToriel')),
               menuText({ x: 19, y: 15 }, {}, turk("Toriel'in Telefonu", "Toriel's Phone")),
               menuSoul({ x: 7, y: 31 }, () => selection('sidebarCell', 'sidebarCellPapyrus')),
               menuText({ x: 19, y: 31 }, {}, turk('Papyrus ve Undyne', 'Papyrus and Undyne')),
               menuSoul({ x: 7, y: 47 }, () => selection('sidebarCell', '0')),
               menuText({ x: 19, y: 47 }, {}, turk('Boyutsal Kutu A', 'Dimensional Box A')),
               menuSoul({ x: 7, y: 63 }, () => selection('sidebarCell', '1')),
               menuText({ x: 19, y: 63 }, {}, turk('Boyutsal Kutu B', 'Dimensional Box B'))
            )
         ]
      })
         .wrapOn('from', self => {
            return (atlas, key) => {
               key === 'sidebar' && ((self.position = { x: 0, y: 0 }), atlas.attach(renderer, 'menu', 'sidebarCell'));
            };
         })
         .on('to', (atlas, key) => {
            atlas.detach(renderer, 'menu', 'sidebarCell');
            if (key === 'sidebarCellBox') {
               atlas.detach(renderer, 'menu', 'sidebar');
               sounds.box.start();
            } else {
               key === 'sidebar' || sounds.select.start();
            }
         })
         .on('move', () => {
            sounds.menu.start();
         }),
      sidebarCellBox: new XNavigator({
         grid: () => [ boxKeys(fixedArray(SAVE.items, 8)), boxKeys(fixedArray(currentDimBox(), 10)) ],
         next: self => {
            const left = self.position.x === 0;
            const source = left ? SAVE.items : currentDimBox();
            const target = left ? currentDimBox() : SAVE.items;
            const index = self.position.y;
            index < source.length && target.length < (left ? 10 : 8) && target.push(source.splice(index, 1)[0]);
         },
         prev: null,
         objects: [
            menuBox(
               { h: 219, w: 299, x: 8, y: 8 },
               menuText({ x: 41, y: 8 }, {}, turk('ENVANTER', 'INVENTORY')),
               menuText({ x: 213, y: 8 }, {}, turk('KUTU', 'BOX')),
               menuText({ x: 89, y: 196 }, {}, turk('Bitirmek İçin [X] Basın.', 'Press [X] to Finish')),
               new XObject({
                  stroke: '#fff',
                  line: { width: 1 },
                  position: { x: 149, y: 29 },
                  objects: [
                     new XPath({
                        tracer (context, x, y) {
                           context.moveTo(x, y);
                           context.lineTo(x, y + 150);
                           context.stroke();
                        }
                     }),
                     new XPath({
                        position: { x: 2 },
                        tracer (context, x, y) {
                           context.moveTo(x, y);
                           context.lineTo(x, y + 150);
                           context.stroke();
                        }
                     })
                  ]
               }),
               ...new Array(24).fill(0).map((x, index) => {
                  const row = Math.floor(index / 3);
                  if (index % 3 === 0) {
                     return menuSoul({ x: 9, y: 29 + row * 16 }, () => position('sidebarCellBox', { x: 0, y: row }));
                  } else if (index % 3 === 1) {
                     return menuText({ x: 21, y: 29 + row * 16 }, {}, () => SAVE.items[row] || '');
                  } else {
                     return new XPath({
                        stroke: '#f00',
                        line: { width: 0.5 },
                        position: { x: 26, y: 35 + row * 16 },
                        tracer (context, x, y) {
                           if (!(row in manager.items)) {
                              context.moveTo(x, y);
                              context.lineTo(x + 90, y);
                              context.stroke();
                           }
                        }
                     });
                  }
               }),
               ...new Array(30).fill(0).map((x, index) => {
                  const row = Math.floor(index / 3);
                  if (index % 3 === 0) {
                     return menuSoul({ x: 158, y: 29 + row * 16 }, () => position('sidebarCellBox', { x: 1, y: row }));
                  } else if (index % 3 === 1) {
                     return menuText({ x: 170, y: 29 + row * 16 }, {}, () => currentDimBox()[row] || '');
                  } else {
                     return new XPath({
                        stroke: '#f00',
                        line: { width: 0.5 },
                        position: { x: 175, y: 35 + row * 16 },
                        tracer (context, x, y) {
                           if (!(row in currentDimBox())) {
                              context.moveTo(x, y);
                              context.lineTo(x + 90, y);
                              context.stroke();
                           }
                        }
                     });
                  }
               })
            )
         ]
      })
         .wrapOn('from', self => {
            return atlas => {
               self.position = { x: 0, y: 0 };
               atlas.attach(renderer, 'menu', 'sidebarCellBox');
            };
         })
         .on('to', atlas => {
            atlas.detach(renderer, 'menu', 'sidebarCellBox');
            global.interact = false;
         }),
      save: new XNavigator({
         flip: true,
         grid: () => (saved ? [] : [ [ 'save', 'return' ] ]),
         next: self => {
            if (self.selection() === 'save') {
               saved = true;
               sounds.save.start();
               global.room === void 0 || (SAVE.room = global.room);
               manager.save();
            } else {
               self.position = { x: 0, y: 0 };
               return null;
            }
         },
         prev: null,
         objects: [
            menuBox(
               { h: 162 / 2, w: 412 / 2, x: 108 / 2, y: 118 / 2 },
               new XObject({
                  objects: [
                     menuText({ x: 13, y: 12 }, {}, () => (saved ? '\xa7fill:#ffff00\xa7' : '') + SAVE.name),
                     menuText(
                        { x: 79, y: 12 },
                        {},
                        () =>
                           `${(saved ? '\xa7fill:#ffff00\xa7' : '') + turk('SV', 'LV')()} \xa0\xa0\xa0${manager.lv()}`
                     ),
                     menuText({ x: 169, y: 12 }, {}, () => `${saved ? '\xa7fill:#ffff00\xa7' : ''}?:??`),
                     menuText(
                        { x: 13, y: 32 },
                        {},
                        () =>
                           (saved ? '\xa7fill:#ffff00\xa7' : '') +
                           (roomNames[SAVE.room]() || turk('Bilinmeyen Yer', 'Unknown Location')())
                     ),
                     menuSoul({ x: 14, y: 62 }, () => selection('save', 'save')),
                     menuText(
                        { x: 28, y: 62 },
                        {},
                        () =>
                           (saved ? '\xa7fill:#ffff00\xa7' : '') +
                           (saved ? turk('Dosya Kaydedildi.', 'File saved.')() : turk('Kaydet', 'Save')())
                     ),
                     menuSoul({ x: 104, y: 62 }, () => selection('save', 'return')),
                     menuText({ x: 118, y: 124 / 2 }, {}, () => (saved ? '' : turk('Geri dön', 'Return')()))
                  ]
               })
            )
         ]
      })
         .on('from', () => {
            saved = false;
            global.interact = true;
            sounds.heal.start();
            SAVE.hp = manager.hp();
            atlas.attach(renderer, 'menu', 'save');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'save');
            global.interact = false;
         })
         .on('move', () => {
            sounds.menu.start();
         }),
      //////////////////////////////////////////////////////////////////////////////////////////////
      //                                                                                          //
      //    DIALOGUERS                                                                            //
      //                                                                                          //
      //////////////////////////////////////////////////////////////////////////////////////////////
      storyDialoguer: new XNavigator({
         objects: [
            menuText(
               { x: 25, y: 120 },
               { anchor: { y: 0 }, font: '16px Dialogue', spacing: { x: 1, y: 7.5 } },
               () => text
            )
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'storyDialoguer');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'storyDialoguer');
         }),
      deathDialoguer: new XNavigator({
         next: () => dialoguer.read(),
         prev: () => dialoguer.skip(),
         objects: [
            menuText(
               { x: 25, y: 120 },
               { anchor: { y: 0 }, font: '16px Dialogue', spacing: { x: 1, y: 7.5 } },
               () => text
            )
         ]
      }),
      // topDialoguer box
      sansDialoguer: new XNavigator({
         next: () => void dialoguer.read(),
         prev: () => void dialoguer.skip(),
         objects: [
            menuBox(
               { x: 16, y: 5, w: 283, h: 70 },
               menuText({ x: 11, y: 11 }, { font: '16px Sans', spacing: { y: 7.5 } }, () => text)
            )
         ]
      })
         .on('from', () => {
            text = '';
            atlas.attach(renderer, 'menu', 'sansDialoguer');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'sansDialoguer');
         }),
      // toriDialoguer "box" (used for tori's dialogue!)
      toriDialoguer: new XNavigator({
         next: () => dialoguer.read(),
         prev: () => dialoguer.skip(),
         objects: [
            new XObject({
               position: { x: 190, y: 20 },
               objects: [
                  new XSprite({
                     scale: { x: 0.1, y: 0.1 },
                     frames: [ content.speechRight ]
                  }),
                  menuText(
                     { x: 15, y: 5 },
                     { color: '#000', font: '6px BattleDialogue', spacing: { y: 3 }, size: 6 },
                     () => text
                  )
               ]
            })
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'toriDialoguer');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'toriDialoguer');
         }),
      // opponentDialoguer "box" (used for status text, check text, etc.)
      opponentDialoguer: new XNavigator({
         next: () => dialoguer.read(),
         prev: () => dialoguer.skip(),
         objects: [ menuText({ x: 26, y: 139 }, { font: '16px Dialogue', spacing: { y: 7 } }, () => text) ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'opponentDialoguer');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'opponentDialoguer');
         }),
      // bottomDialoguer box
      triviaDialoguer: new XNavigator({
         next: () => dialoguer.read(),
         prev: () => dialoguer.skip(),
         objects: [
            menuBox(
               { x: 16, y: 160, w: 283, h: 70 },
               menuText({ x: 11, y: 11 }, { font: '16px Dialogue', spacing: { y: 9 } }, () => text)
            )
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'triviaDialoguer');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'triviaDialoguer');
         }),
      //////////////////////////////////////////////////////////////////////////////////////////////
      //                                                                                          //
      //    FRONT-END                                                                             //
      //                                                                                          //
      //////////////////////////////////////////////////////////////////////////////////////////////
      frontEnd: new XNavigator({
         flip: true,
         grid: [
            [ 'frontEndContinue', 'frontEndReset' ],
            [ 'frontEndSettings', 'frontEndSettings' ]
         ],
         next: self => {
            const selection = self.selection() as string;
            if (selection === 'frontEndContinue') {
               spawn();
            } else {
               return selection;
            }
         },
         objects: [
            menuText({ x: 140 / 2, y: 132 / 2 }, {}, () => SAVE.name),
            menuText({ x: 264 / 2, y: 132 / 2 }, {}, () => `${turk('SV', 'LV')()} \xa0\xa0\xa0${manager.lv()}`),
            menuText({ x: 452 / 2, y: 132 / 2 }, {}, '?:??'),
            menuText(
               { x: 140 / 2, y: 168 / 2 },
               {},
               () => roomNames[SAVE.room]() || turk('Bilinmeyen Yer', 'Unknown Location')()
            ),
            menuText(
               { x: 85, y: 218 / 2 },
               {},
               () =>
                  (selection('frontEnd', 'frontEndContinue') ? '\xa7fill:#ffff00\xa7' : '') +
                  turk('Başla', 'Continue')()
            ),
            menuText(
               { x: 390 / 2, y: 218 / 2 },
               {},
               () => (selection('frontEnd', 'frontEndReset') ? '\xa7fill:#ffff00\xa7' : '') + turk('Sıfırla', 'Reset')()
            ),
            menuText(
               { x: 264 / 2, y: 258 / 2 },
               {},
               () =>
                  (selection('frontEnd', 'frontEndSettings') ? '\xa7fill:#ffff00\xa7' : '') +
                  turk('Ayarlar', 'Settings')()
            )
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'frontEnd');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'frontEnd');
         }),
      frontEndInstructions: new XNavigator({
         grid: [ [ 'frontEndName', 'frontEndSettings' ] ],
         next: 'frontEndName',
         objects: [
            menuText({ x: 85, y: 48 / 2 }, { color: '#c0c0c0' }, turk('--- Yönlendirmeler ---', '--- Instruction ---')),
            menuText(
               { x: 85, y: 108 / 2 },
               { color: '#c0c0c0' },
               turk('[Z ya da ENTER] - Onayla', '[Z or ENTER] - Confirm')
            ),
            menuText(
               { x: 85, y: 144 / 2 },
               { color: '#c0c0c0' },
               turk('[X ya da SHIFT] - İptal Et', '[X or SHIFT] - Cancel')
            ),
            menuText(
               { x: 85, y: 180 / 2 },
               { color: '#c0c0c0' },
               turk('[C ya da CTRL] - Menü (Oyun İçi)', '[C or CTRL] - Menu (In-game)')
            ),
            menuText({ x: 85, y: 216 / 2 }, { color: '#c0c0c0' }, turk('[F4] - Tam Ekran', '[F4] - Fullscreen')),
            menuText(
               { x: 85, y: 252 / 2 },
               { color: '#c0c0c0' },
               turk('[ESC Basılı Tut] - Çıkış', '[Hold ESC] - Quit')
            ),
            menuText(
               { x: 85, y: 288 / 2 },
               { color: '#c0c0c0' },
               turk('Canın 0 olunca, kaybedersin.', 'When HP is 0, you lose.')
            ),
            menuText(
               { x: 85, y: 352 / 2 },
               {},
               () =>
                  (selection('frontEndInstructions', 'frontEndName') ? '\xa7fill:#ffff00\xa7' : '') +
                  turk('Başla', 'Begin Game')()
            ),
            menuText(
               { x: 85, y: 392 / 2 },
               {},
               () =>
                  (selection('frontEndInstructions', 'frontEndSettings') ? '\xa7fill:#ffff00\xa7' : '') +
                  turk('Ayarlar', 'Settings')()
            ),
            menuText(
               { x: 160, y: 232 },
               { anchor: { y: 2, x: 0 }, color: '#808080', font: '8px Stat', size: 5 },
               turk('HİKAYE ANLATICI MOTORU V1.0.0 (SPACEY_432 TARAFINDAN)', 'STORYTELLER ENGINE V1.00 (BY SPACEY_432)')
            )
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'frontEndInstructions');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'frontEndInstructions');
         }),
      frontEndName: new XNavigator({
         flip: true,
         grid: () => [
            [ 'A', 'B', 'C', 'D', 'E', 'F', 'G' ],
            [ 'H', 'I', 'J', 'K', 'L', 'M', 'N' ],
            [ 'O', 'P', 'Q', 'R', 'S', 'T', 'U' ],
            [ 'V', 'W', 'X', 'Y', 'Z' ],
            [ 'a', 'b', 'c', 'd', 'e', 'f', 'g' ],
            [ 'h', 'i', 'j', 'k', 'l', 'm', 'n' ],
            [ 'o', 'p', 'q', 'r', 's', 't', 'u' ],
            [ 'v', 'w', 'x', 'y', 'z' ],
            [ 'quit', 'backspace', 'done' ]
         ],
         next: self => {
            const selection = self.selection() as string;
            if (name.toLowerCase() === 'gaste' && selection.toLowerCase() === 'r') {
               window.close();
               renderer.alpha.value = 0;
               music.intro.source.gain.value = 0;
               return null;
            } else if (selection === 'quit') {
               name = '';
               return 'frontEndInstructions';
            } else if (selection === 'backspace') {
               if (name.length > 0) {
                  name = name.slice(0, -1);
               }
            } else if (selection === 'done') {
               if (name.length > 0) {
                  return 'frontEndReset';
               }
            } else if (name.length < 6) {
               name += selection;
            }
         },
         prev: () => {
            name = '';
            return 'frontEndInstructions';
         },
         objects: [
            menuText({ x: 180 / 2, y: 68 / 2 }, {}, turk('Düşmüş insanı adlandır.', 'Name the fallen human.')),
            menuText({ x: 140, y: 118 / 2 }, {}, () => name),
            menuText(
               { x: 60, y: 204 },
               {},
               () => (selection('frontEndName', 'quit') ? '\xa7fill:#ffff00\xa7' : '') + turk('Çık', 'Quit')()
            ),
            menuText(
               { x: 120, y: 204 },
               {},
               () =>
                  (selection('frontEndName', 'backspace') ? '\xa7fill:#ffff00\xa7' : '') +
                  turk('Geri Dön', 'Backspace')()
            ),
            menuText(
               { x: 220, y: 204 },
               {},
               () => (selection('frontEndName', 'done') ? '\xa7fill:#ffff00\xa7' : '') + turk('Bitir', 'Done')()
            ),
            ...new Array(55).fill(0).map((x, index) => {
               if (index < 52) {
                  const char = index % 26;
                  const letter = letters[char][index < 26 ? 'toUpperCase' : 'slice']();
                  return menuText(
                     { x: 60 + 32 * (char % 7), y: (index < 26 ? 79 : 139) + 14 * Math.floor(char / 7) },
                     {},
                     () =>
                        `${selection('frontEndName', letter) ? '\xa7fill:#ffff00\xa7' : ''}\xa7random:1,1\xa7${letter}`
                  );
               }
            })
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'frontEndName');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'frontEndName');
         }),
      frontEndReset: new XNavigator({
         flip: true,
         grid: [ [ 'no', 'yes' ] ],
         next: self => {
            if (self.selection() === 'no') {
               return SAVE.name ? 'frontEnd' : 'frontEndName';
            } else {
               if (SAVE.name) {
                  // save all persistent flags
                  const flags = {} as XKeyed<XBasic>;
                  for (const flag in SAVE.flags) {
                     flag[0] === '$' && (flags[flag] = SAVE.flags[flag]);
                  }
                  // reset data, load previous name & persistent flags
                  Object.assign(manager, manager.default(), { flags, name: SAVE.name });
               } else {
                  SAVE.name = name;
               }
               spawn();
            }
         },
         prev: 'frontEnd',
         objects: [
            menuText(
               { x: 100, y: 135 },
               { font: '32px Menu', size: 18, spacing: { x: 2.5 } },
               () => `\xa7random:2,3\xa7${name}`
            ),
            menuText(
               { x: 78, y: 204 },
               {},
               () => (selection('frontEndReset', 'no') ? '\xa7fill:#ffff00\xa7' : '') + turk('Hayır', 'No')()
            ),
            menuText(
               { x: 230, y: 204 },
               {},
               () => (selection('frontEndReset', 'yes') ? '\xa7fill:#ffff00\xa7' : '') + turk('Evet', 'Yes')()
            ),
            menuText({ x: 90, y: 34 }, { spacing: { y: 7 } }, () => {
               if (SAVE.name) {
                  return turk('Bir isim çoktan\nSeçilmiş.', 'A name has already\nbeen chosen.')();
               } else {
                  const lowerName = name.toLowerCase();
                  if (lowerName in easterEggs) {
                     const easterEgg = easterEggs[lowerName];
                     return (typeof easterEgg === 'function' ? easterEgg() : easterEgg) || 'Is this name correct?';
                  } else {
                     return turk('Bu isim doğru mu?', 'Is this name correct?')();
                  }
               }
            })
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'frontEndReset');
         })
         .wrapOn('to', self => {
            return () => {
               self.position = { x: 0, y: 0 };
               atlas.detach(renderer, 'menu', 'frontEndReset');
            };
         }),
      frontEndSettings: new XNavigator({
         prev: () => (SAVE.name ? 'frontEnd' : 'frontEndInstructions'),
         grid: [ [ 'exit', 'language' ] ],
         next: self => {
            switch (self.selection()) {
               case 'exit':
                  return SAVE.name ? 'frontEnd' : 'frontEndInstructions';
               case 'language':
                  cycleOption(SAVE.flags, 'settings_language', 'ENGLISH', 'TÜRKÇE');
                  break;
               case 'debug':
                  cycleOption(SAVE.flags, 'settings_debug', 'DISABLED', 'ENABLED');
                  renderer.debug = SAVE.flags.settings_debug === 'ENABLED';
                  break;
            }
         },
         objects: [
            menuText({ x: 104, y: 18 }, { font: '32px Menu', size: 18 }, turk('AYARLAR', 'SETTINGS')),
            menuText(
               { x: 20, y: 44 },
               {},
               () => (selection('frontEndSettings', 'exit') ? '\xa7fill:#ffff00\xa7' : '') + turk('Çıkış', 'EXIT')()
            ),
            menuText(
               { x: 20, y: 74 },
               {},
               () =>
                  (selection('frontEndSettings', 'language') ? '\xa7fill:#ffff00\xa7' : '') + turk('DİL', 'LANGUAGE')()
            ),
            menuText({ x: 92, y: 74 }, {}, () => SAVE.flags.settings_language as string)
         ]
      })
         .on('from', () => {
            atlas.attach(renderer, 'menu', 'frontEndSettings');
         })
         .on('to', () => {
            atlas.detach(renderer, 'menu', 'frontEndSettings');
         }),
      //////////////////////////////////////////////////////////////////////////////////////////////
      //                                                                                          //
      //    BATTLE                                                                                //
      //                                                                                          //
      //////////////////////////////////////////////////////////////////////////////////////////////
      battle: new XNavigator({
         grid: [ [ 'FIGHT', 'ACT', 'ITEM', 'MERCY' ] ],
         flip: true,
         next: self => {
            const selection = self.selection() as string;
            if (selection !== 'ITEM' || SAVE.items.length > 0) {
               sounds.select.start();
               battler.next(selection);
               return 'battleSelector';
            }
         },
         objects: [
            // the battle box & hitboxes
            new XRectangle({
               anchor: { x: 0, y: 1 },
               position: { x: 160, y: 193.75 },
               fill: '#000000',
               stroke: '#ffffff',
               line: { width: 2.5 },
               objects: [
                  // bottom
                  new XHitbox({
                     anchor: { x: 0 },
                     metadata: { barrier: true },
                     position: { y: -1.25 },
                     size: { y: 10 }
                  }).wrapOn('tick', self => {
                     return () => {
                        self.size.x = TORIEL.box.x;
                     };
                  }),
                  // left
                  new XHitbox({
                     metadata: { barrier: true },
                     position: { y: -1.25 },
                     size: { x: -10 }
                  }).wrapOn('tick', self => {
                     return () => {
                        self.position.x = TORIEL.box.x / -2;
                        self.size.y = -TORIEL.box.y;
                     };
                  }),
                  // right
                  new XHitbox({
                     metadata: { barrier: true },
                     size: { x: 10 },
                     position: { y: -1.25 }
                  }).wrapOn('tick', self => {
                     return () => {
                        self.position.x = TORIEL.box.x / 2;
                        self.size.y = -TORIEL.box.y;
                     };
                  }),
                  // top
                  new XHitbox({
                     anchor: { x: 0 },
                     metadata: { barrier: true },
                     size: { y: -10 }
                  }).wrapOn('tick', self => {
                     return () => {
                        self.position.y = -1.25 - TORIEL.box.y;
                        self.size.x = TORIEL.box.x;
                     };
                  })
               ]
            }).wrapOn('tick', self => {
               return () => {
                  self.size.x = TORIEL.box.x + 2.5;
                  self.size.y = TORIEL.box.y + 2.5;
               };
            }),
            menuText(
               { x: 13.5, y: 200 },
               { font: '12px BattleName', size: 2 },
               () => `${SAVE.name}   ${turk('SV', 'LV')()} ${manager.lv()}`
            ),
            menuText(
               { x: manager.hp() * 0.6 + 143.5, y: 200 },
               { font: '12px BattleName', size: 2 },
               () => `${SAVE.hp.toString().padStart(2, '0')} / ${manager.hp().toString().padStart(2, '0')}`
            ),
            new XRectangle({
               position: { x: 137.5, y: 200 },
               size: { x: manager.hp() * 0.6 + 0.5, y: 10.5 },
               fill: '#ff0000',
               stroke: '#00000000'
            }),
            new XRectangle({
               position: { x: 137.5, y: 200 },
               size: { y: 10.5 },
               fill: '#ffff00',
               stroke: '#00000000'
            }).wrapOn('tick', self => {
               return () => (self.size.x = SAVE.hp * 0.6 + 0.5);
            })
         ]
      })
         .wrapOn('from', self => {
            return (atlas, key) => {
               fromOther = key === 'battleSelector';
               self.fire('move', atlas);
               fromOther = false;
               dialoguer.text(...TORIEL.status);
               atlas.attach(renderer, 'menu', 'opponentDialoguer');
            };
         })
         .on('to', atlas => {
            dialoguer.text('');
            atlas.detach(renderer, 'menu', 'opponentDialoguer');
         })
         .wrapOn('move', self => {
            return () => {
               game.player.alpha.value = 1;
               Object.assign(game.player.position, { x: [ 24, 100.5, 180.5, 258 ][self.position.y], y: 227 });
               fromOther || sounds.menu.start();
               const selection = self.selection();
               Object.entries(buttons).map(([ key, value ]) => {
                  if (key.toUpperCase() === selection) {
                     value.state.index = 1;
                  } else {
                     value.state.index = 0;
                  }
               });
            };
         }),
      battleSelector: new XNavigator({
         grid: () => {
            const rows: string[][] = [ [], [], [] ];
            const cols: string[][] = [];
            battler
               .list()
               .filter((choice, index) => choice !== 'ITEM2' || index < SAVE.items.length)
               .map((choice, index) =>
                  rows[Math.floor((index % 6) / 2)].push(choice !== 'ITEM2' ? choice : `ITEM2_${index}`)
               );
            for (const row of rows) {
               row.forEach((choice, index) => (cols[index] || (cols[index] = [])).push(choice));
            }
            return cols;
         },
         next: self => {
            sounds.select.start();
            const selection = self.selection() as any;
            if (selection.startsWith('ITEM2')) {
               global.item = +selection.split('_')[1];
               battler.next(0);
            } else {
               battler.next(battler.list().indexOf(selection));
               self.position = { x: 0, y: 0 };
            }
         },
         prev: self => {
            if (battler.prev()) {
               return 'battle';
            } else {
               self.position = { x: 0, y: 0 };
            }
         },
         objects: new Array(6).fill(0).map((x, index) => {
            return menuText(
               { x: index % 2 === 0 ? 48.5 : 176.5, y: 137.5 + Math.floor(index / 2) * 16 },
               { font: '16px Dialogue' },
               () => {
                  const key = battler.list().slice(Math.floor(atlas.navigators.battleSelector.position.x / 2) * 6)[
                     index
                  ];
                  if (key === 'ITEM2') {
                     const item = SAVE.items[index];
                     if (item) {
                        if (SAVE.flags.settings_language === 'TÜRKÇE') {
                           const turk = manager.items[item].turk;
                           return typeof turk === 'function' ? turk() : turk;
                        } else {
                           return item;
                        }
                     } else {
                        return '';
                     }
                  } else {
                     return TORIEL.text[key] || '';
                  }
               }
            );
         })
      })
         .wrapOn('from', self => {
            return (x, key) => {
               fromOther = key === 'battle';
               self.fire('move', atlas);
               fromOther = false;
               self.position = { x: 0, y: 0 };
               atlas.attach(renderer, 'foreground', 'battleSelector');
            };
         })
         .on('to', (x, key) => {
            key === 'battle' || Object.values(buttons).forEach(value => (value.state.index = 0));
            atlas.detach(renderer, 'foreground', 'battleSelector');
         })
         .wrapOn('move', self => {
            return () => {
               game.player.alpha.value = 1;
               Object.assign(game.player.position, {
                  x: [ 36, 166 ][self.position.x],
                  y: 143 + self.position.y * 16
               });
               fromOther || sounds.menu.start();
            };
         })
   }
});
