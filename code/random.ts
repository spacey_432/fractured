import { XKeyed } from "./storyteller";

const generators = {} as XKeyed<() => number>;

export function random (seed: string) {
   if (seed in generators) {
      return generators[seed]();
   } else {
      let h = 1779033703 ^ seed.length;
      for (let i = 0; i < seed.length; i++)
         (h = Math.imul(h ^ seed.charCodeAt(i), 3432918353)), (h = (h << 13) | (h >>> 19));
      let [ a, b, c, d ] = new Array(4).fill(0).map(() => {
         h = Math.imul(h ^ (h >>> 16), 2246822507);
         h = Math.imul(h ^ (h >>> 13), 3266489909);
         return (h ^= h >>> 16) >>> 0;
      });
      a >>>= 0;
      b >>>= 0;
      c >>>= 0;
      d >>>= 0;
      let t = (a + b) | 0;
      a = b ^ (b >>> 9);
      b = (c + (c << 3)) | 0;
      c = (c << 21) | (c >>> 11);
      d = (d + 1) | 0;
      t = (t + d) | 0;
      c = (c + t) | 0;
      let base = (t >>> 0) / 4294967296;
      return (generators[seed] = () => {
         var hash = (base += 0x6d2b79f5);
         hash = Math.imul(hash ^ (hash >>> 15), hash | 1);
         hash ^= hash + Math.imul(hash ^ (hash >>> 7), hash | 61);
         return ((hash ^ (hash >>> 14)) >>> 0) / 4294967296;
      })();
   }
}
