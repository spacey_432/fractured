import * as assets from './assets';
import { TORIEL } from './battler';
import { player } from './player';
import { SAVE } from './data';
import { X2, XCardinal, XHitbox, XHost, XImage, XKeyed, XSprite } from './storyteller';
import { XGame } from './undertale';

type GameRooms = Exclude<Exclude<Parameters<typeof XGame['build']>[0], void>['rooms'], void>;

function roomFactory (
   id: string,
   name: string,
   turk: string,
   background: XImage | null,
   height: number,
   { h = 0, w = 0, x = 0, y = 0 }: Partial<XKeyed<number, 'h' | 'w' | 'x' | 'y'>>,
   doors: XKeyed<{
      bounds: Partial<XKeyed<number, 'h' | 'w' | 'x' | 'y'>>;
      destination: string;
      direction: XCardinal;
      distance: number;
   }>,
   spawn: Partial<X2>,
   walls: Partial<XKeyed<number, 'h' | 'w' | 'x' | 'y'>>[],
   extra: Exclude<GameRooms[''], void>['layers']
) {
   const z = Math.max(height - 240, 0);
   spawns[id] = { x: spawn.x || 0, y: spawn.y || 0 };
   const room: Required<GameRooms['']> = {
      region: [
         { x: x + 160, y: y + 120 },
         { x: x + w + 160, y: y + h + 120 }
      ],
      layers: {
         background: [
            ...(background ? [ new XSprite({ frames: [ background ] }) ] : []),
            ...walls.map(({ h = 0, w = 0, x = 0, y = 0 }) => {
               return new XHitbox({
                  metadata: { barrier: true },
                  position: { x, y: 240 - (y - z) },
                  size: { x: w, y: -h }
               });
            })
         ],
         foreground: [
            player,
            ...Object.entries(doors).map(
               ([
                  key,
                  {
                     bounds: { h = 0, w = 0, x = 0, y = 0 },
                     destination,
                     direction,
                     distance
                  }
               ]) => {
                  const destie = destinations[id] || (destinations[id] = {});
                  destie[key] = {
                     direction,
                     position: {
                        x: { down: x + w / 2, left: x - distance, right: x + w + distance, up: x + w / 2 }[direction],
                        y:
                           240 -
                           ({ down: y - distance, left: y + h / 2, right: y + h / 2, up: y + h + distance }[direction] -
                              z)
                     }
                  };
                  return new XHitbox({
                     metadata: { action: 'door', destination, key },
                     position: { x, y: 240 - (y - z) },
                     size: { x: w, y: -h }
                  }).wrapOn('tick', self => {
                     return () => detector.fire('tick', self);
                  });
               }
            )
         ]
      }
   };
   for (const garbo in extra) {
      (room.layers[garbo] || (room.layers[garbo] = [])).push(...(extra[garbo] || []));
   }
   roomNames[id] = () => (SAVE.flags.settings_language === 'TÜRKÇE' ? turk : name);
   return room;
}

function triviaText (
   { h = 0, w = 0, x = 0, y = 0 }: Partial<XKeyed<number, 'h' | 'w' | 'x' | 'y'>>,
   interact: boolean,
   flag: string,
   ...text: ((string | (() => string))[] | null)[]
) {
   return new XHitbox({
      priority: 10,
      //@ts-expect-error
      metadata: { action: 'trivia', interact, text, flag },
      size: { x: w, y: h },
      position: { x, y }
   }).wrapOn('tick', self => {
      return () => detector.fire('tick', self);
   });
}

function savePoint (position: X2) {
   return new XHitbox({
      anchor: { x: 0, y: 0 },
      priority: -1,
      position,
      metadata: { action: 'save', interact: true },
      size: { x: 30, y: 30 },
      objects: [
         new XHitbox({
            size: { x: 20, y: 20 },
            anchor: { x: 0, y: 0 },
            metadata: { barrier: true }
         }),
         new XSprite({
            anchor: { x: 0, y: 0 },
            frames: [ assets.content.save0, assets.content.save1 ],
            steps: 6
         }).enable()
      ]
   }).wrapOn('tick', host => {
      return () => detector.fire('tick', host);
   });
}

export const destinations = {} as XKeyed<XKeyed<{ direction: XCardinal; position: X2 }>>;
export const detector = new XHost<{ tick: [XHitbox] }>();
export const spawns = {} as XKeyed<X2>;
export const roomNames = {} as XKeyed<() => string>;

export const rooms: GameRooms = {
   castleRoad: roomFactory(
      'castleRoad',
      'New Home - Castle End',
      'Yeni Ev - Kale Sonu',
      assets.content.castleRoadBackground,
      240,
      { x: -1760, w: 1820 },
      {
         x: {
            bounds: { h: 10, w: 60, x: 120 },
            direction: 'up',
            distance: 10,
            destination: 'lastCorridor'
         },
         $x: {
            bounds: { x: -1760, y: 40, h: 40, w: 20 },
            distance: 10,
            direction: 'right',
            destination: 'castleRoad'
         }
      },
      { x: 160, y: 140 },
      [
         { y: 0, h: 40, x: 120, w: -1880 },
         { x: 180, y: 0, h: 40, w: 120 },
         { x: 300, y: 40, h: 40, w: 20 },
         { y: 80, h: 20, w: -2060, x: 300 }
      ],
      {
         background: [
            new XHitbox({
               size: { x: 20, y: -60 },
               position: { x: -800, y: 220 }
            }).wrapOn('tick', hitbox => {
               return () => {
                  // teleport to room start
               };
            }),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop1 ];
               return new Array(2).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.9 },
                     priority: -10,
                     position: { x: index * -320 },
                     frames
                  });
               });
            })(),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop2 ];
               return new Array(3).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.7 },
                     priority: -9,
                     position: { x: index * -300, y: 50 },
                     frames
                  });
               });
            })(),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop3 ];
               return new Array(4).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.6 },
                     priority: -8,
                     position: { x: index * -320 + 90, y: 30 },
                     frames
                  });
               });
            })(),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop4 ];
               return new Array(5).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.5 },
                     priority: -7,
                     position: { x: index * -320 + 90, y: 85 },
                     frames
                  });
               });
            })(),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop5 ];
               return new Array(5).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.4 },
                     priority: -6,
                     position: { x: index * -350 + 90, y: 80 },
                     frames
                  });
               });
            })(),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop6 ];
               return new Array(5).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.2 },
                     priority: -5,
                     position: { x: index * -300 - 20, y: 100 },
                     frames
                  });
               });
            })(),
            ...(() => {
               const frames = [ assets.content.castleRoadBackdrop7 ];
               return new Array(6).fill(0).map((x, index) => {
                  return new XSprite({
                     parallax: { x: 0.1 },
                     priority: -4,
                     position: { x: index * -320 + 30, y: 40 },
                     frames
                  });
               });
            })(),
            ...new Array(22).fill(0).map((x, index2) => {
               const position = { y: 160, x: index2 * -80 - 80 };
               return new XSprite({
                  position,
                  crop: { right: -80, top: -80 },
                  frames: [ assets.content.castleRoadBackground ]
               });
            })
         ],
         foreground: [
            //
            triviaText(
               { x: 240, y: 160, h: -20, w: 40 },
               true,
               'amogus1',
               [
                  '{@fill:#ffffff}{#c:narrator}{#i:40}* It seems the elevator has been\n  ripped out of the wall.',
                  '{@fill:#ff0000}* Only one way forward now.'
               ],
               [
                  "{@fill:#ff0000}{#c:narrator}{#i:40}* There's nothing special here.{^10}",
                  '{@fill:#ff0000}* Stop wasting time.'
               ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* ...' ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* ...' ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* ...' ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* Really now.' ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* ...' ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* ...' ],
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* ...' ],
               [ "{@fill:#ff0000}{#c:narrator}{#i:40}* Don't you have anything more\n  intelligent to do?" ]
            ),
            triviaText(
               { x: 0, y: 200, h: -40, w: 20 },
               false,
               'amogus2',
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* Wrong way.' ],
               null
            ),
            triviaText(
               { x: -150, y: 200, h: -40, w: 20 },
               false,
               'amogus3',
               [ "{@fill:#ff0000}{#c:narrator}{#i:40}* Mind telling me why you're\n  heading backwards?" ],
               null
            ),
            triviaText(
               { x: -600, y: 200, h: -40, w: 20 },
               false,
               'amogus4',
               [ "{@fill:#ff0000}{#c:narrator}{#i:40}* Don't tell me you're having\n  doubts NOW..." ],
               null
            ),
            triviaText(
               { x: -1200, y: 200, h: -40, w: 20 },
               false,
               'amogus5',
               [
                  "{@fill:#ff0000}{#c:narrator}{#i:40}* If you keep going the way\n  you are, I'll have no choice\n  but to force your hand."
               ],
               null
            ),
            triviaText(
               { x: -1720, y: 200, h: -40, w: 20 },
               false,
               'amogus6',
               [
                  "{@fill:#ff0000}{#c:narrator}{#i:40}* Take one more step, and I\n  won't hesitate to turn this\n  vessel around."
               ],
               null
            )
         ]
      }
   ),
   lastCorridor: roomFactory(
      'lastCorridor',
      'Last Corridor',
      'Son Koridor',
      assets.content.lastCorridorBackground,
      240,
      { w: 1000 },
      {
         x: {
            bounds: { h: 20, w: 40, x: 140, y: 121 },
            destination: 'castleRoad',
            direction: 'down',
            distance: 8
         },
         y: {
            bounds: { h: 20, w: 40, x: 1220, y: 121 },
            destination: 'castleHook',
            direction: 'down',
            distance: 8
         }
      },
      { x: 160, y: 130 },
      [
         { h: 20, w: 1200, x: 100, y: 20 },
         { h: 20, w: 40, x: 100, y: 120 },
         { h: 20, w: 1040, x: 180, y: 120 },
         { h: 20, w: 40, x: 1260, y: 120 },
         { h: 80, w: 20, x: 80, y: 40 },
         { h: 80, w: 20, x: 1300, y: 40 },
         { h: 20, w: 60, x: 240, y: 100 },
         { h: 20, w: 60, x: 360, y: 100 },
         { h: 20, w: 60, x: 480, y: 100 },
         { h: 20, w: 60, x: 600, y: 100 },
         { h: 20, w: 60, x: 720, y: 100 },
         { h: 20, w: 60, x: 840, y: 100 },
         { h: 20, w: 60, x: 960, y: 100 },
         { h: 20, w: 60, x: 1080, y: 100 }
      ],
      {
         foreground: [
            savePoint({ x: 120, y: 120 }),
            triviaText(
               { x: 500, y: 200, h: -80, w: 20 },
               false,
               'sus1',
               [
                  '{@fill:#ff0000}{#c:narrator}{#i:40}* Good work back there with Sans\n  and Undyne.',
                  "{@fill:#ff0000}* They really didn't stand a\n  chance, {^4}did they?",
                  '{@fill:#ff0000}* ...',
                  '{@fill:#ff0000}* Hee hee hee...',
                  '{@fill:#ff0000}{@random:1,1}* 1 left.'
               ],
               null
            ),
            triviaText(
               { x: 800, y: 200, h: -80, w: 20 },
               false,
               'sus2',
               [ '{@fill:#ff0000}{#c:narrator}{#i:40}* Calm down.', '{@fill:#ff0000}* It was only your imagination.' ],
               null
            ),
            new XHitbox({
               metadata: { action: 'snas' },
               position: { x: 770, y: 200 },
               size: { x: 20, y: -80 }
            }).wrapOn('tick', self => {
               return () => detector.fire('tick', self);
            })
         ],
         overlay: [ 340, 230, 230, 230, 230, 230, 110, 120, 230, 230 ].map((x, index, array) => {
            let position = 0;
            for (const margin of array.slice(0, index + 1)) {
               position += margin;
            }
            return new XSprite({
               parallax: { x: -1 },
               position: { x: position },
               frames: [ assets.content.pillar ]
            });
         })
      }
   ),
   castleHook: roomFactory(
      'castleHook',
      'The Capitol - Main Hall',
      'Başkent - Meydan',
      assets.content.castleHookBackground,
      540,
      { x: 10, h: 300, w: 500 },
      {
         x: {
            bounds: { h: 20, w: 60, x: 360, y: 421 },
            destination: 'throneRoom',
            direction: 'down',
            distance: 8
         },
         y: {
            bounds: { h: 10, w: 60, x: 140 },
            destination: 'lastCorridor',
            direction: 'up',
            distance: 8
         },
         z: {
            bounds: { h: 10, w: 60, x: 640, y: 0 },
            destination: 'coffinStairs',
            direction: 'up',
            distance: 8
         }
      },
      { x: 390, y: 130 },
      [
         { h: 420, w: 20, x: 120, y: 0 },
         { h: 360, w: 20, x: 200, y: 0 },
         { h: 420, w: 20, x: 700, y: 0 },
         { h: 360, w: 20, x: 620, y: 0 },
         { h: 20, w: 400, x: 220, y: 340 },
         { h: 20, w: 220, x: 140, y: 420 },
         { h: 20, w: 280, x: 420, y: 420 }
      ],
      {
         background: [
            new XSprite({ frames: [ assets.content.castleHookScribbleBackground ] }).wrapOn('tick', self => {
               return () => (self.alpha.value = SAVE.fun > 75 && SAVE.fun < 80 ? 1 : 0);
            })
         ],
         foreground: [
            savePoint({ x: 340, y: 120 }),
            triviaText(
               { x: 140, y: 500, h: -20, w: 60 },
               false,
               'impostor1',
               [
                  '{@fill:#ff0000}{#c:narrator}{#i:40}* This is monster dust...',
                  '{@fill:#ff0000}* ...',
                  '{@fill:#ff0000}* What the HELL is going on here?'
               ],
               null
            ),
            triviaText({ x: 175, y: 400, h: -30, w: 25 }, true, 'impostor2', [
               "{#c:narrator}{#i:40}* It's a pile of dust.\n{^8}* It looks burnt."
            ]),
            triviaText({ x: 140, y: 145, h: -30, w: 30 }, true, 'impostor3', [
               '{#c:narrator}{#i:40}* A pile of dust in the corner.\n{^8}* It seems to be burnt.'
            ]),
            triviaText({ x: 260, y: 125, h: -20, w: 40 }, true, 'impostor4', [
               '{#c:narrator}{#i:40}* Throne Room',
               () =>
                  SAVE.fun > 30 && SAVE.fun < 40
                     ? `{#c:darkflower}{#i:40}{@random:1,1}* She's waiting for you, {^4}${SAVE.name}.`
                     : ''
            ]),
            triviaText({ x: 580, y: 120, h: 20, w: 33 }, true, 'impostor5', [
               '{#c:narrator}{#i:40}* More dust.\n{^8}* Another burnt floor tile.'
            ]),
            triviaText({ x: 510, y: 125, h: -20, w: 40 }, true, 'impostor6', [
               '{#c:gorey}{#i:65}* Head through the throne room\n  to reach the surface!\n    - Asgore'
            ]),
            triviaText({ x: 635, y: 125, h: -20, w: 40 }, true, 'impostor7', [
               '{#c:gorey}{#i:65}* There is nothing to see down\n  there. Please turn back.\n    - Asgore'
            ]),
            triviaText({ x: 420, y: 125, h: -20, w: 20 }, true, 'impostor8', [
               () =>
                  SAVE.fun > 75 && SAVE.fun < 80
                     ? '{@fill:#ff0000}{#c:tori}{#i:55}{@random:1,1}* Come in, my child...'
                     : '{#c:narrator}{#i:40}* Strangely, there are faint\n  scratch marks in the wall here.',
               () =>
                  SAVE.fun > 75 && SAVE.fun < 80
                     ? '{@fill:#ff0000}{@random:1,1}* I have a {@random:3,3}surprise {@random:1,1}for you!\n\n    - Mother Toriel'
                     : '* You cannot make out the words.'
            ]),
            triviaText({ x: 678, y: 203, h: 29, w: 27 }, true, 'impostor9', [
               '{#c:narrator}{#i:40}* Another dust pile.\n{^8}* Burnt, just like the rest.'
            ]),
            triviaText({ x: 640, y: 352, h: 30, w: 25 }, true, 'impostor10', [
               '{#c:narrator}{#i:40}* Yet another pile of dust.\n{^8}* Why there are so many here\n  puzzles you.'
            ])
         ]
      }
   ),
   coffinStairs: roomFactory(
      'coffinStairs',
      'The Capitol - Annex',
      'Başkent',
      assets.content.coffinStairsBackground,
      540,
      { x: -10, h: 300 },
      {
         x: {
            bounds: { h: 10, w: 60, x: 120 },
            destination: 'coffinRoom',
            direction: 'up',
            distance: 8
         },
         z: {
            bounds: { h: 10, w: 60, x: 120, y: 530 },
            destination: 'castleHook',
            direction: 'down',
            distance: 8
         }
      },
      { x: 160, y: 130 },
      [
         { h: 540, w: 20, x: 100, y: 0 },
         { h: 540, w: 20, x: 180, y: 0 }
      ]
   ),
   coffinRoom: roomFactory(
      'coffinRoom',
      'The Capitol - Storage',
      'Başkent - Depo',
      assets.content.coffinRoomBackground,
      240,
      {},
      {
         x: {
            bounds: { h: 10, w: 40, x: 60, y: 101 },
            destination: 'coffinStairs',
            direction: 'down',
            distance: 8
         }
      },
      { x: 160, y: 130 },
      [
         { h: 60, w: 20, x: 0, y: 40 },
         { h: 20, w: 40, x: 20, y: 100 },
         { h: 20, w: 40, x: 100, y: 100 },
         { h: 60, w: 20, x: 140, y: 40 },
         { h: 20, w: 120, x: 20, y: 20 }
      ],
      {
         background: [
            new XSprite({ frames: [ assets.content.coffinRoomMonsterBackground ] }).wrapOn('tick', self => {
               return () => (self.alpha.value = SAVE.fun === 77 ? 1 : 0);
            })
         ],
         foreground: [
            triviaText(
               { x: 135, y: 190, h: -40, w: 20 },
               true,
               'garbo1234',
               [
                  () =>
                     SAVE.fun === 77
                        ? "{@fill:#ff0000}{#c:narrator}{#i:40}* My coffin's been VANDALIZED!\n"
                        : '{#c:narrator}{#i:40}* The resting place of the first\n  fallen child.',
                  () => (SAVE.fun === 77 ? '{@fill:#ff0000}* ...' : '* You deem it wise to let it be.'),
                  () =>
                     SAVE.fun === 77
                        ? "{@fill:#ff0000}* When I find out who's behind\n  this, I WILL make them PAY."
                        : '',
                  () =>
                     SAVE.fun === 77 ? '{@fill:#ff0000}{@random:1,2}* NOBODY {@random:0,0}desecrates MY coffin!' : '',
                  () => (SAVE.fun === 77 ? '{@fill:#ff0000}{@random:2,3}* NOBODY!!' : '')
               ],
               [
                  () =>
                     SAVE.fun === 77
                        ? "{@fill:#ff0000}* I don't wanna see it anymore.\n  * Let's get out of here."
                        : "{#c:narrator}{#i:40}* It's a coffin."
               ],
               [ () => (SAVE.fun === 77 ? '{@fill:#ff0000}* ...' : "{#c:narrator}{#i:40}* It's a coffin.") ],
               [ () => (SAVE.fun === 77 ? '{@fill:#ff0000}* ...' : "{#c:narrator}{#i:40}* It's a coffin.") ],
               [ () => (SAVE.fun === 77 ? '{@fill:#ff0000}* ...' : "{#c:narrator}{#i:40}* It's a coffin.") ],
               [
                  () =>
                     SAVE.fun === 77
                        ? "{@fill:#ff0000}* Come on, let's go already."
                        : "{#c:narrator}{#i:40}* It's a coffin."
               ]
            )
         ]
      }
   ),
   throneRoom: roomFactory(
      'throneRoom',
      'The Capitol - Throne Room',
      'Başkent - Taht Odası',
      assets.content.throneRoomBackground,
      640,
      { h: 380 },
      {
         x: {
            bounds: { h: 10, w: 40, x: 140 },
            destination: 'castleHook',
            direction: 'up',
            distance: 8
         },
         y: {
            bounds: { h: 20, w: 60, x: 80, y: 480 },
            destination: 'battle',
            direction: 'down',
            distance: 8
         }
      },
      { x: 160, y: 130 },
      [
         { h: 320, w: 20, x: 20, y: 120 },
         { h: 20, w: 20, x: 40, y: 100 },
         { h: 40, w: 20, x: 40, y: 440 },
         { h: 20, w: 40, x: 60, y: 80 },
         { h: 20, w: 20, x: 60, y: 480 },
         { h: 20, w: 40, x: 100, y: 60 },
         { h: 60, w: 20, x: 120, y: 0 },
         { h: 35, w: 45, x: 140, y: 360 },
         { h: 20, w: 120, x: 140, y: 480 },
         { h: 60, w: 20, x: 180, y: 0 },
         { h: 20, w: 40, x: 180, y: 60 },
         { h: 35, w: 70, x: 210, y: 120 }, // aa
         { h: 20, w: 40, x: 220, y: 80 },
         { h: 20, w: 20, x: 260, y: 100 },
         { h: 40, w: 20, x: 260, y: 440 },
         { h: 320, w: 20, x: 280, y: 120 }
      ],
      {
         foreground: [
            new XSprite({
               frames: [ assets.content.throneRoomDecoration ]
            }),
            new XHitbox({
               metadata: { action: 'goatmom' },
               position: { x: 40, y: 450 },
               size: { x: 240, y: -20 }
            }).wrapOn('tick', self => {
               return () => detector.fire('tick', self);
            }),
            triviaText({ x: 195, y: 530, h: 50, w: 50 }, true, 'amoggers1', [
               '{#c:narrator}{#i:40}* A pile of dust.\n{^8}* The garden has been scorched.'
            ]),
            triviaText({ x: 55, y: 465, h: 40, w: 40 }, true, 'amoggers2', [
               '{#c:narrator}{#i:40}* This dust pile is huge!\n{^8}* You wonder who it was that\n  died here.'
            ]),
            triviaText({ x: 210, y: 479, h: 45, w: 80 }, true, 'amoggers3', [
               '{#c:narrator}{#i:40}* Seems this chair has been\n  thrown across the room.\n{^8}* Someone was not in a good mood.'
            ])
         ],
         overlay: [
            new XSprite({
               frames: [ assets.content.throneRoomOverlay ]
            }),
            new XSprite({
               frames: [ assets.content.throneRoomOverlay ]
            })
         ]
      }
   ),
   battle: roomFactory('battle', '[]', '[]', null, 0, {}, {}, { x: 0, y: 0 }, [], {
      foreground: [
         assets.buttons.fight,
         assets.buttons.act,
         assets.buttons.item,
         assets.buttons.mercy,
         assets.sprites.hp,
         TORIEL.body.base
      ]
   })
};
