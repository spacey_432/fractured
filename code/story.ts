import { music } from './assets';
import { dialoguer } from './dialoguer';
import { X, XObject, XRectangle } from './storyteller';

const anchor = { x: 0 };
const size = { x: 160, y: 120 };

export const elements = [
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#ffffffff'
      }),
      lines: [
         // panel 1
         '{#i:55}{#c:storyteller}Upon reaching the surface, \nmonsters were once again \nshunned by humanity.{^22}',
         'FRISK, possessed by the HATE \nof their own past genocides, \nwasted no time in their \nbetrayal of the monsters.{^22}'
      ]
   },
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#00ffffff'
      }),
      lines: [
         // panel 2
         'This led the humans, with ten \nof their best wizards, to \nonce again seal the monsters \nunderground.{^22}',
         "In a twist of fate, the \nhumans saw FRISK'S betrayal \nas dishonorable, trapping \nthem with the monsters.{^22}"
      ]
   },
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#ffffffff'
      }),
      lines: [
         // panel 3
         "Alphys resumed the \ndetermination experiments, \nand Undyne helped keep the \nmonster's hopes up.{^22}"
      ]
   },
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#00ffffff'
      }),
      lines: [
         // panel 4
         'Meanwhile, in a moment of \nanger, ASGORE killed four \nhumans before the barrier \nwas sealed off forever.{^22}'
      ]
   },
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#ffffffff'
      }),
      lines: [
         // panel 5
         'After absorbing one of the \nSOULs to fight FRISK, he \npushed the human to their \nabsolute limit.{^22}',
         'And, without the power to \nLOAD... they had no choice \nbut to FIGHT.{^22}'
      ]
   },
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#00ffffff'
      }),
      lines: [
         // panel 6
         "Killing ASGORE caused FRISK's \nLOVE to shoot up dramatically, \nand their genocidal side took \nover.{^22}",
         'SANS, UNDYNE, and a few other \nmonsters tried stopping the \nhuman... but they died in the \nprocess.{^22}'
      ]
   },
   {
      panel: new XRectangle({
         anchor,
         size,
         fill: '#ffffffff'
      }),
      lines: [
         // panel 7
         '{#i:56}Seeing everything unfold, \nFLOWEY suggested that TORIEL \nshould gain some LOVE of her \nown.{^22}',
         'After hearing about all the \ndeaths in the kingdom, she \nrealized something truly... \n{^3}{@fill:red}terrible.{^22}',
         'The only way to stop a \ngenocidal maniac... was to \nbecome one herself.{^22}'
      ]
   },
   {
      panel: new XObject(),
      lines: [
         // panel 8 (black screen)
         '{#i:60}She had {^3}no {^6}other {^6}{@random:1,1}c{#triggerSlowdown3} {^2}h {^2}o {^2}i {^2}c {^2}e{^2}.{^50}',
         '{^1}'
      ]
   }
];

dialoguer.on('header', async key => {
   if (key === 'triggerSlowdown1') {
      music.intro.source.rate.value = 0.9;
   } else if (key === 'triggerSlowdown2') {
      music.intro.source.rate.value = 0.8;
   } else if (key === 'triggerSlowdown3') {
      while (music.intro.source.rate.value > 0.5) {
         music.intro.source.rate.value -= 0.01;
         await X.pause(100);
      }
   }
});

export const elementJobs = new Map<typeof elements[number], XObject>();
for (const element of elements) {
   elementJobs.set(
      element,
      new XObject({
         position: { x: 160, y: 20 },
         alpha: 0,
         objects: [ element.panel ]
      })
   );
}
